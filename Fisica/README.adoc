= Apuntes
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 3
:numbered:
:experimental:

<<<

{sp}+

== Info

{sp}+

Cada tema tendrá definido en el titulo un numero:

{sp}+

*G:* General

*I:* Bachillerato/Ciclos

*II:* Universidad

*III:* Otros

{sp}+

== Utilizar proyecto

{sp}+

*https://asciidocfx.com/[AsciidocFX]*: Programa para editar, visualizar y pasar a diferentes formatos los apuntes.

{sp}+

=== Importar y exportar el proyecto

{sp}+

*Importación:*

{sp}+

- `git clone https://gitlab.com/noahlio/apuntes.git`

{sp}+

*Exportación:*

{sp}+

- `git add` 

- `git commit -m "Mensaje"` 

- `git push -u origin nombreDeLaRama` El nombre de la rama suele ser main

{sp}+