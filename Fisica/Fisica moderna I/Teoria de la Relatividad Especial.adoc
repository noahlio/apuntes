= Template
Author Name <noahbobis0512@gmail.com>; Noah Bobis 
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

:imagesdir: ./Imagenes/
:enunciados: ./Ejercicios/Enunciados/
:soluciones: link:./Imagenes/Ejercicios/Soluciones/SolucionTeoriaDeLaRelatividadEspecial

== Ejercicios

{sp}+

=== Ejercicio 1

{sp}+

image::{enunciados}TeoriaDeLaRelatividadEspecialEj1.png[]
{soluciones}Ej1.png[Solución oficial]

{sp}+

----
----

{sp}+

