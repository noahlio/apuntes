= Gravitacion Universal
Author Name <noahbobis0512@gmail.com>; Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-03-14
:encoding: utf-8
:lang: ca
:toc: 
:toc-title: Índice
:imagesdir: ./Imagenes/
:enunciados: ./Ejercicios/Enunciados/
:soluciones: link:./Imagenes/Ejercicios/Soluciones/SolucionGravitacionUniversal
:contenido: ./Contenido/
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

== Ejercicios

{sp}+

=== Ejercicio 1

{sp}+

image::{enunciados}GravitacionUniversalEj1.png[]

{soluciones}Ej1.png[Solución oficial]

{sp}+

----
Fuerza gravitacional entre los objetos:

F = GMm/r^2 = 1,667 * 10^-8 N

Fuerza gravitacional entre tierra - objeto:

F = GMm/r^2 = 488,53 N
----

{sp}+

=== Ejercicio 2

{sp}+

image::{enunciados}GravitacionUniversalEj2.png[]

{soluciones}Ej2.png[Solución oficial]

{sp}+

----
Las fuerzas son negativas porque se hace un movimiento negativo al objeto en el plano.

Fuerza de atracción eje y: 

F1 = -GMm/r^2 * j= 121,4j N

Fuerza de atracción eje x: 

F2 = -GMm/r^2 * i= 121,4i N
----

{sp}+

=== Ejercicio 3

{sp}+

image::{enunciados}GravitacionUniversalEj3.png[]

{soluciones}Ej3.png[Solución oficial]

{sp}+

----
Fuerza gravitacional sobre A en el eje X:

Fi = GMm/r^2 i = 981,82i N 

Fuerza gravitacional sobre A en el eje Y:

Fj = GMm/r^2 j = 461j N 

Fuerza gravitacional sobre A:

F = Fi + Fj = 981,82i + 461j = √981,82^2+461^2 = 1084,66N

Angulo de trayectoria:

α = arccos(981,82/1084,66) = 25,15º
----

{sp}+

=== Ejercicio 4

{sp}+

image::{enunciados}GravitacionUniversalEj4.png[]

{soluciones}Ej4.png[Solución oficial]

{sp}+

----
g = 9,81 m/s^2
d = 5,45*10^3 kg/m^3
r = 6,37*10^6 m
v = 4/3𝞹r^3 = 1,08*10^21 m^3
M = d*v = 5,9*10^24 kg

g = GM/r^2  ->  G = g*r^2/M = 6,74*10^-11 m^3/kg s^2
----

image::{enunciados}GravitacionUniversalEj4.png[]

{sp}+

=== Ejercicio 5

{sp}+

image::{enunciados}GravitacionUniversalEj5.png[]

{soluciones}Ej5.png[Solución oficial]

{sp}+

----
k = 4𝞹^2 / GM = 3,12 * 10^-16 s^2/m^3

r = 3^√t^2/k = 421549829,5 m
----

{sp}+

=== Ejercicio 6

{sp}+

image::{enunciados}GravitacionUniversalEj6.png[]

{soluciones}Ej6.png[Solución oficial]

{sp}+

----
k = t^2/r^3 = 8,54*10^-16 s^2/m^3

M = 4𝞹^2 / Gk = 6,93 * 10^26 kg
----

{sp}+

=== Ejercicio 7

{sp}+

image::{enunciados}GravitacionUniversalEj7.png[]

{soluciones}Ej7.png[Solución oficial]

{sp}+

----
velocidad perihelio = distancia afelio * velocidad afelio / distancia perihelio = 3,02 * 10^4 m/s

distancia focal = excentricidad * senmieje mayor = 2543170,44 km

semieje menor = √semieje mayor^2-distancia focal^2 = 149576642,5 km
----

{sp}+

=== Ejercicio 8

{sp}+

image::{enunciados}GravitacionUniversalEj8.png[]

{soluciones}Ej8.png[Solución oficial]

{sp}+

----
g = GM/r^2 = 3,7 m/s^2
----

{sp}+

=== Ejercicio 9

{sp}+

image::{enunciados}GravitacionUniversalEj9.png[]

{soluciones}Ej9.png[Solución oficial]

{sp}+

----
g = GM/r^2  ->  r = √GM/g = 8946507,7 m

Altura = rh - r = 2546507,7 m 
----

{sp}+

=== Ejercicio 10

{sp}+

image::{enunciados}GravitacionUniversalEj10.png[]

{soluciones}Ej10.png[Solución oficial]

{sp}+

----
g = GM/r^2  ->  M = gr^2/G = 5,97*10^24 kg

v = 4/3𝞹*r^3 = 1,08*10^21 m^3

d = m/v = 5514 kg/m^3

v2 = 4/3𝞹*r2^3 = 6,54*10^19 m^3

M2 = d*v2 = 3,61*10^23 kg

g = GM2/r2^2 = 3,85 m/s^2 
----

{sp}+

=== Ejercicio 11

{sp}+

image::{enunciados}GravitacionUniversalEj11.png[]

{soluciones}Ej11.png[Solución oficial]

{sp}+

----
g = GM / r^2  ->  M = gr^2/G = 5,97*10^24

k = 4𝞹^2 / GM = 9,91*10^-14

rtl = 60r = 3,822*10^8 m

k = t^2/r^3  ->  t = √k*r^3 = 2352192.45 s = 27,2 dias
----

{sp}+

image::{enunciados}GravitacionUniversalEj11.png[]