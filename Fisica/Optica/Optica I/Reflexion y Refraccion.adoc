= Template
Author Name <noahbobis0512@gmail.com>; Noah Bobis 
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

:imagesdir: ./Imagenes/
:enunciados: ./Ejercicios/Enunciados/
:soluciones: link:./Imagenes/Ejercicios/Soluciones/SolucionReflexionyRefraccion

== Ejercicios

{sp}+

=== Ejercicio 1

{sp}+

image::{enunciados}ReflexionyRefraccionEj1.png[]
{soluciones}Ej1.png[Solución oficial]

{sp}+

----
----

{sp}+

=== Ejercicio 2

{sp}+

image::{enunciados}ReflexionyRefraccionEj2.png[]
{soluciones}Ej2.png[Solución oficial]

{sp}+

----
----

{sp}+

=== Ejercicio 3

{sp}+

image::{enunciados}ReflexionyRefraccionEj3.png[]
{soluciones}Ej3.png[Solución oficial]

{sp}+

----
----

{sp}+

=== Ejercicio 4

{sp}+

image::{enunciados}ReflexionyRefraccionEj4.png[]
{soluciones}Ej4.png[Solución oficial]

{sp}+

----
----

{sp}+

=== Ejercicio 5

{sp}+

image::{enunciados}ReflexionyRefraccionEj5.png[]
{soluciones}Ej5.png[Solución oficial]

{sp}+

----
----

{sp}+

=== Ejercicio 6

{sp}+

image::{enunciados}ReflexionyRefraccionEj6.png[]
{soluciones}Ej6.png[Solución oficial]

{sp}+

----
----

{sp}+

=== Ejercicio 7

{sp}+

image::{enunciados}ReflexionyRefraccionEj7.png[]
{soluciones}Ej7.png[Solución oficial]

{sp}+

----
----

{sp}+

