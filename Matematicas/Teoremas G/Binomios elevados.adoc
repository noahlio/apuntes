= Binomios elevados
Author Name <noahbobis0512@gmail.com>; Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 3
:numbered:
:experimental:

{sp}+

Para resolver un binomio elevado se multiplica el binomio por el numero de veces que esta separado y se va simplificando

{sp}+

*Ejemplo de binomio elevado:*

----
(3x^2 - y)^4 = 

(3x^2 - y) (3x^2 - y) (3x^2 - y) (3x^2 - y) =

(9x^4 - 6yx^2 + y^2) (9x^4 - 6yx^2 + y^2) = 

81x^8 - 108yx^6 + 54y^2x^4 - 12y^3x^2 + y^4
----
