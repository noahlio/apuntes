lambda <- 3
k <- 15
k2 <- 19
probablitat_menys_de_16 <- ppois(15, lambda * 4)
probablitat_mes_de_15 <- 1 - probablitat_menys_de_16
probabilitat_menys_de_20 <- ppois(k2, lambda * 4) - probablitat_menys_de_16
probabilitat_menys_de_20_sabent_mes_de_15 <- probabilitat_menys_de_20 / probablitat_mes_de_15
print(paste("La probabilitat de que arribin menys de 20 cotxes sabent que han arribat més de 15 en 4 hores es:", 
            probabilitat_menys_de_20_sabent_mes_de_15 * 100, "%"))