setwd("/data/Git/Apuntes/Matematicas/Estadistica/PAC 1/R")

observaciones <- c(9,4,7,10,8,8,9)
observaciones <- c(10,14,19,15,7,8,1,11)
observaciones <- c(3,4,16,15,16,9,14,19,10,10,17)
print(summary(observaciones))
print(quantile(observaciones, 0.75))

print(sqrt(mean((observaciones - mean(observaciones))^2, na.rm = TRUE)))

observaciones <- c(rep(80, 8), rep(95, 10), rep(118, 3), rep(77, 5))
observaciones <- c(rep(79, 10), rep(71, 8), rep(120, 9), rep(83, 10))
observaciones <- c(rep(3, 3), rep(6, 5), rep(11, 5))

print(quantile(observaciones, 0.75))
print(quantile(observaciones, 0.25, type=6))

print(sqrt(mean((observaciones - mean(observaciones))^2, na.rm = TRUE)))

print(round(summary(observaciones), 3))

dadesVendes <- read.table("vendes_pac1_P_15_1.csv", header=TRUE, 
                          sep=";",na.strings="NA",
                          fileEncoding = "UTF-8", quote = "\"", 
                          colClasses=c(rep("numeric",5)))

taula_freq_abs <- table(dadesVendes$PreuAm2)
print(taula_freq_abs)
par(mar = c(10, 5, 5, 1))
barplot(taula_freq_abs, main = "Freqüències Absolutes de IncomeGroup", xlab = "IncomeGroup", 
        ylab = "Freqüencia", ylim = c(0, 25))

resum_preuDm2 <- summary(dadesVendes$PreuAm2)
print(resum_preuDm2)
boxplot(dadesVendes$PreuAm2)
