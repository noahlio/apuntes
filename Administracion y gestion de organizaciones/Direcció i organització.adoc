= Direcció i organització
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 3
:numbered:
:experimental:
:imagesdir: ./Imagenes/

<<<

{sp}+

== Objectius empresarials

{sp}+

Funcions que compleixen els objectius empresarials:

- Guiar, incitar i coordinar les decisions i les accions de l'empresa.
- Proporcionar una base d'avaluació i control dels resultats obtinguts.
- Motivar els membres de l'empresa a conèixer, entendre i acceptar les seves metes per a que estiguin implicats.
- Transmetre les intencions de l'empresa (imatge d'empresa).

{sp}+

=== Missió

{sp}+

----
La missió recull la identitat i personalitat de l'empresa, en el moment actual i de cara al futur, des d'un punt de vista molt general.
----

{sp}+

S'inclouen els valors, les creences i la filosofia de l'empresa, els negocis en els quals està o estarà en el futur, les capacitats essencials que té i, també cada vegada amb més freqüència, aspectes de responsabilitat social.

{sp}+

=== Visió

{sp}+

----
La visió o propòsit estratègic és la percepció actual del que és o hauria de ser l'empresa en el futur i marca la direcció que cal seguir.
----

{sp}+

La visió incorpora la idea profunda de triomf, és estable al llarg del temps i ha de merèixer l'esforç i el compromís del personal en la seva consecució.

{sp}+

=== Objectius estratègics

{sp}+

La visió pot resultar massa abstracta i originar un gran desfasament entre la realitat actual i el futur que volem assolir.

Els objectius estratègics són menys ambiciosos, específics, successius, realistes i desafiadors.

{sp}+

Criteris:

- Naturalesa dels objectius: Objectius financiers i estratègics (competició en el mercat)
- Horitzó temporal: Objectius a curt i llarg termini.
- Grau de concreció: Objectius oberts (alta rentabilitat, elevada solvència...) i objectius tancats (quota de mercat del 15%, rendibilitat del 12%)
- Abast: Objectius ambiciosos i impossibles
- Nivell d'implantació: objectius corporatius (penetrar en el mercat asiàtic), objectius competitius (aconseguir productes més diferenciats) i objectius funcionals (minimitzar el grau d'endeutament de l'empresa)

{sp}+

=== Encarregats de fixar els objectius

{sp}+

El Consell d'Administració i els alts executius fixen la missió, la visió o propòsit estratègic i els objectius corporatius o globals de l'empresa.

Els directius o administradors de nivell intermedi fixen els objectius de les diferents divisions o unitats estratègiques de negoci.

El director de cada una de les àrees funcionals (màrqueting, producció, recursos humans, etc.) qui fixa els objectius de la seva àrea o departament.

{sp}+

== Organització

{sp}+

Tota activitat empresarial, per molt simple i senzilla que ens sembli, necessita estar organitzada perquè es pugui desenvolupar de manera eficaç i eficient.

Les dues decisions principals són la divisió de treball i la coordinació. Per la divisió de treball es creen unitats organitzatives (departaments, àrees...) que després es coordinen per garantir el funcionament de l'empresa.

{sp}+

=== Estructura organitzativa

{sp}+

L'estructura organitzativa ha de ser un dels aspectes més estables de l'organització, s'ha d'anar modificant per adaptar-se als canvis.

.Estuctura d'una organització
image::estructuraOrganitzativa.png[400,400]

{sp}+

.Elements bàsics d'una organització
image::elementsOrganitzacio.png[400,400]

{sp}+

==== Vèrtex estratègic

{sp}+

Compost per càrrecs de responsabilitat general i directius d'alt nivell amb funcions de caràcter global i assistencia directe (secretàries de direcció, assessors personals...) es l'encarregat de que l'organització compleixi la seva missió i objectius, les seves funcions principals són:

- Supervisió directa de l'organització, prendre decisions sobre el disseny de l'estructura organitzativa formal, assignació de recursos, correcció d'anomalies, motivació i comunicació.
- Relació amb l'entorn (representants devant grups, institucions...).
- Desenvolupament de l'estratègia global de l'empresa (definició d'objectius, anàlisi interna i externa, i formulació de la implantació de l'estratègia).

{sp}+

==== Nucli operatiu

{sp}+

Compost per tots els treballadors amb tasques relacionades amb la producció de productes i serveis, les seves principals funcions són:

- Assegurar les entrades per a la producció (aprovisionament, recepció de matèries primeres, emmagatzemament...)
- Transformar les entrades en sortides (muntatge, fabricació, prestació de serveis...)
- Distribuir les sortides (màrqueting, vendes i distribució física dels productes).
- Proporcionar suport a les activitats anteriors (control d'inventaris, manteniment d'equips, control de qualitat...)

{sp}+

==== Línia intermèdia

{sp}+

Composta per encarregats de la gestió (directius funcionals, caps de taller, capatassos, caps de servei...), sorgeixen a mesura que l'organització creix i les seves funcions són:

- Ascendir problemes des dels nivells inferiors fins als superiors, amb propostes de solució.
- Fan descendir les decisions dels directius de nivell superior, perquè s'executin en el nivell adequat.
- Són responsables de dirigir la seva unitat i desenvolupar els projectes que se'ls assignin.

Si la línia intermèdia es molt desenvolupada es produeix una pèrdua de control, és recomanable que no hi hagin molts nivells jeràrquics (aplanament de la piràmide organitzativa).

{sp}+

==== Tecnoestructura

{sp}+

Composta per tècnics i analistes (enginyers industrials, analistes de planificació i control, formadors, responsables de contractació...), sense esta sotmesa a la línia jeràrquica principal de l'organització (funcionen a tots els nivells), te la funció principal de fer més eficaç i eficient la feina d'altres parts de l'organització.

{sp}+

==== Equip de suport

{sp}+

Composta per unitats especialitzades, tenen la funció de donar suport i prestar serveis a l'organització, sense tenir relació directa amb el procés de producció.

De vegades aquesta part de l'esctructura són petites organitzacions (proveïdors externs).

.Exemple unitats d'una organització
image::unitatsOrganitzacio.png[400,400]

{sp}+

== Dimensions de disseny organitzatiu

{sp}+

Processor del disseny de l'estructura:

- Diferenciació: Dividir la feina en tasques per millorar l'eficiencia.
- Integració d'activitats: Coordinar els esforços de diferents parts de l'organització per complir objectius.

Per sapiguer quines tasques fer i a qui assignar-les s'utilitzen una sèrie de variables (paràmetres de disseny).

{sp}+

=== Diferenciació d'activitats

{sp}+

{sp}+

== Glossari

{sp}+

- Stakeholders: Parts interessades d'una empresa, els quals es veuen afectats pels canvis de l'empresa.