= Conceptes bàsics
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 3
:numbered:
:experimental:
:imagesdir: ./Imagenes/

<<<

{sp}+

== Organització

{sp}+

Associació de persones regulada per un conjunt de normes en funció de determinades finalitats.

Així, tota organització ha d'estar formada per persones capaces d'unirse amb altres que volen col·laborar en una activitat per a assolir una finalitat comuna.

{sp}+

Elements per a que una organització es consideri empresa:

- Unitat de transformació d'entrades en sortides de manera que se n'incrementi la utilitat.
- Unitat de direcció, ja que hi ha d'haver una persona o conjunt de persones que s'encarreguin d'organitzar i dirigir tot el procés.
- Un objectiu compartit per tots els membres de l'empresa.

== Empresa

{sp}+

Conjunt de factors de producció (matèries primeres, mà d'obra, etc.), factors referents al màrqueting i factors financers.

Tota empresa presenta finalitats o objectius que en justifiquen l'existència i generen valor.

La coordinació dels factors, necessària per a l'existència de l'empresa, la fa el factor directiu.

L'empresa és un sistema, conjunt d'elements relacionats entre ells i amb el sistema global, que tracta d'assolir certs objectius.

{sp}+

Àrees funcionals: producció, màrqueting, finances i recursos humans.

{sp}+

=== Tipus

{sp}+

Segons la propietat o titularitat del capital:

- Empresa privada: en què el capital és propietat de particulars.
- Empresa pública: en què els poders públics (administració central, autonòmica o local) són els propietaris de tot el capital o una part substancial, i la seva influència en el sistema de direcció és decisiva.
- Empresa cooperativa o social: en què feina i propietat s'unifiquen en un sol factor, és a dir, el capital és propietat dels treballadors.

{sp}+

Segons la mida de l'empresa:

- Microempreses: menys de 10 empleats i menys de 2 milions d'euros de volum de negoci o menys de 2 milions d'euros d'actius.
- Empreses petites: entre 10 i 49 empleats i menys de 10 milions d'euros de volum de negoci o menys de 10 milions d'euros d'actius.
- Empreses mitjanes: entre 50 i 249 empleats i menys de 50 milions d'euros de volum de negoci o menys de 43 milions d'euros d'actius.
- Empreses grans: més de 250 empleats i més de 50 milions de volum de negoci o més de 43 milions d'euros d'actius.

{sp}+

Segons la naturalesa de l'activitat econòmica productiva:

* Empreses industrials: produeixen béns transformant o extraient matèries primeres
** Empreses extractives: Explotació de recursos naturals (pesqueres, fusta, mineres...)
** Empreses transformadores o manufactures: Transformen matèries primeres en productes acabats
*** Productes de consum final: Productes per clients finals (roba, aliments...)
*** Productes de producció: Productes per produir altres de consum final (maquinària, productes químics...)
* Empreses comercials: Intermediaries entre productor i consumidor.
** Majoristes: Venen a gran escala.
** Detallistes: Venen al detall.
** Comissionistes: Intermediaris entre venedors i clients.
* Empreses de serveis: Donen serveis a clients
** Transport
** Turisme
** Finances
** Educació

{sp}+

Segons el sector d'activitat

- Sector primari: Exploten recursos naturals, no inclou mineria. (pesqueres, silvicultura, ramaderes...)
- Sector secundari: Explotació del subsòl o activitats de maquinaria i manufactura. (mineres, constructores, químiques...)
- Sector terciari: Resta de sectors (hosteleria, transport, comercials...)

{sp}+

Segons l'àmbit d'actuació

- Empreses locals
- Empreses provincials
- Empreses regionals
- Empreses nacionals
- Empreses multinacionals

{sp}+

Segons característiques socioeconòmiques

- Empresa artesanal: Empreses petites que fan treballs manuals i d'actuació local.
- Empresa capitalista: Producció en massa utilitzant certa tecnologia i hi ha separació entre propietat i direcció.

{sp}+

Segons la forma jurídica

- Titularitat de persona física: Una persona física assumeix el control i el risc derivat de l'activitat.
- Sense personalitat jurídica: Comunitats de béns (autònoms operant conjuntament) o societat civil.
- Titularitat de persona jurídica: Associacions voluntaries desenvolupen una activitat econòmica.

{sp}+

=== Elecció de forma jurídica

{sp}+

Per escollir la forma jurídica d'una empresa s'ha de tenir en compte:

[%header, cols=3*,separator=|]
|===
| Aspectes | Persones físiques | Persones jurídiques |
Responsabilitat devant de tercers | Responsabilitat il·limitada, respon amb béns presents i futurs | Responsabilitat del seu capital |
Tràmits administratius | Tràmits corresponents a l'exercici de l'activitat | Tràmits per l'adopció de la personalitat jurídica |
Capital mínim per a la construcció | No necessari | Segons el tipus de societat |
Tributació dels beneficis | IRPF | Impost sobre societats |
|===

image::tipusEmpreses.png[400,400]

{sp}+

=== Classificació d'empreses turístiques

{sp}+

Segons l'activitat principal es classifiquen en:

- Allotjaments turístics: Ofereixen hostalatge, amb serveis complementaris o sense (hotels, motels, pensions...).
- Empreses de restauració: Restaurans, cafeteries, tavernes...
- Empreses d'intermediació: Agències de viatge, centrals de reserves..
- Empreses de transport i auxiliars: Ferrocarrils, autocars...
- Empreses d'esbarjo i cultura: Parcs temàtics, espais naturals, fires...
- Altres: Guies, consultories...
