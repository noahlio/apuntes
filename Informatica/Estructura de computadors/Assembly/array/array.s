section .data
    arreglo dd 10, 20, 30, 40, 50

section .text
    global _start

_start:
    lea rbx, [arreglo]

    mov rdi, 1  ; segundo elemento
    imul rdi, 4 ; cada elemento es de 4 bytes
    add rbx, rdi

    int3

    mov eax, [rbx]  ; carga el valor en eax

    mov rax, 60         ; syscall number for sys_exit
    xor rdi, rdi        ; exit code 0
    syscall
