section .bss
    buffer resb 10   ; Buffer para almacenar la entrada (10 caracteres)
    number resb 16

section .text
    global _start

_start:
    ; Llamada al sistema para leer desde la entrada estándar (stdin)
    mov rax, 0          ; Número de llamada al sistema para sys_read
    mov rdi, 0          ; Descriptor de archivo 0: stdin
    mov rsi, buffer     ; Puntero al buffer de entrada
    mov rdx, 10         ; Longitud máxima a leer
    syscall

    mov rdi, buffer     ; Puntero al Buffer
    call atoi

    mov r8, rax
    imul r8, 3

    call itoa

    call print_r8
    call exit

atoi:
    mov rax, 0              ; Set initial total to 0
    call loop_atoi
    ret
     
loop_atoi:
    movzx rsi, byte [rdi]   ; Get the current character

    cmp rsi, 10
    je done

    test rsi, rsi           ; Check for \0
    je done
    
    cmp rsi, 48             ; Anything less than 0 is invalid
    jl error
    
    cmp rsi, 57             ; Anything greater than 9 is invalid
    jg error
     
    sub rsi, 48             ; Convert from ASCII to decimal 
    imul rax, 10            ; Multiply total by 10
    add rax, rsi            ; Add current digit to total
    
    inc rdi                 ; Get the address of the next character
    jmp loop_atoi

itoa:
    mov rdi, rbx        ; Dirección del buffer
    mov rdx, 0          ; Inicializa el contador de dígitos a 0
    call loop_itoa
    ret

loop_itoa:
    mov r8, rax         ; Copia el valor de rax a r8 para preservar rax
    mov rax, 0          ; Inicializa rax antes de la división
    mov rcx, 10         ; Base decimal
    div rcx             ; Divide rdx:rax por rcx, el cociente queda en rax, el resto en rdx
    add dl, '0'         ; Convierte el dígito a carácter ASCII
    dec rdi             ; Retrocede en el buffer
    mov [rdi], dl       ; Almacena el carácter en el buffer
    inc rdx             ; Incrementa el contador de dígitos

    test rax, rax       ; Verifica si el cociente es cero
    jnz loop_itoa       ; Si no es cero, continúa el bucle

    ; Restaura el valor original de rax
    mov rax, r8
    ret

error:
    mov rax, 60
    mov rdi, 1              ; Código de salida 1 (indicando un error)
    syscall
 
done:
    ret                     ; Return total or error code

print_r8:
    mov [number], r8
    mov esi, [number]
    add esi, '0'
    mov [number], esi
    mov eax, 4
    mov ecx, number
    mov edx, 10
    int 0x80
    ret

exit:
    mov rax, 60         ; Número de llamada al sistema para sys_exit
    mov rdi, 0
    syscall