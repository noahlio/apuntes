section .bss
    number resb 4

section .text
    global _start

_start:
    mov esi, 3
    mov [number], esi
    mov esi, [number]
    add esi, 48
    mov [number], esi
    mov eax, 4
    mov ecx, number
    mov edx, 10
    int 0x80

    ; Exit
    mov rax, 60         ; syscall number for sys_exit
    xor rdi, rdi        ; exit code 0
    syscall
