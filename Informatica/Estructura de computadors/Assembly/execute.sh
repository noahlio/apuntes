#!/bin/bash
while getopts ":f:x" opt; do
    case $opt in
        f)
            archivo="$OPTARG"
            ;;
        x)
            execute=true
            ;;
        \?)
            echo "Opción inválida: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "La opción -$OPTARG requiere un argumento." >&2
            exit 1
            ;;
    esac
done

if [ -z "$archivo" ]; then
    echo "Por favor, proporciona el nombre de una carpeta con un archivo .s como argumento f."
    exit 1
fi

cd "$archivo" || exit

nasm -f elf64 -g -o "$archivo".o "$archivo".s
ld "$archivo".o -o "$archivo"

if [ "$execute" ]; then
    ./"$archivo"
fi
