
section .data
    mesg: db "Hello world!", 10
    mesgLen: equ $-mesg

section .text
    global _start

_start:
    ; Escribir la cadena a stdout
    mov rax, 1          ; syscall number for sys_write
    mov rdi, 1          ; file descriptor 1 is stdout
    mov rsi, mesg       ; pointer to the message
    mov rdx, 12         
    syscall

    ; Exit
    mov rax, 60         ; syscall number for sys_exit
    xor rdi, rdi        ; exit code 0
    syscall
