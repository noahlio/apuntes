section .data               
;Canviar Nom i Cognom per les vostres dades.
developer db "Noah Bobis",0

;Constant que també està definida en C.
DIMMATRIX equ 5		

section .text            
;Variables definides en Assemblador.
global developer                        

;Subrutines d'assemblador que es criden des de C.
global posCurScreenP1, updateColP1, updateMatrixBoardP1, getSecretPlayP1
global printSecretP1, checkSecretP1, printHitsP1, checkPlayP1, printMessageP1, playP1

;Variables globals definides en C.
extern charac, rowScreen, colScreen, mSecretPlay
extern col, state, tries, hX

;Funcions de C que es criden des de assemblador.
extern clearScreen_C,  gotoxyP1_C, printchP1_C, getchP1_C
extern printBoardP1_C, printMessageP1_C


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ATENCIÓ: Recordeu que en assemblador les variables i els paràmetres 
;;   de tipus 'char' s'han d'assignar a registres de tipus  
;;   BYTE (1 byte): al, ah, bl, bh, cl, ch, dl, dh, sil, dil, ..., r15b
;;   les de tipus 'short' s'han d'assignar a registres de tipus 
;;   WORD (2 bytes): ax, bx, cx, dx, si, di, ...., r15w
;;   les de tipus 'int' s'han d'assignar a registres de tipus 
;;   DWORD (4 bytes): eax, ebx, ecx, edx, esi, edi, ...., r15d
;;   les de tipus 'long' s'han d'assignar a registres de tipus 
;;   QWORD (8 bytes): rax, rbx, rcx, rdx, rsi, rdi, ...., r15
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Les subrutines en assemblador que heu implementar són:
;;   posCurScreenP1, updateColP1, updateMatrixBoardP1, getSecretPlayP1
;;   printSecretPlayP1, checkSecretP1, printHitsP1, checkPlayP1.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Situar el cursor a la fila indicada per la variable (rowScreen) i a 
; la columna indicada per la variable (colScreen) de la pantalla,
; cridant la funció gotoxyP1_C.
; 
; Variables globals utilitzades:   
; (rowScreen): Fila de la pantalla on posicionem el cursor.
; (colScreen): Columna de la pantalla on posicionem el cursor.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
gotoxyP1:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   call gotoxyP1_C
 
   ;restaurar l'estat dels registres que s'han guradat a la pila.
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Mostrar un caràcter guardat a la variable (charac) a la pantalla, 
; en la posició on està el cursor, cridant la funció printchP1_C.
; 
; Variables globals utilitzades:   
; (charac): Caràcter que volem mostrar.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
printchP1:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   call printchP1_C
 
   ;restaurar l'estat dels registres que s'han guradat a la pila.
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax

   mov rsp, rbp
   pop rbp
   ret
   

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Llegir una tecla i guarda el caràcter associat a la variable (charac)
; sense mostrar-la per pantalla, cridant la funció getchP1_C. 
; 
; Variables globals utilitzades:   
; (charac): Caràcter que llegim de teclat.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
getchP1:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   call getchP1_C
 
   ;restaurar l'estat dels registres que s'han guradat a la pila.
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax
   
   mov rsp, rbp
   pop rbp
   ret 



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Posiciona el cursor dins al tauler segons la posició del cursor (col),
; dels intents que queden (tries) i l'estat del joc (state). 
; Si estem entrant la combinació secreta (state==0) ens posarem a la
; fila 3 (rowScreen=3), si estem entrant un jugada (state==1) la 
; fila es calcula amb la fórmula: (rowScreen=9+(DIMMATRIX-tries)*2).
; La columna es calcula amb la fòrmula (colScreen= 8+(col*2)).
; Per a posicionar el cursor es crida la subrutina gotoxyP1.
; 
; Variables globals utilitzades:
; (rowScreen): Fila de la pantalla on es situa el cursor.
; (colScreen): Columna de la pantalla on es situa el cursor.
; (state)    : Estat del joc.
; (tries)    : Intents que queden.
; (col)      : Columna on està el cursor.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
posCurScreenP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   cmp dword [state], 0
   jne posCurScreenP1_if_state_not_0
   mov dword [rowScreen], 3
   jmp posCurScreenP1_end_if_state_0
   posCurScreenP1_if_state_not_0:
   mov rax, DIMMATRIX
   sub rax, qword[tries]
   imul rax, 2
   add rax, 9
   mov dword [rowScreen], eax
   posCurScreenP1_end_if_state_0:
   mov eax, [col]    ; Mueve el valor de col a eax
   shl eax, 1      ; Multiplica el valor de eax por 2 (equivale a col * 2)
   add eax, 8      ; Suma 8 al valor de eax
   mov [colScreen], eax ; Almacena el resultado en colScreen
   call gotoxyP1
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Actualitzar la columna (col) on està el cursor.
; Si s'ha llegit (charac=='j') esquerra o (charac=='k') dreta 
; actualitzar la posició del cursor (col +/- 1)
; controlant que no surti del vector [0..DIMMATRIX-1]. 
;  
; Variables globals utilitzades:	
; (charac): Caràcter llegit des del teclat.
; (col)   : Posició on està el cursor.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
updateColP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   cmp byte[charac], 'j'
   je updateColP1_if_charac_j
   cmp byte[charac], 'k'
   je updateColP1_if_charac_k
   jmp updateColP1_end_if_charac
   updateColP1_if_charac_j:
   cmp dword [col], 0
   jle updateColP1_end_if_charac
   sub dword [col], 1
   jmp updateColP1_end_if_charac
   updateColP1_if_charac_k:
   mov eax, DIMMATRIX
   sub eax, 1
   cmp eax, [col]
   jle updateColP1_end_if_charac
   add dword [col], 1
   updateColP1_end_if_charac:
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Guardar el caràcter llegit ['0'-'9'] (charac) a la matriu 
; (mSecretPlay) a la fila indicada per la variable (state) i
; la columna indicada per la variable (col).
; Si (state==0) canviarem el caràcter llegit per un '*' (charac='*') 
; perquè no es vegi la combinació secreta que escrivim.
; Finalment mostrarem el caràcter (charac) a la pantalla a la posició
; on està el cursor cridant la subrutina printchP1.
; 
; Variables globals utilitzades:	
; (charac)    : Caràcter a mostrar.
; (mSecrePlay): Matriu on guardem la combinació secreta i la jugada.
; (col)       : Columna on està el cursor.
; (state)     : Estat del joc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
updateMatrixBoardP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.

   mov eax, dword [state]
   imul eax, DIMMATRIX
   add eax, dword [col]
   lea rbx, [mSecretPlay]
   lea rbx, [rbx + rax]
   mov al, byte [charac]
   mov byte [rbx], al

   cmp dword [state], 0
   jne updateMatrixBoardP1_end_if_state_0
   mov byte[charac], '*'
   updateMatrixBoardP1_end_if_state_0:
   call printchP1_C
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Llegir els caràcters de la combinació secreta o la jugada.
; Mentre no es premi ENTER(10) o ESC(27) fer el següent:
; · Posicionar el cursor a la pantalla cridant la subrutina posCurScreenP1, 
;   segons el valor de les variables (col, tries i state).
; · Llegir un caràcter de teclat cridant la subrutina getchP1
;   que retorna a (charac) el codi ASCII del caràcter llegit.
;   - Si s'ha llegit una 'j'(esquerra) o una 'k' (dreta) moure el 
;     cursor per les 5 posicions de la combinació actualitzant 
;     el valor de la variable (col) cridant la subrutina updateColP1
;     en funció de les variables (col, tries i state).
;   - Si s'ha llegit un número ['0'-'9'] el guardem a la matriu
;     (mSecretPlay) i el mostrem cridant la subrutina updateMatrixBoardP1
;     en funció de les variables (charac, mSecretPlay, col i state).
; Si s'ha premut ESC(27) posar (state=-1) per a indicar que hem de sortir.
; Si es prem ENTER(10) s'acceptarà la combinació tal com estigui.
; NOTA: Cal tindre en compte que si es prem ENTER sense haver assignat
; valors a totes les posicions de la combinació, hi haurà posicions
; que seran un espai (valor utilitzat per inicialitzar la matriu).
; 
; Variables globals utilitzades:
; (charac)     : Caràcter llegit des del teclat.
; (col)        : Columna on està el cursor.
; (mSecretPlay): Matriu on guardem la combinació secreta i la jugada.
; (state)      : Estat del joc.
; (tries)      : Nombre d'intents que queden.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
getSecretPlayP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   mov dword[col], 0

   getSecretPlayP1_while_charac_exit:
   call posCurScreenP1
   call getchP1

   cmp byte[charac], 'j'
   je getSecretPlayP1_if_charac_j_k
   cmp byte[charac], 'k'
   je getSecretPlayP1_if_charac_j_k
   jmp getSecretPlayP1_end_if_charac_j_k
   getSecretPlayP1_if_charac_j_k:
   call updateColP1
   getSecretPlayP1_end_if_charac_j_k:

   cmp byte[charac], '0'
   jl getSecretPlayP1_end_if_charac_09
   cmp byte[charac], '9'
   jg getSecretPlayP1_end_if_charac_09
   call updateMatrixBoardP1
   getSecretPlayP1_end_if_charac_09:
   
   cmp byte[charac], 10
   je getSecretPlayP1_end_while_charac_exit
   cmp byte[charac], 27
   je getSecretPlayP1_end_while_charac_exit
   jmp getSecretPlayP1_while_charac_exit

   getSecretPlayP1_end_while_charac_exit:
   cmp byte[charac], 27
   jne getSecretPlayP1_end_if_charac_27
   mov byte[state], -1
   getSecretPlayP1_end_if_charac_27:
   
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mostrar la combinació secreta del joc.
; Mostra la combinació secreta (fila 0 de la matriu mSecretPlay) 
; a la part superior del tauler quan finalitza el joc. 
; Per a mostrar els valors s'ha de cridar la subrutina gotoxyP1 per a 
; posicionar el curso, a la fila 3 (rowScreen=3) i a partir de la 
; columna 8 (colScreen=8) i printchP1 per a mostrar cada caràcter.
; Incrementar la columna (colScreen) de 2 en 2. 
; 
; Variables globals utilitzades:
; (rowScreen)  : Fila de la pantalla on es situa el cursor.
; (colScreen)  : Columna de la pantalla on es situa el cursor.
; (charac)     : Caràcter llegit des del teclat.
; (mSecretPlay): Matriu on guardem la combinació secreta i la jugada.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
printSecretP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   mov dword[rowScreen], 3
   mov dword[colScreen], 8
   mov rcx, -1

   printSecretP1_for_i_lower_dimmatrix:
   add rcx, 1
   call gotoxyP1
   mov al, byte[mSecretPlay + rcx]
   mov byte[charac], al
   call printchP1
   add dword[colScreen], 2
   cmp rcx, DIMMATRIX
   jl printSecretP1_for_i_lower_dimmatrix
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Verificar que la combinació secreta no tingui el valor inicial (' '),
; ni números repetits.
; Per cada element de la fila [0] de la matriu (mSecretPlay) mirar que 
; no hi hagi un espai (' ') i que no estigui repetit (de la posició 
; següent a l'actual fins al final). Per a indicar que la combinació
; secreta no és correcte posarem (secretError=1).
; Si la combinació secreta no és correcta, posar (state=2) per 
; indicar-ho.
; Sinó, la combinació secreta és correcta, posar (state=1) per anar
; a llegir jugades.
; 
; Variables globals utilitzades:
; (mSecretPlay): Matriu on guardem la combinació secreta i la jugada.
; (state)      : Estat del joc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
checkSecretP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   mov r8, -1

   checkSecretP1_for_i_lower_dimmatrix:
   add r8, 1
   cmp r8, DIMMATRIX
   jge checkSecretP1_secretError_off
   mov al, byte[mSecretPlay + r8]
   cmp al, ' '
   je checkSecretP1_secretError_on
   mov r9, r8

   checkSecretP1_for_j_lower_dimmatrix:
   add r9, 1
   cmp r9, DIMMATRIX
   jge checkSecretP1_for_i_lower_dimmatrix
   movzx rcx, byte[mSecretPlay + r9]
   cmp al, cl
   jne checkSecretP1_for_j_lower_dimmatrix

   checkSecretP1_secretError_on:
   mov dword[state], 2
   jmp checkSecretP1_end_secretError
   checkSecretP1_secretError_off:
   mov dword[state], 1
   checkSecretP1_end_secretError:
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mostrar els encerts al lloc.
; Situar el cursor en la fila (rowScreen = 9+(DIMMATRIX-tries)*2) i 
; la columna (colScreen = 22) (part dreta del tauler) per a mostrar 
; els encerts al tauler de joc.
; Es mostren els encerts al lloc (hX), tantes 'X' com 
; encerts al lloc hi hagin.
; Per a mostrar els encerts s'ha de cridar la subrutina gotoxyP1 per a 
; posicionar el cursor i printchP1 per a mostrar els caràcters. 
; Cada cop que es mostra un encert s'ha d'incrimentar 
; la columna (colScreen) de 2 en 2.
; NOTA: (hX ha de ser sempre més petit o igual que DIMMATRIX).
; 
; Variables globals utilitzades:
; (rowScreen): Fila de la pantalla on es situa el cursor.
; (colScreen): Columna de la pantalla on es situa el cursor.
; (charac)   : Caràcter a mostrar.	
; (tries)    : Intents que queden.
; (hX)       : Encerts al lloc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
printHitsP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   mov rax, DIMMATRIX
   sub rax, qword[tries]
   imul rax, 2
   add rax, 9
   mov dword[rowScreen], eax
   mov dword[colScreen], 22
   mov byte[charac], 'X'

   mov r9, [hX]
   printHitsP1_for_i_upper_0:
   cmp r9, 0
   jle printHitsP1_end_for_i_upper_0
   call gotoxyP1
   call printchP1
   mov EBX, dword[colScreen]
   add EBX, 2
   mov dword[colScreen], EBX
   sub r9, 1
   jmp printHitsP1_for_i_upper_0
   printHitsP1_end_for_i_upper_0:
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Compta els encerts al lloc de la jugada 
; respecte de la combinació secreta.
; Comparar cada element de la combinació secreta  amb
; l'element que hi ha a la mateixa posició de la jugada.
; Si un element de la combinació secreta (mSecretPlay[0][i]) és igual
; a l'element de la mateixa posició de la jugada (mSecretPlay[1][i])
; serà un encert al lloc 'X' i s'han d'incrementar els encerts a lloc (hX++).
; Si totes les posicions de la combinació secreta i de la jugada
; són iguals (hX=DIMMATRIX) hem guanyat i s'ha de modificar l'estat
; del joc per a indicar-ho (state=3),
; sinó, mirar si s'han exhaurit els intents (tries=1) modificar l'estat del
; joc per a indicar-ho (state=4).
; Mostrar els encerts al lloc al tauler de joc 
; cridant la subrutina printHitsP1.
; 
; Variables globals utilitzades:	
; (mSecretPlay): Matriu on guardem la combinació secreta i la jugada
; (state)      : Estat del joc.
; (tries)      : Intents que queden.
; (hX)         : Encerts a lloc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
checkPlayP1:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   mov word[hX], 0
   mov r8, -1

   checkPlayP1_for_i_lower_dimmatrix:
   add r8, 1
   cmp r8, DIMMATRIX
   jge checkPlayP1_end_for_i_lower_dimmatrix
   mov al, byte[mSecretPlay + r8]
   movzx rcx, byte[mSecretPlay + r8 + DIMMATRIX]
   cmp al, cl
   jne checkPlayP1_for_i_lower_dimmatrix
   add word[hX], 1
   jmp checkPlayP1_for_i_lower_dimmatrix
   checkPlayP1_end_for_i_lower_dimmatrix:

   cmp word[hX], DIMMATRIX
   je checkPlayP1_if_hx_equals_dimmatrix
   cmp qword[tries], 1
   jne checkPlayP1_if_end
   mov dword[state], 4
   jmp checkPlayP1_if_end
   checkPlayP1_if_hx_equals_dimmatrix:
   mov dword[state], 3
   checkPlayP1_if_end:

   call printHitsP1
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mostra un missatge a la part inferior dreta del tauler segons el 
; valor de la variable (state).
; (state) -1: S'ha premut ESC per sortir
;          0: Estem entrant la combinació secreta.
;          1: Estem entrant la jugada.
;          2: La combinació secreta té espais o valors repetits.
;          3: S'ha guanyat, jugada = combinació secreta.
;          4: S'han esgotat els intents.
; 
; S'espera que es premi una tecla per continuar. 
; Mostrar un missatge a sota al tauler per indicar-ho 
; i al prémer una tecla l'esborra.
; 
; Variables globals utilitzades:	
; (rowScreen): Fila de la pantalla on es situa el cursor.
; (colScreen): Columna de la pantalla on es situa el cursor.
; (state)    : Estat del joc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
printMessageP1:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   call printMessageP1_C
 
   ;restaurar l'estat dels registres que s'han guardat a la pila.
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Subrutina principal del joc
; Llegeix la combinació secreta i verifica que sigui correcte.
; A continuació es llegeix una jugada, compara la jugada amb la
; combinació secreta per a determinar els encerts a lloc.
; Repetir el procés mentre no s'encerta la combinació secreta i mentre 
; queden intents. Si es prem la tecla 'ESC' durant la lectura de la 
; combinació secreta o d'una jugada sortir.
; 
; Pseudo codi:
; El jugador disposa de 5 intents (tries=5) per encertar
; la combinació secreta, l'estat inicial del joc és 0 (state=0) i
; el cursor es posa a la columna 0 (col=0).
; Mostrar el tauler de joc cridant la funció printBoardP1_C.
; 
; Mentre (state==0) llegir la combinació secreta o (state==1) llegir
; la jugada:
;   - Mostrar els intents que queden (tries) per a encertar la combinació 
;     secreta, situar el cursor a la fila 21, columna 5 cridant la subrutina
;     gotoxyP1 i mostra el caràcter associat al valor de la variable.
;     (tries) sumant '0' i cridant la subrutina printchP1.
;   - Mostrar un missatge segons estat del joc (state) cridant 
;     la subrutina printMessageP1.
;   - Posicionar el cursor al tauler cridant la subrutina posCurBoardP1.
;   - Llegir els caràcters de la combinació secreta o de la jugada 
;     i actualitzar l'estat del joc cridant la subrutina getSecretPlayP1.
;   - Si estem introduïnt la combinació secreta (state==0) verificar
;     que es correcte cridant la subrutina checkSecretP1.
;     Sino, si estem introduïnt la jugada (state==1) verificar 
;     els encerts de la jugada cridant la subrutina checkPlayP1,
;     decrementar els intents (tries). Inicialitzar la jugada que
;     tenim guardada a la fila 1 de la matriu mSecretPlay amb 
;     espais (' ') per poder introduir una nova jugada.
; 
; Per a acabar, mostrar els intents que queden (tries) per a 
; encertar la combinació secreta, situar el cursor a la fila 21, 
; columna 5 cridant la subrutina gotoxyP1 i mostra el caràcter associat 
; al valor de la variable (tries) sumant '0' i cridant a la subrutina 
; printchP1, mostrar la combinació secreta cridant la subrutina 
; printSecretP1 i finalment mostrar un missatge segons l'estat del 
; joc (state) cridant la subrutina printMessageP1.
; S'acaba el joc.
; 
; Variables globals utilitzades:
; (col)        : Posició on està el cursor.
; (state)      : Estat del joc.
; (tries)      : Intents que queden.
; (rowScreen)  : Fila de la pantalla on es situa el cursor.
; (colScreen)  : Columna de la pantalla on es situa el cursor.
; (charac)     : Caràcter llegit des del teclat i a mostrar.
; (mSecretPlay): Adreça de la matriu on guardem la combinació secreta i la jugada.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
playP1:
   push rbp
   mov  rbp, rsp 
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.	
   push rcx
   push rdi
   
   mov  DWORD[col], 0         ;col = 0;    //Posició on està el cursor.
   mov  DWORD[state], 0       ;state=0;    //Estat del joc.
   mov  QWORD[tries], 5       ;tries=5;    //Intents que queden.
   
   call printBoardP1_C        ;printBoardP1_C(tries);
      
   P1_while:
   cmp DWORD[state], 0        ;while (state == 0 
   je  P1_whileOk
   cmp DWORD[state], 1        ;|| state==1) {
   jne P1_endwhile
     P1_whileOk:
     
     mov DWORD[rowScreen], 21
     mov DWORD[colScreen], 5
     call gotoxyP1            ;gotoxyP1_C();
     mov rdi, QWORD[tries]
     add dil, '0' 
     mov BYTE[charac], dil    ;charac = tries + '0';
     call printchP1           ;printchP1_C();
     call printMessageP1      ;printMessageP1_C();
     call posCurScreenP1      ;posCurScreenP1();
      
     call getSecretPlayP1     ;getSecretPlayP1_C();
	 
	 P1_if1:
	 cmp DWORD[state], 0      ;if (state==0) {
	 jne P1_else1
       call checkSecretP1     ;checkSecretP1_C();
       jmp P1_endif1          ;}
     P1_else1:                ;} else {
       P1_if2:
       cmp DWORD[state], 1    ;if (state==1) {
	   jne P1_endif2
	     call checkPlayP1     ;checkPlayP1_C();
         dec QWORD[tries]     ;tries --;
       P1_endif2:             ;}
       mov rcx, 0             ;i=0;
       P1_for:                ;for (
       cmp rcx, (DIMMATRIX)
       jge P1_endfor          ;i<DIMMATRIX;i++) {
		 mov BYTE[mSecretPlay+DIMMATRIX+rcx], ' '  ;mSecretPlay[1][i]=' ';
		 inc rcx              ;i++;
		 jmp P1_for
	   P1_endfor:             ;}
     P1_endif1:               ;}
     jmp P1_while
   P1_endwhile:               ;}
     
   mov DWORD[rowScreen], 21
     mov DWORD[colScreen], 5
   call gotoxyP1              ;gotoxyP1_C(21,5);
   mov rdi, QWORD[tries]
   add  dil, '0'
   mov BYTE[charac], dil      ;charac = tries + '0';
   call printchP1             ;printchP1_C();
   call printSecretP1         ;printSecretP1_C();
   call printMessageP1        ;printMessage_C();
   
   P1_end:
   ;restaurar l'estat dels registres que s'han guardat a la pila.
   pop rdi
   pop rcx

   mov rsp, rbp
   pop rbp
   ret
