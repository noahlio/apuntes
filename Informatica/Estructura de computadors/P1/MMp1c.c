/**
 * Implementació en C de la pràctica, per a què tingueu una
 * versió funcional en alt nivell de totes les funcions que heu 
 * d'implementar en assemblador.
 * Des d'aquest codi es fan les crides a les subrutines de assemblador. 
 * AQUEST CODI NO ES POT MODIFICAR I NO S'HA DE LLIURAR.
 **/
 
#include <stdio.h>
#include <termios.h>    //termios, TCSANOW, ECHO, ICANON
#include <unistd.h>     //STDIN_FILENO

/**
 * Constants.
 **/
#define DIMMATRIX 5

/**
 * Definició de variables globals.
 **/
extern int developer;	 //Variable declarada en assemblador que indica el nom del programador

char charac;   //Caràcter llegit de teclat i per a escriure a pantalla.
int  rowScreen;//Fila per a posicionar el cursor a la pantalla.
int  colScreen;//Columna per a posicionar el cursor a la pantalla

char mSecretPlay[2][DIMMATRIX] = { {' ',' ',' ',' ',' '},   //Fila 0: Combinació secreta.
                                   {' ',' ',' ',' ',' '} }; //Fila 1: Jugada.
int  col;      //Columna que estem accedint de la matriu.

int  state;    //Estat del joc
               //-1: S'ha premut ESC per sortir   
               // 0: Estem entrant la combinació secreta.
               // 1: Estem entrant la jugada.
               // 2: La combinació secreta té espais o nombres repetits.
               // 3: S'ha guanyat, jugada = combinació secreta.
               // 4: S'han esgotat els intents.
                
long  tries;   //Intents que queden.
short hX;      //Encerts a lloc.


/**
 * Definició de les funcions de C.
 **/
void clearScreen_C();
void gotoxyP1_C();
void printchP1_C();
void getchP1_C();
void printMenuP1_C();
void printBoardP1_C();

void posCurScreenP1_C();
void updateColP1_C();
void updateMatrixBoardP1_C();
void getSecretPlayP1_C();
void printSecretP1_C();
void checkSecretP1_C();
void printHitsP1_C();
void checkPlayP1_C();
void printMessageP1_C();
void playP1_C();

/**
 * Definició de les subrutines d'assemblador que es criden des de C.
 **/
void posCurScreenP1();
void updateColP1();
void updateMatrixBoardP1();
void getSecretPlayP1();
void printSecretP1();
void checkSecretP1();
void printHitsP1();
void checkPlayP1();
void playP1();

/**
 * Esborrar la pantalla
 * 
 * Variables globals utilitzades:   
 * Cap.
 * 
 * Aquesta funció no es crida des d'assemblador
 * i no hi ha definida una subrutina d'assemblador equivalent.
 **/
void clearScreen_C(){
   
    printf("\x1B[2J");
    
}


/**
 * Situar el cursor a la fila indicada per la variable (rowScreen) i a 
 * la columna indicada per la variable (colScreen) de la pantalla.
 * 
 * Variables globals utilitzades:   
 * (rowScreen): Fila de la pantalla on posicionem el cursor.
 * (colScreen): Columna de la pantalla on posicionem el cursor.
 * 
 * S'ha definit un subrutina en assemblador equivalent 'gotoxyP1' per a 
 * poder cridar aquesta funció guardant l'estat dels registres del 
 * processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 **/
void gotoxyP1_C(){
   
   printf("\x1B[%d;%dH",rowScreen,colScreen);
   
}


/**
 * Mostrar un caràcter guardat a la variable (charac) a la pantalla, 
 * en la posició on està el cursor.
 * 
 * Variables globals utilitzades:   
 * (charac): Caràcter que volem mostrar.
 * 
 * S'ha definit un subrutina en assemblador equivalent 'printchP1' per a
 * poder cridar aquesta funció guardant l'estat dels registres del 
 * processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 **/
void printchP1_C(){

   printf("%c",charac);
   
}


/**
 * Llegir una tecla i guardar el caràcter associat a la variable (charac)
 * sense mostrar-lo per pantalla. 
 * 
 * Variables globals utilitzades:   
 * (charac): Caràcter que llegim de teclat.
 * 
 * S'ha definit un subrutina en assemblador equivalent 'getchP1' per a
 * poder cridar aquesta funció guardant l'estat dels registres del 
 * processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 **/
void getchP1_C(){

   static struct termios oldt, newt;

   /*tcgetattr obtenir els paràmetres del terminal
   STDIN_FILENO indica que s'escriguin els paràmetres de l'entrada estàndard (STDIN) sobre oldt*/
   tcgetattr( STDIN_FILENO, &oldt);
   /*es copien els paràmetres*/
   newt = oldt;

   /* ~ICANON per a tractar l'entrada de teclat caràcter a caràcter no com a línia sencera acabada amb /n
      ~ECHO per a què no mostri el caràcter llegit*/
   newt.c_lflag &= ~(ICANON | ECHO);          

   /*Fixar els nous paràmetres del terminal per a l'entrada estàndard (STDIN)
   TCSANOW indica a tcsetattr que canvii els paràmetres immediatament. **/
   tcsetattr( STDIN_FILENO, TCSANOW, &newt);

   /*Llegir un caràcter*/
   charac = (char) getchar();                 
    
   /*restaurar els paràmetres originals*/
   tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
   
}


/**
 * Mostrar a la pantalla el menú del joc i demana una opció.
 * Només accepta una de les opcions correctes del menú ('0'-'8')
 * 
 * Variables globals utilitzades:   
 * (developer):((char;)&developer): variable definida en el codi assemblador.
 * (rowScreen): Fila de la pantalla on posicionem el cursor.
 * (colScreen): Columna de la pantalla on posicionem el cursor.
 * (charac)   : Opció triada del menú, llegida de teclat.
 * 
 * Aquesta funció no es crida des d'assemblador
 * i no hi ha definida una subrutina d'assemblador equivalent.
 **/
void printMenuP1_C(){
	
   clearScreen_C();
   rowScreen = 1;
   colScreen = 1;
   gotoxyP1_C();
   printf("                              \n");
   printf("       Developed by:          \n");
   printf("     ( %s )   \n",(char *)&developer);
   printf(" _____________________________ \n");
   printf("|                             |\n");
   printf("|    MENU MASTERMIND v1.0     |\n");
   printf("|_____________________________|\n");
   printf("|                             |\n");
   printf("|     1.  PosCurScreen        |\n");
   printf("|     2.  UpdateCol           |\n");
   printf("|     3.  UpdateMatrixBoard   |\n");
   printf("|     4.  getSecretPlay       |\n");
   printf("|     5.  PrintSecret         |\n");
   printf("|     6.  CheckSecret         |\n");
   printf("|     7.  PrintHits           |\n");
   printf("|     8.  CheckPlay           |\n");
   printf("|     9.  Play Game           |\n");
   printf("|     0.  Play Game C         |\n");
   printf("|    ESC. Exit game           |\n");
   printf("|                             |\n");
   printf("|         OPTION:             |\n");
   printf("|_____________________________|\n"); 

   charac=' ';
   while (charac!=27 && (charac < '0' || charac > '9')) {
     rowScreen = 21;
     colScreen = 19;
     gotoxyP1_C();
     getchP1_C();
   }
	
}


/**
 * Mostrar el tauler de joc a la pantalla. Les línies del tauler.
 * 
 * Variables globals utilitzades:
 * (rowScreen): Fila de la pantalla on posicionem el cursor.
 * (colScreen): Columna de la pantalla on posicionem el cursor.
 * (tries)    : Intents que queden.
 * 
 * Aquesta funció es crida des de C i des d'assemblador,
 * i no hi ha definida una subrutina d'assemblador equivalent.
 **/
void printBoardP1_C(){
   int i;

   clearScreen_C();
   rowScreen = 1;
   colScreen = 1;
   gotoxyP1_C();
   printf(" _______________________________ \n");//1
   printf("|                               |\n");//2
   printf("|      _ _ _ _ _   Secret Code  |\n");//3
   printf("|_______________________________|\n");//4
   printf("|                 |             |\n");//5
   printf("|       Play      |     Hits    |\n");//6
   printf("|_________________|_____________|\n");//7
   for (i=0;i<tries;i++){                        //8-19
     printf("|   |             |             |\n");
     printf("| %d |  _ _ _ _ _  |  _ _ _ _ _  |\n",i+1);
   }
   printf("|___|_____________|_____________|\n");//20
   printf("|       |                       |\n");//21
   printf("| Tries |                       |\n");//22
   printf("|  ___  |                       |\n");//23
   printf("|_______|_______________________|\n");//24
   printf(" (ENTER) next Try       (ESC)Exit \n");//25
   printf(" (0-9) values    (j)Left (k)Right   ");//26
   
}
   

/**
 * Posiciona el cursor dins al tauler segons la posició del cursor (col),
 * dels intents que queden (tries) i l'estat del joc (state). 
 * Si estem entrant la combinació secreta (state==0) ens posarem a la
 * fila 3 (rowScreen=3), si estem entrant un jugada (state==1) la 
 * fila es calcula amb la fórmula: (rowScreen=9+(DIMMATRIX-tries)*2).
 * La columna es calcula amb la fòrmula (colScreen= 8+(col*2)).
 * Per a posicionar el cursor es crida la funció gotoxyP1_C.
 * 
 * Variables globals utilitzades:
 * (rowScreen): Fila de la pantalla on es situa el cursor.
 * (colScreen): Columna de la pantalla on es situa el cursor.
 * (state)    : Estat del joc.
 * (tries)    : Intents que queden.
 * (col)      : Columna on està el cursor.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'posCurScreenP1'.
 **/
void posCurScreenP1_C(){
	
   if (state==0) {
      rowScreen = 3;
   } else {
      rowScreen = 9+(DIMMATRIX-tries)*2;
   }
   colScreen = 8+(col*2);
   gotoxyP1_C();
   
}


/**
 * Actualitzar la columna (col) on està el cursor.
 * Si s'ha llegit (charac=='j') esquerra o (charac=='k') dreta 
 * actualitzar la posició del cursor (col +/- 1)
 * controlant que no surti del vector [0..DIMMATRIX-1]. 
 *  
 * Variables globals utilitzades:	
 * (charac): Caràcter llegit des del teclat.
 * (col)   : Posició on està el cursor.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'updateColP1'.
 **/
void updateColP1_C(){
	
   if ((charac=='j') && (col>0)){             
      col--;
   }
   if ((charac =='k') && (col<DIMMATRIX-1)){
      col++;
   }
   
}


/**
 * Guardar el caràcter llegit ['0'-'9'] (charac) a la matriu 
 * (mSecretPlay) a la fila indicada per la variable (state) i
 * la columna indicada per la variable (col).
 * Si (state==0) canviarem el caràcter llegit per un '*' (charac='*') 
 * perquè no es vegi la combinació secreta que escrivim.
 * Finalment mostrarem el caràcter (charac) a la pantalla a la posició
 * on està el cursor cridant la funció printchP1_C.
 * 
 * Variables globals utilitzades:	
 * (charac)    : Caràcter a mostrar.
 * (mSecrePlay): Matriu on guardem la combinació secreta i la jugada.
 * (col)       : Columna on està el cursor.
 * (state)     : Estat del joc.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'updateArrayP1'.
 **/
void updateMatrixBoardP1_C(){

   mSecretPlay[state][col] = charac;
   if (state==0) {
      charac='*';
   }    
   printchP1_C();
   
}


/**
 * Llegir els caràcters de la combinació secreta o la jugada.
 * Mentre no es premi ENTER(10) o ESC(27) fer el següent:
 * · Posicionar el cursor a la pantalla cridant la funció posCurScreenP1_C, 
 *   segons el valor de les variables (col, tries i state).
 * · Llegir un caràcter de teclat cridant la funció getchP1_C
 *   que retorna a (charac) el codi ASCII del caràcter llegit.
 *   - Si s'ha llegit una 'j'(esquerra) o una 'k' (dreta) moure el 
 *     cursor per les 5 posicions de la combinació actualitzant 
 *     el valor de la variable (col) cridant la funció updateColP1_C
 *     en funció de les variables (col, tries i state).
 *   - Si s'ha llegit un número ['0'-'9'] el guardem a la matriu
 *     (mSecretPlay) i el mostrem cridant la funció updateMatrixBoardP1_C
 *     en funció de les variables (charac, mSecretPlay, col i state).
 * Si s'ha premut ESC(27) posar (state=-1) per a indicar que hem de sortir.
 * Si es prem ENTER(10) s'acceptarà la combinació tal com estigui.
 * NOTA: Cal tindre en compte que si es prem ENTER sense haver assignat
 * valors a totes les posicions de la combinació, hi haurà posicions
 * que seran un espai (valor utilitzat per inicialitzar la matriu).
 * 
 * Variables globals utilitzades:
 * (charac)     : Caràcter llegit des del teclat.
 * (col)        : Columna on està el cursor.
 * (mSecretPlay): Matriu on guardem la combinació secreta i la jugada.
 * (state)      : Estat del joc.
 * (tries)      : Nombre d'intents que queden.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'getSecretPlayP1'.
 **/
void getSecretPlayP1_C(){
   
   col = 0;
   
   do {
	 posCurScreenP1_C();
	 getchP1_C();
     if (charac=='j' || charac=='k'){             
       updateColP1_C();
     }
     if (charac>='0' && charac<='9'){   
       updateMatrixBoardP1_C();
     }  
   } while (charac!=10 && charac!=27);  

   if (charac == 27) {
     state = -1;    
   }
   
}


/**
 * Verificar que la combinació secreta no tingui el valor inicial (' '),
 * ni números repetits.
 * Per cada element de la fila [0] de la matriu (mSecretPlay) mirar que 
 * no hi hagi un espai (' ') i que no estigui repetit (de la posició 
 * següent a l'actual fins al final). Per a indicar que la combinació
 * secreta no és correcte posarem (secretError=1).
 * Si la combinació secreta no és correcta, posar (state=2) per 
 * indicar-ho.
 * Sinó, la combinació secreta és correcta, posar (state=1) per anar
 * a llegir jugades.
 * 
 * Variables globals utilitzades:
 * (mSecretPlay): Matriu on guardem la combinació secreta i la jugada.
 * (state)      : Estat del joc.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'checkSecretP1'. 
 **/
void checkSecretP1_C() {
   int i,j;
   int secretError = 0;
     
   for (i=0;i<DIMMATRIX;i++) {
     if (mSecretPlay[0][i]==' ') {
       secretError=1;
     }
     for (j=i+1;j<DIMMATRIX;j++) {
       if (mSecretPlay[0][i]==mSecretPlay[0][j]) {
		 secretError=1;
	   }
     }
   }
   
   if (secretError==1) state = 2; 
   else state = 1; 

}


/**
 * Mostrar la combinació secreta del joc.
 * Mostra la combinació secreta (fila 0 de la matriu mSecretPlay) 
 * a la part superior del tauler quan finalitza el joc. 
 * Per a mostrar els valors s'ha de cridar la funció gotoxyP1_C per a 
 * posicionar el cursor, a la fila 3 (rowScreen=3) i a partir de la 
 * columna 8 (colScreen=8) i printchP1_C per a mostrar cada caràcter.
 * Incrementar la columna (colScreen) de 2 en 2. 
 * 
 * Variables globals utilitzades:
 * (rowScreen)  : Fila de la pantalla on es situa el cursor.
 * (colScreen)  : Columna de la pantalla on es situa el cursor.
 * (charac)     : Caràcter llegit des del teclat.
 * (mSecretPlay): Matriu on guardem la combinació secreta i la jugada.
 *  
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'printSecretP1'.  
 **/
void printSecretP1_C() {
	
   int i;
   rowScreen = 3;
   colScreen = 8;
   
   for (i=0; i<DIMMATRIX; i++){
	 gotoxyP1_C();
	 charac = mSecretPlay[0][i];
     printchP1_C(charac);
     colScreen = colScreen + 2;     
   }
   
}


/**
 * Mostrar els encerts al lloc.
 * Situar el cursor en la fila (rowScreen = 9+(DIMMATRIX-tries)*2) i 
 * la columna (colScreen = 22) (part dreta del tauler) per a mostrar 
 * els encerts al tauler de joc.
 * Es mostren els encerts al lloc (hX), tantes 'X' com 
 * encerts al lloc hi hagin.
 * Per a mostrar els encerts s'ha de cridar la funció gotoxyP1_C per a 
 * posicionar el cursor i printchP1_C per a mostrar els caràcters. 
 * Cada cop que es mostra un encert s'ha d'incrimentar 
 * la columna (colScreen) de 2 en 2.
 * NOTA: (hX ha de ser sempre més petit o igual que DIMMATRIX).
 * 
 * Variables globals utilitzades:
 * (rowScreen): Fila de la pantalla on es situa el cursor.
 * (colScreen): Columna de la pantalla on es situa el cursor.
 * (charac)   : Caràcter a mostrar.	
 * (tries)    : Intents que queden.
 * (hX)       : Encerts al lloc.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'printHitsP1'.
 **/ 
void printHitsP1_C() {
   int i;
   
   rowScreen = 9 + (DIMMATRIX-tries)*2;
   colScreen = 22;
   charac = 'X';
   
   for(i=hX;i>0;i--) {
     gotoxyP1_C();
     printchP1_C();
     colScreen = colScreen + 2;
   }
   
}


/**
 * Compta els encerts al lloc de la jugada 
 * respecte de la combinació secreta.
 * Comparar cada element de la combinació secreta  amb
 * l'element que hi ha a la mateixa posició de la jugada.
 * Si un element de la combinació secreta (mSecretPlay[0][i]) és igual
 * a l'element de la mateixa posició de la jugada (mSecretPlay[1][i]))
 * serà un encert al lloc 'X' i s'han d'incrementar els encerts a lloc (hX++).
 * Si totes les posicions de la combinació secreta i de la jugada
 * són iguals (hX=DIMMATRIX) hem guanyat i s'ha de modificar l'estat
 * del joc per a indicar-ho (state=3),
 * sinó, mirar si s'han exhaurit els intents (tries=1) modificar l'estat del
 * joc per a indicar-ho (state=4).
 * Mostrar els encerts al lloc al tauler de joc 
 * cridant la funció printHitsP1_C.
 * 
 * Variables globals utilitzades:	
 * (mSecretPlay): Matriu on guardem la combinació secreta i la jugada
 * (state)      : Estat del joc.
 * (tries)      : Intents que queden.
 * (hX)         : Encerts a lloc.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'checkPlayP1'.
 **/
void checkPlayP1_C(){

   int i;
   hX = 0;
   for (i=0;i<DIMMATRIX;i++) {
	 if (mSecretPlay[0][i]==mSecretPlay[1][i]) {
       hX++;
     } 
   }
    
   if (hX == DIMMATRIX ) {
     state = 3;
   } else if (tries==1) {
		 state = 4;
   }
   
   printHitsP1_C();
   
}


/**
 * Mostra un missatge a la part inferior dreta del tauler segons el 
 * valor de la variable (state).
 * (state) -1: S'ha premut ESC per sortir.
 *          0: Estem entrant la combinació secreta.
 *          1: Estem entrant la jugada.
 *          2: La combinació secreta té espais o valors repetits.
 *          3: S'ha guanyat, jugada = combinació secreta.
 *          4: S'han esgotat els intets.
 * 
 * S'espera que es premi una tecla per continuar. 
 * Mostrar un missatge a sota al tauler per indicar-ho 
 * i al prémer una tecla l'esborra.
 * 
 * Variables globals utilitzades:	
 * (rowScreen): Fila de la pantalla on es situa el cursor.
 * (colScreen): Columna de la pantalla on es situa el cursor.
 * (state)    : Estat del joc.
 * 
 * S'ha definit un subrutina en assemblador equivalent 'printMessageP1' 
 * per a cridar aquesta funció guardant l'estat dels registres del 
 * processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 **/
void printMessageP1_C(){

   rowScreen = 20;
   colScreen = 11;
   gotoxyP1_C();
   switch(state){
     case -1:
       printf(" EXIT: (ESC) PRESSED ");
     break;
     case 0: 
       printf("Write the Secret Code");
     break;
     case 1:
       printf(" Write a combination ");
     break;
     case 2:
       printf("Secret Code ERROR!!! ");
     break;
     case 3:
       printf("YOU WIN: CODE BROKEN!");
     break;
     case 4:
       printf("GAME OVER: No tries! ");
     break;
   }
   rowScreen = 21;
   colScreen = 11;
   gotoxyP1_C(); 
   printf("  Press any key   ");
   getchP1_C();	  
   gotoxyP1_C();  
   printf("                  ");
   
}


/**
 * Funció principal del joc
 * Llegeix la combinació secreta i verifica que sigui correcte.
 * A continuació es llegeix una jugada, compara la jugada amb la
 * combinació secreta per a determinar els encerts a lloc.
 * Repetir el procés mentre no s'encerta la combinació secreta i mentre 
 * queden intents. Si es prem la tecla 'ESC' durant la lectura de la 
 * combinació secreta o d'una jugada sortir.
 * 
 * Pseudo codi:
 * El jugador disposa de 5 intents (tries=5) per encertar
 * la combinació secreta, l'estat inicial del joc és 0 (state=0) i
 * el cursor es posa a la columna 0 (col=0).
 * Mostrar el tauler de joc cridant la funció printBoardP1_C.
 * 
 * Mentre (state==0) llegir la combinació secreta o (state==1) llegir
 * la jugada:
 *   - Mostrar els intents que queden (tries) per a encertar la combinació 
 *     secreta, situar el cursor a la fila 21, columna 5 cridant la funció
 *     gotoxyP1_C i mostra el caràcter associat al valor de la variable.
 *     (tries) sumant '0' i cridant la funció printchP1_C.
 *   - Mostrar un missatge segons estat del joc (state) cridant 
 *     la funció printMessageP1_C.
 *   - Posicionar el cursor al tauler cridant la funció posCurBoardP1_C.
 *   - Llegir els caràcters de la combinació secreta o de la jugada 
 *     i actualitzar l'estat del joc cridant la funció getSecretPlayP1_C.
 *   - Si estem introduïnt la combinació secreta (state==0) verificar
 *     que es correcte cridant la funció checkSecretP1_C.
 *     Sino, si estem introduïnt la jugada (state==1) verificar 
 *     els encerts de la jugada cridant la funció checkPlayP1_C,
 *     decrementar els intents (tries). Inicialitzar la jugada que
 *     tenim guardada a la fila 1 de la matriu mSecretPlay amb 
 *     espais (' ') per poder introduir una nova jugada.
 * 
 * Per a acabar, mostrar els intents que queden (tries) per a 
 * encertar la combinació secreta, situar el cursor a la fila 21, 
 * columna 5 cridant la funció gotoxyP1_C i mostra el caràcter associat 
 * al valor de la variable (tries) sumant '0' i cridant a la funció 
 * printchP1_C, mostrar la combinació secreta cridant la funció 
 * printSecretP1_C i finalment mostrar un missatge segons l'estat del 
 * joc (state) cridant la funció printMessageP1_C.
 * S'acaba el joc.
 * 
 * Variables globals utilitzades:
 * (col)        : Posició on està el cursor.
 * (state)      : Estat del joc.
 * (tries)      : Intents que queden.
 * (rowScreen)  : Fila de la pantalla on es situa el cursor.
 * (colScreen)  : Columna de la pantalla on es situa el cursor.
 * (charac)     : Caràcter llegit des del teclat i a mostrar.
 * (mSecretPlay): Matriu on guardem la combinació secreta i la jugada.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'playP1'. 
 **/
void playP1_C() {
	
   col=0;
   state = 0;
   tries = 5;
   
   printBoardP1_C();
   
   int i;
     
   while (state == 0 || state == 1) {
	 rowScreen=21;
     colScreen=5;  
	 gotoxyP1_C();
	 charac = (char)tries + '0';
     printchP1_C();
     printMessageP1_C();
     posCurScreenP1_C();
     
	 getSecretPlayP1_C();
	 if (state==0) {
	   checkSecretP1_C();
     } else {
       if (state==1) {
	     checkPlayP1_C();
	     tries --;
	   }
	   for (i=0;i<DIMMATRIX;i++) {
		   mSecretPlay[1][i]=' ';
	   }
	 }
     
   }
   
   rowScreen=21;
   colScreen=5;
   gotoxyP1_C();
   charac=tries + '0';
   printchP1_C();
   printSecretP1_C();
   printMessageP1_C();
   
}


/**
 * Programa Principal
 * 
 * ATENCIÓ: Podeu provar la funcionalita de les subrutines que s'han de
 * desenvolupar treient els comentaris de la crida a la funció 
 * equivalent implementada en C que hi ha sota a cada opció.
 * Per al joc complet hi ha una opció per la versió en assemblador i 
 * una opció pel joc en C.
 **/
void main(void){   
   int i;
   int op=' ';      

   while (op!=27) {
     printMenuP1_C();	  //Mostrar menú i retornar opció.
     op = charac;
     switch(op){
       case 27:
         rowScreen=23;
         colScreen=1;
         gotoxyP1_C(); 
         break;
       case '1':	          //Posiciona Cursor al Tauler.
         state=1;
         tries=5;
         col = 0;		
         printBoardP1_C();
         rowScreen=21;
         colScreen=11;
         gotoxyP1_C();
         printf("   Press any key  ");   
         //=======================================================
         posCurScreenP1();
         //posCurScreenP1_C();
         //=======================================================
         getchP1_C();
         break;
       case '2':	          //Actualitzar la columna del cursor.
         state=1;
         tries=5;
	     col = 4;
         printBoardP1_C();
         rowScreen=21;
         colScreen=11;  
         gotoxyP1_C();
         printf(" Press 'j' or 'k' ");
         posCurScreenP1_C();
         getchP1_C();
         if (charac=='j' || charac=='k') {
         //=======================================================
         updateColP1();
         //updateColP1_C();	    
         //=======================================================
         }
         rowScreen=21;
         colScreen=11;
         gotoxyP1_C();
         printf("   Press any key  ");
         posCurScreenP1_C();
         getchP1_C();
         break;
       case '3': 	     //Actualitzar array i mostrar-ho per pantalla.
         state=0;
         tries=5;
         col=2;		  
         printBoardP1_C();
         printSecretP1_C();
         rowScreen=21;
         colScreen=11;
         gotoxyP1_C();
         printf(" Press (0-9) value ");
         posCurScreenP1_C();
         getchP1_C();
         if (charac>='0' && charac<='9'){       
         //=======================================================
         updateMatrixBoardP1();
         //updateMatrixBoardP1_C();
         //=======================================================
         }
         rowScreen=20;
         colScreen=11;
         gotoxyP1_C();
         printf("   To show Secret  "); 
         rowScreen=21;
         colScreen=11;
         gotoxyP1_C();
         printf("   Press any key   ");
         getchP1_C();
         printSecretP1_C();
         rowScreen=20;
         colScreen=11;
         gotoxyP1_C();
         printf("                   "); 
         rowScreen=21;
         colScreen=11;
         gotoxyP1_C();
         printf("   Press any key  ");
         getchP1_C();
         break;
       case '4': 	     //Llegir la combinació secreta o la jugada
         state=0;
         tries=5;
         col=0;
         for (i=0;i<DIMMATRIX;i++) {
		   mSecretPlay[state][i]=' ';
	     }
         printBoardP1_C();
         printMessageP1_C();	
         //=======================================================
         getSecretPlayP1();
         //getSecretPlayP1_C();
         //=======================================================
         printSecretP1_C();		
         checkSecretP1_C();
         printMessageP1_C();	
         break;
       case '5': 	     //Mostrar la combinació secreta.
         state=0;
         tries=5;
         col=0;
         printBoardP1_C();
         //=======================================================
         printSecretP1();
         //printSecretP1_C();		
         //=======================================================
         rowScreen=21;
         colScreen=11;
         gotoxyP1_C();
         printf("   Press any key  ");
         getchP1_C();
         break; 
       case '6': 	     //Verificar la combinació secreta.
         state=0;
         tries=5;
         col=0;	
         printBoardP1_C();
         //=======================================================
         checkSecretP1();
         //checkSecretP1_C();
         //=======================================================
         printSecretP1_C();		
         printMessageP1_C();
         break;
       case '7': 	     //Mostrar encerts.
         state=1;
         tries=5;
         col=0;
         printBoardP1_C();
         hX = 4;
         //=======================================================
         printHitsP1();
         //printHitsP1_C();
         //=======================================================
         rowScreen=21;
         colScreen=11;
         gotoxyP1_C();
         printf("   Press any key  ");
         getchP1_C();
         break;
       case '8': 	     //Compta els encerts a lloc.
         state=0;
         tries=5;
         col=0;
         printBoardP1_C();
         char  msecretplay2[2][DIMMATRIX] = {{'1','2','3','4','5'}, //Combinació secreta
                                             {'1','4','3','1','4'}};//Jugada 
         for (i=0;i<DIMMATRIX;i++) {
           mSecretPlay[0][i]=msecretplay2[0][i];
           mSecretPlay[1][i]=msecretplay2[1][i];
         } 
         printSecretP1_C();
         state=1;
         rowScreen = 9+(DIMMATRIX-tries)*2;
         colScreen = 8;
         for (i=0; i<DIMMATRIX; i++){
	       gotoxyP1_C();
	       charac = mSecretPlay[1][i];
           printchP1_C();
           colScreen = colScreen + 2;     
         }
         //=======================================================
         checkPlayP1();
         //checkPlayP1_C();
         //=======================================================
         for (i=0;i<DIMMATRIX;i++) {
           mSecretPlay[0][i]=' ';
           mSecretPlay[1][i]=' ';
         } 
         printMessageP1_C();
         break;
       case '9': 	     //Joc complet en assemblador.
         i=0;
         for (i=0;i<DIMMATRIX;i++) {
           mSecretPlay[0][i]=' ';
           mSecretPlay[1][i]=' ';
         } 
         //=======================================================
         playP1();
         //=======================================================
         break;
       case '0': 	     //Joc complet en C
         i=0;
         for (i=0;i<DIMMATRIX;i++) {
           mSecretPlay[0][i]=' ';
           mSecretPlay[1][i]=' ';
         } 
         //=======================================================
         playP1_C();
         //=======================================================
         break;
     }
   }

}
