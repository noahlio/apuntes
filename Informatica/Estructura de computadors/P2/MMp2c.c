/**
 * Implementació en C de la pràctica, per a què tingueu una
 * versió funcional en alt nivell de totes les funcions que heu 
 * d'implementar en assemblador.
 * Des d'aquest codi es fan les crides a les subrutines de assemblador. 
 * AQUEST CODI NO ES POT MODIFICAR I NO S'HA DE LLIURAR.
 **/
 
#include <stdio.h>
#include <termios.h>    //termios, TCSANOW, ECHO, ICANON
#include <unistd.h>     //STDIN_FILENO

/**
 * Constants.
 **/
#define DIMMATRIX 5

/**
 * Definició de variables globals.
 **/
 extern int developer;	//Variable declarada en assemblador que indica el nom del programador

/**
 * Definició de les funcions de C.
 **/
void  clearScreen_C();
void  gotoxyP2_C(int, int);
void  printchP2_C(char);
char  getchP2_C();
char  printMenuP2_C();
void  printBoardP2_C(long);

void  posCurScreenP2_C(int, long, int);
int   updateColP2_C(char, int);
void  updateMatrixBoardP2_C(char, char[2][DIMMATRIX], int, int);
int   getSecretPlayP2_C(char[2][DIMMATRIX], int, long);
void  printSecretP2_C(char[2][DIMMATRIX]);
int   checkSecretP2_C(char[2][DIMMATRIX]);
void  printHitsP2_C(short, short, long);
int   checkPlayP2_C(char[2][DIMMATRIX], int, long);

void  printMessageP2_C(int);
void  playP2_C(char[2][DIMMATRIX]);

/**
 * Definició de les subrutines d'assemblador que es criden des de C.
 **/
void  posCurScreenP2(int, long, int);
int   updateColP2(char, int);
void  updateMatrixBoardP2(char, char[2][DIMMATRIX], int, int);
int   getSecretPlayP2(char[2][DIMMATRIX], int, long);
void  printSecretP2(char[2][DIMMATRIX]);
int   checkSecretP2(char[2][DIMMATRIX]);
void  printHitsP2(short, short, long);
int   checkPlayP2(char[2][DIMMATRIX], int, long);
void  playP2(char[2][DIMMATRIX]);


/**
 * Esborrar la pantalla
 * 
 * Variables globals utilitzades:   
 * Cap.
 * 
 * Paràmetres d'entrada : 
 * Cap.
 *   
 * Paràmetres de sortida: 
 * Cap.
 * 
 * Aquesta funció no es crida des d'assemblador
 * i no hi ha definida una subrutina d'assemblador equivalent.
 **/
void clearScreen_C(){
   
    printf("\x1B[2J");
    
}


/**
 * Situar el cursor en una fila i una columna de la pantalla
 * en funció de la fila (rowScreen) i de la columna (colScreen) 
 * rebuts com a paràmetre.
 * 
 * Variables globals utilitzades:   
 * Cap.
 * 
 * Paràmetres d'entrada : 
 * (rowScreen): rdi(edi): Fila
 * (colScreen): rsi(esi): Columna
 * 
 * Paràmetres de sortida: 
 * Cap.
 * 
 * S'ha definit un subrutina en assemblador equivalent 'gotoxyP2' 
 * per a poder cridar aquesta funció guardant l'estat dels registres 
 * del processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 * El pas de paràmetres és equivalent.
 **/
void gotoxyP2_C(int rowScreen, int colScreen){
   
   printf("\x1B[%d;%dH",rowScreen,colScreen);
   
}


/**
 * Mostrar un caràcter (c) a la pantalla, rebut com a paràmetre, 
 * en la posició on està el cursor.
 * 
 * Variables globals utilitzades:   
 * Cap.
 * 
 * Paràmetres d'entrada : 
 * (c): rdi(dil): Caràcter que volem mostrar
 * 
 * Paràmetres de sortida: 
 * Cap.
 * 
 * S'ha definit un subrutina en assemblador equivalent 'printchP2' 
 * per a cridar aquesta funció guardant l'estat dels registres del 
 * processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 * El pas de paràmetres és equivalent.
 **/
void printchP2_C(char c){
   
   printf("%c",c);
   
}


/**
 * Llegir una tecla i retornar el caràcter associat 
 * sense mostrar-lo per pantalla. 
 * 
 * Variables globals utilitzades:   
 * Cap.
 * 
 * Paràmetres d'entrada : 
 * Cap.
 * 
 * Paràmetres de sortida: 
 * (c): rax(al): Caràcter llegit de teclat
 * 
 * S'ha definit un subrutina en assemblador equivalent 'getchP2' 
 * per a cridar aquesta funció guardant l'estat dels registres del 
 * processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 * El pas de paràmetres és equivalent.
 **/
char getchP2_C(){

   int c;   

   static struct termios oldt, newt;

   /*tcgetattr obtenir els paràmetres del terminal
   STDIN_FILENO indica que s'escriguin els paràmetres de l'entrada estàndard (STDIN) sobre oldt*/
   tcgetattr( STDIN_FILENO, &oldt);
   /*es copien els paràmetres*/
   newt = oldt;

   /* ~ICANON per a tractar l'entrada de teclat caràcter a caràcter no com a línia sencera acabada amb /n
      ~ECHO per a què no mostri el caràcter llegit*/
   newt.c_lflag &= ~(ICANON | ECHO);          

   /*Fixar els nous paràmetres del terminal per a l'entrada estàndard (STDIN)
   TCSANOW indica a tcsetattr que canvii els paràmetres immediatament.*/
   tcsetattr( STDIN_FILENO, TCSANOW, &newt);

   /*Llegir un caràcter*/
   c=getchar();                 
    
   /*restaurar els paràmetres originals*/
   tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

   /*Retornar el caràcter llegit*/
   return (char)c;
   
}



/**
 * Mostrar a la pantalla el menú del joc i demana una opció.
 * Només accepta una de les opcions correctes del menú ('0'-'8')
 * 
 * Variables globals utilitzades:   
 * developer:((char;)&developer): variable definida en el codi assemblador.
 * 
 * Paràmetres d'entrada : 
 * Cap.
 * 
 * Paràmetres de sortida: 
 * (charac): rax(al): Opció triada del menú, llegida de teclat.
 * 
 * Aquesta funció no es crida des d'assemblador
 * i no hi ha definida una subrutina d'assemblador equivalent.
 **/
char printMenuP2_C(){
	
   clearScreen_C();
   gotoxyP2_C(1,1);
   printf("                               \n");
   printf("         Developed by:         \n");
   printf("     ( %s )    \n",(char *)&developer);
   printf(" _____________________________ \n");
   printf("|                             |\n");
   printf("|    MENU MASTERMIND v2.0     |\n");
   printf("|_____________________________|\n");
   printf("|                             |\n");
   printf("|     1.  PosCurScreen        |\n");
   printf("|     2.  UpdateCol           |\n");
   printf("|     3.  UpdateMatrixBoard   |\n");
   printf("|     4.  getSecretPlay       |\n");
   printf("|     5.  PrintSecret         |\n");
   printf("|     6.  CheckSecret         |\n");
   printf("|     7.  PrintHits           |\n");
   printf("|     8.  CheckPlay           |\n");
   printf("|     9.  Play Game           |\n");
   printf("|     0.  Play Game C         |\n");
   printf("|    ESC. Exit game           |\n");
   printf("|                             |\n");
   printf("|         OPTION:             |\n");
   printf("|_____________________________|\n"); 
   
   char charac =' ';
   while (charac!=27 && (charac < '0' || charac > '9')) {
      gotoxyP2_C(21,19);      
      charac = getchP2_C();   
   }
   return charac;
   
}


/**
 * Mostrar el tauler de joc a la pantalla. Les línies del tauler.
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (tries): rdi(rdi): Intents que queden.
 * 
 * Paràmetres de sortida: 
 * Cap
 * 
 * Aquesta funció es crida des de C i des d'assemblador,
 * i no hi ha definida una subrutina d'assemblador equivalent.
 **/
void printBoardP2_C(long tries){
   int i;

   clearScreen_C();
   gotoxyP2_C(1,1);
   printf(" _______________________________ \n");//1
   printf("|                               |\n");//2
   printf("|      _ _ _ _ _   Secret Code  |\n");//3
   printf("|_______________________________|\n");//4
   printf("|                 |             |\n");//5
   printf("|       Play      |     Hits    |\n");//6
   printf("|_________________|_____________|\n");//7
   for (i=0;i<tries;i++){                        //8-19
     printf("|   |             |             |\n");
     printf("| %d |  _ _ _ _ _  |  _ _ _ _ _  |\n",i+1);
   }
   printf("|___|_____________|_____________|\n");//20
   printf("|       |                       |\n");//21
   printf("| Tries |                       |\n");//22
   printf("|  ___  |                       |\n");//23
   printf("|_______|_______________________|\n");//24
   printf(" (ENTER) next Try       (ESC)Exit \n");//25
   printf(" (0-9) values    (j)Left (k)Right   ");//26
   
}


/**
 * Posiciona el cursor dins al tauler segons la posició del cursor (col),
 * dels intents que queden (tries) i l'estat del joc (state). 
 * Si estem entrant la combinació secreta (state==0) ens posarem a la
 * fila 3 (rowScreen=3), si estem entrant un jugada (state==1) la 
 * fila es calcula amb la fórmula: (rowScreen=9+(DIMMATRIX-tries)*2).
 * La columna es calcula amb la fòrmula (colScreen= 8+(col*2)).
 * Per a posicionar el cursor es crida la funció gotoxyP2_C.
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (state):rdi(edi):  Estat del joc.
 * (tries):rsi(rsi):  Intents que queden.
 * (col)  :rdx(edx):  Columna on està el cursor.
 * 
 * Paràmetres de sortida: 
 * Cap
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'posCurScreenP2',  
 * el pas de paràmetres és equivalent.
 **/
void posCurScreenP2_C(int state, long tries, int col){
   int rowScreen, colScreen;
   if (state==0) {
      rowScreen = 3;
   } else {
      rowScreen = 9+(DIMMATRIX-tries)*2;
   }
   colScreen = 8+(col*2);
   gotoxyP2_C(rowScreen, colScreen);
}


/**
 * Actualitzar la columna (col) on està el cursor.
 * Si s'ha llegit (charac=='j') esquerra o (charac=='k') dreta 
 * actualitzar la posició del cursor (col +/- 1)
 * controlant que no surti del vector [0..DIMMATRIX-1]. 
 * Retornar el valor actualitzat de (col).
 *  
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (charac): rdi(dil): Caràcter llegit des del teclat.
 * (col)   : rsi(esi): Columna on està el cursor.
 * 
 * Paràmetres de sortida: 
 * (col)   : rax(eax):  Columna on està el cursor actualitzada.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'updateColP2',  
 * el pas de paràmetres és equivalent.
 **/
int updateColP2_C(char charac, int col){
	
   if ((charac=='j') && (col>0)){             
      col--;
   }
   if ((charac=='k') && (col<DIMMATRIX-1)){
      col++;
   }
   return col;
}


/**
 * Guardar el caràcter llegit ['0'-'9'] (charac) a la matriu 
 * (mSecretPlay) a la fila indicada per la variable (state) i
 * la columna indicada per la variable (col).
 * Si (state==0) canviarem el caràcter llegit per un '*' (charac='*') 
 * perquè no es vegi la combinació secreta que escrivim.
 * Finalment mostrarem el caràcter (charac) a la pantalla a la posició
 * on està el cursor cridant la funció printchP2_C.
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (charac)    : rdi(dil): Caràcter a mostrar.
 * (mSecrePlay): rsi(rsi): Adreça de la matriu on guardem la combinació secreta i la jugada.
 * (col)       : rdx(edx): Columna on està el cursor.
 * (state)     : rcx(ecx): Estat del joc.
 * 
 * Paràmetres de sortida: 
 * Cap
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'updateMatrixBoardP2',  
 * el pas de paràmetres és equivalent.
 **/
void updateMatrixBoardP2_C(char charac, char mSecretPlay[2][DIMMATRIX], int col, int state){

   mSecretPlay[state][col]=charac;   
   if (state==0) {
      charac='*';
   } 
   printchP2_C(charac);
}


/**
 * Llegir els caràcters de la combinació secreta o la jugada.
 * Mentre no es premi ENTER(10) o ESC(27) fer el següent:
 * · Posicionar el cursor a la pantalla cridant la funció posCurScreenP2_C, 
 *   segons el valor de les variables (col, tries i state).
 * · Llegir un caràcter de teclat cridant la funció getchP2_C
 *   que retorna a (charac) el codi ASCII del caràcter llegit.
 *   - Si s'ha llegit una 'j'(esquerra) o una 'k' (dreta) moure el 
 *     cursor per les 5 posicions de la combinació actualitzant 
 *     el valor de la variable (col) cridant la funció updateColP2_C
 *     en funció de les variables(col, tries i state).
 *   - Si s'ha llegit un número ['0'-'9'] el guardem a la matriu
 *     (mSecretPlay) i el mostrem cridant la funció updateMatrixBoardP2_C
 *     en funció de les variables (charac, mSecretPlay, col i state).
 * Si s'ha premut ESC(27) posar (state=-1) per a indicar que hem de sortir.
 * Retornar l'estat actual del joc.
 * Si es prem ENTER(10) s'acceptarà la combinació tal com estigui.
 * NOTA: Cal tindre en compte que si es prem ENTER sense haver assignat
 * valors a totes les posicions de la combinació, hi haurà posicions 
 * que seran un espai (valor utilitzat per inicialitzar la matriu).
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
 * (state)      : rsi(esi): Estat del joc.
 * (tries)      : rdx(rdx): Nombre d'intents que queden.
 * 
 * Paràmetres de sortida: 
 * (state)      : rax(eax): Estat del joc.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'getSecretPlayP2'.
 **/
int getSecretPlayP2_C(char mSecretPlay[2][DIMMATRIX], int state, long tries ){
	
   int  rowScreen, colScreen;
   char charac;
   int  col = 0;
   
   do {
	 posCurScreenP2_C(state, tries, col);
	 charac = getchP2_C();
     if (charac=='j' || charac=='k'){             
       col = updateColP2_C(charac, col);
     } else if (charac>='0' && charac<='9') {   
       updateMatrixBoardP2_C(charac, mSecretPlay, col, state);
     }  
   } while (charac!=10 && charac!=27);  

   if (charac == 27) {
     state = -1;    
   }
   
   return state;
}


/**
 * Verificar que la combinació secreta no tingui el valor inicial (' '),
 * ni números repetits.
 * Per cada element de la fila [0] de la matriu (mSecretPlay) mirar que 
 * no hi hagi un espai (' ') i que no estigui repetit (de la posició 
 * següent a l'actual fins al final). Per a indicar que la combinació
 * secreta no és correcte posarem (secretError=1).
 * Si la combinació secreta no és correcta, posar (state=2) per 
 * indicar-ho.
 * Sinó, la combinació secreta és correcta, posar (state=1) per anar
 * a llegir jugades.
 * Retornar l'estat actual del joc (state).
 * 
 * Variables globals utilitzades:
 * Cap	
 * 
 * Paràmetres d'entrada : 
 * (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
 * 
 * Paràmetres de sortida: 
 * (state)      : rax(eax): Estat del joc.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'checkSecretP2',  
 * el pas de paràmetres és equivalent. 
 **/
int checkSecretP2_C(char mSecretPlay[2][DIMMATRIX]) {
   int i,j;
   int secretError = 0;
   int state;
     
   for (i=0;i<DIMMATRIX;i++) {
     if (mSecretPlay[0][i]==' ') {
       secretError=1;
     }
     for (j=i+1;j<DIMMATRIX;j++) {
       if (mSecretPlay[0][i]==mSecretPlay[0][j]) {
		 secretError=1;
	   }
     }
   }
   
   if (secretError==1) state = 2; 
   else state = 1; 

   return state;

}


/**
 * Mostrar la combinació secreta del joc.
 * Mostra la combinació secreta (fila 0 de la matriu mSecretPlay) 
 * a la part superior del tauler quan finalitza el joc.
 * Per a mostrar els valors s'ha de cridar la funció gotoxyP2_C per a 
 * posicionar el curso, a la fila 3 (rowScreen=3) i a partir de la 
 * columna 8 (colScreen=8) i printchP2_C per a mostrar cada caràcter.
 * Incrementar la columna (colScreen) de 2 en 2. 
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
 * 
 * Paràmetres de sortida: 
 * Cap
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'printSecretP2',  
 * el pas de paràmetres és equivalent.  
 **/
void printSecretP2_C(char mSecretPlay[2][DIMMATRIX]) {
	
   int  i;
   char charac;
   int  rowScreen = 3;
   int  colScreen = 8;
   
   for (i=0; i<DIMMATRIX; i++){
	 gotoxyP2_C(rowScreen, colScreen);
	 charac = mSecretPlay[0][i];
     printchP2_C(charac);
     colScreen = colScreen + 2;     
   } 
   
}


/**
 * Mostrar els encerts al lloc i fora de lloc.
 * Situar el cursor en la fila (rowScreen = 9+(DIMMATRIX-tries)*2) i 
 * la columna (colScreen = 22) (part dreta del tauler) per a mostrar 
 * els encerts al tauler de joc.
 * Primer es mostren els encerts al lloc (hX), tantes 'X' com 
 * encerts al lloc hi hagin i després tantes 'O' com 
 * encerts fora de lloc (hO) hi hagin.
 * Per a mostrar els encerts s'ha de cridar la funció gotoxyP2_C per a 
 * posicionar el cursor i printchP2_C per a mostrar els caràcters. 
 * Cada cop que es mostra un encert s'ha d'incrimentar 
 * la columna (colScreen) de 2 en 2.
 * NOTA: (hX + hO ha de ser sempre més petit o igual que DIMMATRIX).
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (hX)   : rdi(di) : Encerts al lloc.
 * (hO)   : rsi(si) : Encerts fora de lloc.
 * (tries): rdx(rdx): Intents que queden.
 * 
 * Paràmetres de sortida: 
 * Cap
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'printHitsP2',  
 * el pas de paràmetres és equivalent.
 **/ 
void printHitsP2_C(short hX, short hO, long tries) {
   int i;
   int rowScreen = 9 + (DIMMATRIX-tries)*2;
   int colScreen = 22;
   
   for(i=hX;i>0;i--) {
     gotoxyP2_C(rowScreen,colScreen);
     printchP2_C('X');
     colScreen = colScreen + 2;
   }
   for(i=hO;i>0;i--) {
     gotoxyP2_C(rowScreen,colScreen);
     printchP2_C('O');
     colScreen = colScreen + 2;
   }
}


/**
 * Comptar els encerts al lloc i fora de lloc de la jugada
 * respecte de la combinació secreta.
 * Per a fer la comparació cal fer el següent:
 * Per cada element de la combinació secreta (fila 0 de la matriu mSecretPlay)
 * (s'utilitza de referència perquè no té valors repetits). 
 * Comparar-lo amb tots els elements de la jugada (fila 1 de la matriu mSecretPlay).
 * Si un element de la combinació secreta (mSecretPlay[0][i]) és igual a un 
 * l'element de la mateixa posició de la jugada (mSecretPlay[1][i]) serà un 
 * encert al lloc 'X' i s'han d'incrementar els encerts a lloc (hX++),
 * sinó, serà un encert fora de lloc 'O' si un element de la combinació
 * secreta (mSecretPlay[0][i]) és igual a un element de la jugada 
 * (mSecretPlay[1][j]), però ocupen posicions diferents (i!=j), 
 * s'han d'incrementar els encer fora de lloc (hO++) i deixar de 
 * recórrer la jugada per comptar cada encert un sol cop.
 * Si totes les posicions de la combinació secreta i de la jugada
 * són iguals (hX=DIMMATRIX) hem guanyat i s'ha de modificar l'estat
 * del joc per a indicar-ho (state=3),
 * sinó, mirar si s'han exhaurit els intents (tries==1), si s'han
 * exhaurit modificar l'estat del joc per a indicar-ho (state=4).
 * Mostrar els encerts al lloc i fora de lloc al tauler de joc 
 * cridant la funció printHitsP2_C.
 * Retornar l'estat actual del joc (state).
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
 * (state)      : rsi(esi): Estat del joc.
 * (tries)      : rdx(rdx): Intents que queden.
 * 
 * Paràmetres de sortida: 
 * (state)      : rax(eax): Estat del joc.
 * 
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'checkPlayP2',  
 * el pas de paràmetres és equivalent.
 **/
int checkPlayP2_C(char mSecretPlay[2][DIMMATRIX], int state, long tries){
   int i,j;
   short hO = 0;
   short hX = 0;
   
   for (i=0;i<DIMMATRIX;i++) {
	 if (mSecretPlay[0][i]==mSecretPlay[1][i]) {
       hX++;
     } else {
       for (j=0;j<DIMMATRIX;j++) {
         if (mSecretPlay[0][i]==mSecretPlay[1][j]) {
           hO++;
           j=DIMMATRIX;  
         }
       }
     }
   }
    
   if (hX == DIMMATRIX ) {
     state = 3;
   } else if (tries==1) {
		 state = 4;
   }
   printHitsP2_C(hX, hO, tries);
   
   return state;
}


/**
 * Mostra un missatge a la part inferior dreta del tauler segons el 
 * valor de la variable (state).
 * (state) -1: S'ha premut ESC per sortir
 *          0: Estem entrant la combinació secreta.
 *          1: Estem entrant la jugada.
 *          2: La combinació secreta té espais o valors repetits.
 *          3: S'ha guanyat, jugada = combinació secreta.
 *          4: S'han esgotat els intents.
 *          
 * S'espera que es premi una tecla per continuar.
 * Mostrar un missatge a sota al tauler per indicar-ho 
 * i al prémer una tecla l'esborra.
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (state): rdi(edi): Estat del joc.
 * 
 * Paràmetres de sortida: 
 * Cap
 *  
 * S'ha definit un subrutina en assemblador equivalent 'printMessageP2' 
 * per a cridar aquesta funció guardant l'estat dels registres del 
 * processador. Això es fa perquè les funcions de C no mantenen 
 * l'estat dels registres.
 * El pas de paràmetres és equivalent.
 **/
void printMessageP2_C(int state){

   gotoxyP2_C(20,11);
   switch(state){
	 case -1:
       printf(" EXIT: (ESC) PRESSED ");
     break;
     case 0: 
       printf("Write the Secret Code");
     break;
     case 1:
       printf(" Write a combination ");
     break;
     case 2:
       printf("Secret Code ERROR!!! ");
     break;
     case 3:
       printf("YOU WIN: CODE BROKEN!");
     break;
     case 4:
       printf("GAME OVER: No tries! ");
     break;
   }
   gotoxyP2_C(21,11); 
   printf("    Press any key ");
   getchP2_C();	  
   gotoxyP2_C(21,11);  
   printf("                  ");
   
}


/**
 * Funció principal del joc
 * Llegeix la combinació secreta i verifica que sigui correcte.
 * A continuació es llegeix una jugada, compara la jugada amb la
 * combinació secreta per a determinar els encerts.
 * Repetir el procés mentre no s'encerta la combinació secreta i mentre 
 * mentre queden intents. Si es prem la tecla 'ESC' durant la lectura
 * de la combinació secreta o d'una jugada sortir.
 * 
 * Pseudo codi:
 * El jugador disposa de 5 intents (tries=5) per encertar
 * la combinació secreta, l'estat inicial del joc és 0 (state=0) i
 * el cursor es posa a la columna 0 (col=0).
 * Mostrar el tauler de joc cridant la funció printBoardP2_C.
 * 
 * Mentre (state==0) llegir la combinació secreta o (state==1) llegir
 * la jugada:
 *   - Mostrar els intents que queden (tries) per a encertar la combinació 
 *     secreta, situar el cursor a la fila 21, columna 5 cridant la funció
 *     gotoxyP2_C i mostra el caràcter associat al valor de la variable.
 *     (tries) sumant '0' i cridant la funció printchP2_C.
 *   - Mostrar un missatge segons estat del joc (state) cridant 
 *     la funció printMessageP2_C.
 *   - Posicionar el cursor al tauler cridant la funció posCurBoardP2_C.
 *   - Llegir els caràcters de la combinació secreta o de la jugada 
 *     i actualitzar l'estat del joc cridant la funció getSecretPlayP2_C.
 *   - Si estem introduïnt la combinació secreta (state==0) verificar
 *     que es correcte cridant la funció checkSecretP2_C.
 *     Sino, si estem introduïnt la jugada (state==1) verificar 
 *     els encerts de la jugada cridant la funció checkPlayP2_C,
 *     decrementar els intents (tries). Inicialitzar la jugada que
 *     tenim guardada a la fila 1 de la matriu mSecretPlay amb 
 *     espais (' ') per poder introduir una nova jugada.
 * 
 * Per a acabar, mostrar els intents que queden (tries) per a 
 * encertar la combinació secreta, situar el cursor a la fila 21, 
 * columna 5 cridant la funció gotoxyP2_C i mostra el caràcter associat 
 * al valor de la variable (tries) sumant '0' i cridant a la funció 
 * printchP2_C, mostrar la combinació secreta cridant la funció 
 * printSecretP2_C i finalment mostrar un missatge segons l'estat del 
 * joc (state) cridant la funció printMessageP2_C.
 * S'acaba el joc.
 * 
 * Variables globals utilitzades:	
 * Cap
 * 
 * Paràmetres d'entrada : 
 * (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
 * 
 * Paràmetres de sortida: 
 * Cap
 *  
 * Aquesta funció no es crida des d'assemblador.
 * Hi ha un subrutina en assemblador equivalent 'playP2',  
 * el pas de paràmetres és equivalent.
 **/
void playP2_C(char mSecretPlay[2][DIMMATRIX]) {
	
   int  col = 0;    //Columna que estem accedint de la matriu.

   int  state = 0;  //Estat del joc
                    //-1: S'ha premut ESC per sortir   
                    // 0: Estem entrant la combinació secreta.
                    // 1: Estem entrant la jugada.
                    // 2: La combinació secreta té espais o valors repetits.
                    // 3: S'ha guanyat, jugada = combinació secreta.
                    // 4: S'han esgotat els intents

   long  tries = 5; //Intents que queden.
   
   printBoardP2_C(tries);
           
   int   i;

   while (state == 0 || state == 1) {
	   
	 gotoxyP2_C(21,5);
     printchP2_C((char)tries + '0');
     printMessageP2_C(state);
     posCurScreenP2_C(state, tries, col);
     
	 state = getSecretPlayP2_C(mSecretPlay, state, tries);
	 if (state==0) {
	   state = checkSecretP2_C(mSecretPlay);
     } else {
       if (state==1) {
	     state = checkPlayP2_C(mSecretPlay, state, tries);
	     tries --;
	   }
	   for (i=0;i<DIMMATRIX;i++) {
		   mSecretPlay[1][i]=' ';
	   }
	 }
     
   }
   gotoxyP2_C(21,5);
   printchP2_C((char)tries + '0');
   printSecretP2_C(mSecretPlay);
   printMessageP2_C(state);
   
}


/**
 * Programa Principal
 * 
 * ATENCIÓ: Podeu provar la funcionalita de les subrutines que s'han de
 * desenvolupar treient els comentaris de la crida a la funció 
 * equivalent implementada en C que hi ha sota a cada opció.
 * Per al joc complet hi ha una opció per la versió en assemblador i 
 * una opció pel joc en C.
 **/
void main(void){   
   /**
   * Variables Locals del joc
   **/
   char c;          //Caràcter llegit de teclat i per a escriure a pantalla.
   int i;
   //int  rowScreen;  //Fila per a posicionar el cursor a la pantalla.
   //int  colScreen;  //Columna per a posicionar el cursor a la pantalla

   char mSecretPlay[2][DIMMATRIX] = { {' ',' ',' ',' ',' '},   //Fila 0: Combinació secreta.
                                      {' ',' ',' ',' ',' '}};  //Fila 1: Jugada.
   int  col = 0;    //Columna que estem accedint de la matriu.

   int  state = 0;  //Estat del joc
                    //-1: S'ha premut ESC per sortir   
                    // 0: Estem entrant la combinació secreta.
                    // 1: Estem entrant la jugada.
                    // 2: La combinació secreta té espais o valors repetits.
                    // 3: S'ha guanyat, jugada = combinació secreta.
                    // 4: S'han esgotat els intents

   long  tries = 5; //Intents que queden
   short hitsO;     //Encerts fora de lloc
   short hitsX;     //Encerts a lloc.
   
   
   int op=' ';      
   
   while (op!=27) {
     op = printMenuP2_C();	  //Mostrar menú i retornar opció.
     switch(op){
       case 27:
         gotoxyP2_C(23,1); 
         break;
       case '1':	          //Posiciona Cursor al Tauler.
         state=1;
         tries=5;
         col = 0;		
         printBoardP2_C(tries);
         gotoxyP2_C(21,11);
         printf("   Press any key  ");   
         //=======================================================
         posCurScreenP2(state, tries, col);
         ///posCurScreenP2_C(state, tries, col);
         //=======================================================
         getchP2_C();
         break;
       case '2':	          //Actualitzar la columna del cursor.
         state=1;
         tries=5;
	     col = 3;
         printBoardP2_C(tries);
         gotoxyP2_C(21,11);
         printf(" Press 'j' or 'k' ");
         posCurScreenP2_C(state, tries, col);
         c = getchP2_C();
         if (c=='j' || c=='k') {
         //=======================================================
         col = updateColP2(c, col);
         ///col = updateColP2_C(c, col);	    
         //=======================================================
         }
         gotoxyP2_C(21,11);
         printf("   Press any key   ");
         posCurScreenP2_C(state, tries, col);
         getchP2_C();
         break;
       case '3': 	     //Actualitzar array i mostrar-ho per pantalla.
         state=0;
         tries=5;
         col=2;		  
         printBoardP2_C(tries);
         gotoxyP2_C(21,11);
         printf(" Press (0-9) value ");
         posCurScreenP2_C(state, tries, col);
         c = getchP2_C();
         if (c>='0' && c<='9'){       
         //=======================================================
         updateMatrixBoardP2(c, mSecretPlay, col, state);
         ///updateMatrixBoardP2_C(c, mSecretPlay, col, state);
         //=======================================================
         }
         gotoxyP2_C(20,11);
         printf("   To show Secret  "); 
         gotoxyP2_C(21,11);
         printf("   Press any key   ");
         getchP2_C();
         printSecretP2_C(mSecretPlay);
         gotoxyP2_C(20,11);
         printf("                   "); 
         gotoxyP2_C(21,11);
         printf("    Press any key  "); 
         getchP2_C();
         break;
       case '4': 	     //Llegir la combinació secreta o la jugada
         state=0;
         tries=5;
         col=0;
         for (i=0;i<DIMMATRIX;i++) {
		   mSecretPlay[state][i]=' ';
	     }
         printBoardP2_C(tries);
         printMessageP2_C(state);	
         //=======================================================
         state = getSecretPlayP2(mSecretPlay, state, tries);
         ///state = getSecretPlayP2_C(mSecretPlay, state, tries);
         //=======================================================
         printSecretP2_C(mSecretPlay);
         state = checkSecretP2_C(mSecretPlay);
         printMessageP2_C(state);	
         break;  
       case '5': 	     //Mostrar la combinació secreta.
         state=0;
         tries=5;
         col=0;	
         printBoardP2_C(tries);  
         //=======================================================
         printSecretP2(mSecretPlay);
         ///printSecretP2_C(mSecretPlay);		
         //=======================================================
         gotoxyP2_C(21,11);
         printf("   Press any key  ");
         getchP2_C();
         break;
       case '6': 	     //Verificar combinació secret
         state=0;
         tries=5;
         col=0;
         printBoardP2_C(tries);
         //=======================================================
         state = checkSecretP2(mSecretPlay);
         ///state = checkSecretP2_C(mSecretPlay);
         //=======================================================
         printSecretP2_C(mSecretPlay);		
         printMessageP2_C(state);
         break;
       case '7': 	     //Mostrar encerts.
         state=1;
         tries=5;
         col=0;
         printBoardP2_C(tries);
         hitsO = 2;
         hitsX = 2;
         //=======================================================
         printHitsP2(hitsX, hitsO, tries);
         ///printHitsP2_C(hitsX, hitsO, tries);
         //=======================================================
         gotoxyP2_C(21,11);
         printf("   Press any key  ");
         getchP2_C();
         break;
       case '8': 	     //Compta els encerts a lloc i fora de lloc.
         state=0;
         tries=5;
         col=0;
         printBoardP2_C(tries);
         char  msecretplay2[2][DIMMATRIX] = {{'1','2','3','4','5'}, //Combinació secreta
                                             {'1','4','3','1','4'}};//Jugada 
         printSecretP2_C(msecretplay2);
         state=1;
         int rowScreen = 9+(DIMMATRIX-tries)*2;;
         int colScreen = 8;
         for (i=0; i<DIMMATRIX; i++){
	       gotoxyP2_C(rowScreen, colScreen);
	       c = msecretplay2[1][i];
           printchP2_C(c);
           colScreen = colScreen + 2;     
         } 	
         //=======================================================
         state = checkPlayP2(msecretplay2, state, tries);
         //state = checkPlayP2_C(msecretplay2, state, tries);
         //=======================================================
         printMessageP2_C(state);
         break;
       case '9': 	//Joc complet en assemblador.
         i=0;
         for (i=0;i<DIMMATRIX;i++) {
           mSecretPlay[0][i]=' ';
           mSecretPlay[1][i]=' ';
         } 
         //=======================================================
         playP2(mSecretPlay);
         //=======================================================
         break;
       case '0': 	//Joc complet en C.
       i=0;
         for (i=0;i<DIMMATRIX;i++) {
           mSecretPlay[0][i]=' ';
           mSecretPlay[1][i]=' ';
         } 
         //=======================================================
         playP2_C(mSecretPlay);
         //=======================================================
         break;
     }
   }

}