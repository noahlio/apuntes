section .data               
;Canviar Nom i Cognom per les vostres dades.
developer db "Noah Bobis",0

;Constant que també està definida en C.
DIMMATRIX equ 5		

section .text            
;Variables definides en Assemblador.
global developer                        

;Subrutines d'assemblador que es criden des de C.
global posCurScreenP2, updateColP2, updateMatrixBoardP2, getSecretPlayP2
global printSecretP2, checkSecretP2, printHitsP2, checkPlayP2, printMessageP2, playP2

;Funcions de C que es criden des de assemblador.
extern clearScreen_C,  gotoxyP2_C, printchP2_C, getchP2_C
extern printBoardP2_C, printMessageP2_C


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ATENCIÓ: Recordeu que en assemblador les variables i els paràmetres 
;;   de tipus 'char' s'han d'assignar a registres de tipus  
;;   BYTE (1 byte): al, ah, bl, bh, cl, ch, dl, dh, sil, dil, ..., r15b
;;   les de tipus 'short' s'han d'assignar a registres de tipus 
;;   WORD (2 bytes): ax, bx, cx, dx, si, di, ...., r15w
;;   les de tipus 'int' s'han d'assignar a registres de tipus 
;;   DWORD (4 bytes): eax, ebx, ecx, edx, esi, edi, ...., r15d
;;   les de tipus 'long' s'han d'assignar a registres de tipus 
;;   QWORD (8 bytes): rax, rbx, rcx, rdx, rsi, rdi, ...., r15
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Les subrutines en assemblador que s'han de modificar per a
;; implementar el pas de paràmetres són:
;;   posCurScreenP2, updateColP2, updateMatrixBoardP2, getSecretPlayP2
;;   printSecretPlayP2, checkSecretP2, printHitsP2.
;; La subrutina que s'ha de modificar la funcionalitat:
;;   checkPlayP2.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Situar el cursor en una fila i una columna de la pantalla
; en funció de la fila (edi) i de la columna (esi) rebuts com 
; a paràmetre cridant a la funció gotoxyP2_C.
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (rowScreen): rdi(edi) : Fila de la pantalla on es situa el cursor.
; (colScreen): rsi(esi) : Columna de la pantalla on es situa el cursor.
;
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
gotoxyP2:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   ; Quan cridem la funció gotoxyP2_C(int row_num, int row_num) des d'assemblador 
   ; el primer paràmetre (row_num) s'ha de passar pel registre rdi(edi), i
   ; el segon  paràmetre (col_num) s'ha de passar pel registre rsi(esi).	
   call gotoxyP2_C
 
   ;restaurar l'estat dels registres que s'han guardat a la pila.
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Mostrar un caràcter (dil) a la pantalla, rebut com a paràmetre, 
; en la posició on està el cursor cridant la funció printchP2_C.
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (c) : rdi(dil) : Caràcter a mostrar.
; 
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
printchP2:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   ; Quan cridem la funció printchP2_C(char c) des d'assemblador, 
   ; el paràmetre (c) s'ha de passar pel registre rdi(dil).
   call printchP2_C
 
   ;restaurar l'estat dels registres que s'han guardat a la pila.
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax

   mov rsp, rbp
   pop rbp
   ret
   

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Llegir una tecla i retornar el caràcter associat (al) sense 
; mostrar-lo per pantalla, cridant la funció getchP2_C.
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; Cap
; 
; Paràmetres de sortida: 
; (c) : rax(al) : Caràcter llegit des del teclat.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
getchP2:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15
   push rbp
   
   mov rax, 0
   ; Quan cridem la funció getchP2_C des d'assemblador, 
   ; retorna sobre el registre rax(al) el caràcter llegit
   call getchP2_C
 
   ;restaurar l'estat dels registres que s'han guardat a la pila.
   pop rbp
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   
   mov rsp, rbp
   pop rbp
   ret 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Posiciona el cursor dins al tauler segons la posició del cursor (col),
; dels intents que queden (tries) i l'estat del joc (state). 
; Si estem entrant la combinació secreta (state==0) ens posarem a la
; fila 3 (rowScreen=3), si estem entrant un jugada (state==1) la 
; fila es calcula amb la fórmula: (rowScreen=9+(DIMMATRIX-tries)*2).
; La columna es calcula amb la fòrmula (colScreen= 8+(col*2)).
; Per a posicionar el cursor es crida la subrutina gotoxyP2.
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (state):rdi(edi):  Estat del joc.
; (tries):rsi(rsi):  Intents que queden.
; (col)  :rdx(edx):  Columna on està el cursor.
; 
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
posCurScreenP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   push rdi
   push rsi

   cmp rdi, 0
   jne posCurScreenP2_if_state_not_0
   mov rdi, 3
   jmp posCurScreenP2_end_if_state_0
   posCurScreenP2_if_state_not_0:
   mov rax, DIMMATRIX
   sub rax, rsi
   imul rax, 2
   add rax, 9
   mov rdi, rax
   posCurScreenP2_end_if_state_0:
   mov rax, rdx
   shl rax, 1
   add rax, 8
   mov rsi, rax
   call gotoxyP2

   pop rsi
   pop rdi
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Actualitzar la columna (col) on està el cursor.
; Si s'ha llegit (charac=='j') esquerra o (charac=='k') dreta 
; actualitzar la posició del cursor (col +/- 1)
; controlant que no surti del vector [0..DIMMATRIX-1]. 
; Retornar el valor actualitzat de (col).
;  
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (charac): rdi(dil): Caràcter llegit des del teclat.
; (col)   : rsi(esi): Columna on està el cursor.
; 
; Paràmetres de sortida: 
; (col)   : rax(eax):  Columna on està el cursor actualitzada.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
updateColP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.

   push rdi
   push rsi
   
   cmp dil, 'j'
   je updateColP2_if_charac_j
   cmp dil, 'k'
   je updateColP2_if_charac_k
   jmp updateColP2_end_if_charac
   updateColP2_if_charac_j:
   cmp rsi, 0
   jle updateColP2_end_if_charac
   sub rsi, 1
   jmp updateColP2_end_if_charac
   updateColP2_if_charac_k:
   mov eax, DIMMATRIX
   sub eax, 1
   cmp eax, esi
   jle updateColP2_end_if_charac
   add rsi, 1
   updateColP2_end_if_charac:
   mov rax, rsi

   pop rsi 
   pop rdi
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Guardar el caràcter llegit ['0'-'9'] (charac) a la matriu 
; (mSecretPlay) a la fila indicada per la variable (state) i
; la columna indicada per la variable (col).
; Si (state==0) canviarem el caràcter llegit per un '*' (charac='*') 
; perquè no es vegi la combinació secreta que escrivim.
; Finalment mostrarem el caràcter (charac) a la pantalla a la posició
; on està el cursor cridant la subrutina printchP2.
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (charac)    : rdi(dil): Caràcter a mostrar.
; (mSecrePlay): rsi(rsi): Adreça de la matriu on guardem la combinació secreta i la jugada.
; (col)       : rdx(edx): Columna on està el cursor.
; (state)     : rcx(ecx): Estat del joc.
; 
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
updateMatrixBoardP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   
   push rdi
   push rsi
   push rdx
   push rcx

   mov eax, ecx
   imul eax, DIMMATRIX
   add eax, edx
   lea rbx, [rsi]
   lea rbx, [rbx + rax]
   mov al, dil
   mov byte [rbx], al

   cmp ecx, 0
   jne updateMatrixBoardP2_end_if_state_0
   mov dil, '*'
   updateMatrixBoardP2_end_if_state_0:
   call printchP2

   pop rcx
   pop rdx
   pop rsi
   pop rdi
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Llegir els caràcters de la combinació secreta o la jugada.
; Mentre no es premi ENTER(10) o ESC(27) fer el següent:
; · Posicionar el cursor a la pantalla cridant la subrutina posCurScreenP2, 
;   segons el valor de les variables (col, tries i state).
; · Llegir un caràcter de teclat cridant la subrutina getchP2
;   que retorna a (charac) el codi ASCII del caràcter llegit.
;   - Si s'ha llegit una 'j'(esquerra) o una 'k' (dreta) moure el 
;     cursor per les 5 posicions de la combinació actualitzant 
;     el valor de la variable (col) cridant la subrutina updateColP2
;     en funció de les variables (col, tries i state).
;   - Si s'ha llegit un número ['0'-'9'] el guardem a la matriu
;     (mSecretPlay) i el mostrem cridant la subrutina updateMatrixBoardP2
;     en funció de les variables (charac, mSecretPlay, col i state).
; Si s'ha premut ESC(27) posar (state=-1) per a indicar que hem de sortir.
; Retornar l'estat actual del joc.
; Si es prem ENTER(10) s'acceptarà la combinació tal com estigui.
; NOTA: Cal tindre en compte que si es prem ENTER sense haver assignat
; valors a totes les posicions de la combinació, hi haurà posicions 
; que seran un espai (valor utilitzat per inicialitzar la matriu).
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
; (state)      : rsi(esi): Estat del joc.
; (tries)      : rdx(rdx): Nombre d'intents que queden.
; 
; Paràmetres de sortida: 
; (state)      : rax(eax): Estat del joc.
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
getSecretPlayP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   push rdi
   push rsi
   push rdx
   push r9
   push r10
   push r11


   mov r8, rdi
   mov r9, rdx
   mov r10, rsi
   mov r11, 0

   getSecretPlayP2_while_charac_exit:
   mov rsi, r9
   mov edi, r10d
   mov rdx, r11
   call posCurScreenP2
   call getchP2

   cmp al, 'j'
   je getSecretPlayP2_if_charac_j_k
   cmp al, 'k'
   je getSecretPlayP2_if_charac_j_k
   jmp getSecretPlayP2_end_if_charac_j_k
   getSecretPlayP2_if_charac_j_k:
   mov dil, al
   mov rsi, r11
   call updateColP2
   mov r11, rax
   getSecretPlayP2_end_if_charac_j_k:

   cmp al, '0'
   jl getSecretPlayP2_end_if_charac_09
   cmp al, '9'
   jg getSecretPlayP2_end_if_charac_09
   mov ecx, r10d
   mov rsi, r8
   mov dil, al
   mov rdx, r11
   call updateMatrixBoardP2
   getSecretPlayP2_end_if_charac_09:
   
   cmp al, 10
   je getSecretPlayP2_end_while_charac_exit
   cmp al, 27
   je getSecretPlayP2_end_while_charac_exit
   jmp getSecretPlayP2_while_charac_exit

   getSecretPlayP2_end_while_charac_exit:
   cmp al, 27
   jne getSecretPlayP2_else_charac_27
   mov eax, -1
   jmp getSecretPlayP2_end_if_charac_27
   getSecretPlayP2_else_charac_27:
   mov eax, r10d
   getSecretPlayP2_end_if_charac_27:
   
   pop r11
   pop r10
   pop r9
   pop rdx
   pop rsi
   pop rdi
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Verificar que la combinació secreta no tingui el valor inicial (' '),
; ni números repetits.
; Per cada element de la fila [0] de la matriu (mSecretPlay) mirar que 
; no hi hagi un espai (' ') i que no estigui repetit (de la posició 
; següent a l'actual fins al final). Per a indicar que la combinació
; secreta no és correcte posarem (secretError=1).
; Si la combinació secreta no és correcta, posar (state=2) per 
; indicar-ho.
; Sinó, la combinació secreta és correcta, posar (state=1) per anar
; a llegir jugades.
; Retornar l'estat actual del joc (state).
; 
; Variables globals utilitzades:
; Cap	
; 
; Paràmetres d'entrada : 
; (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
; 
; Paràmetres de sortida: 
; (state)      : rax(eax): Estat del joc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
checkSecretP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   push rdi
   push r8
   push r9

   mov r8, -1

   checkSecretP1_for_i_lower_dimmatrix:
   add r8, 1
   cmp r8, DIMMATRIX
   jge checkSecretP1_secretError_off
   mov al, byte[rdi + r8]
   cmp al, ' '
   je checkSecretP1_secretError_on
   mov r9, r8

   checkSecretP1_for_j_lower_dimmatrix:
   add r9, 1
   cmp r9, DIMMATRIX
   jge checkSecretP1_for_i_lower_dimmatrix
   movzx rcx, byte[rdi + r9]
   cmp al, cl
   jne checkSecretP1_for_j_lower_dimmatrix

   checkSecretP1_secretError_on:
   mov rax, 2
   jmp checkSecretP1_end_secretError
   checkSecretP1_secretError_off:
   mov rax, 1
   checkSecretP1_end_secretError:
   
   pop r9
   pop r8
   pop rdi
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mostrar la combinació secreta del joc.
; Mostra la combinació secreta (fila 0 de la matriu mSecretPlay) 
; a la part superior del tauler quan finalitza el joc.
; Per a mostrar els valors s'ha de cridar la subrutina gotoxyP2 per a 
; posicionar el curso, a la fila 3 (rowScreen=3) i a partir de la 
; columna 8 (colScreen=8) i printchP2_C per a mostrar cada caràcter.
; Incrementar la columna (colScreen) de 2 en 2. 
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
; 
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
printSecretP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.

   mov r8, 8
   mov r9, rdi
   mov rcx, -1

   printSecretP1_for_i_lower_dimmatrix:
   add rcx, 1
   mov rdi, 3
   mov rsi, r8
   call gotoxyP2
   mov al, byte[r9 + rcx]
   movzx rdi, al
   call printchP2
   add r8, 2
   cmp rcx, DIMMATRIX
   jl printSecretP1_for_i_lower_dimmatrix
   
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mostrar els encerts al lloc i fora de lloc.
; Situar el cursor en la fila (rowScreen = 9+(DIMMATRIX-tries)*2) i 
; la columna (colScreen = 22) (part dreta del tauler) per a mostrar 
; els encerts al tauler de joc.
; Primer es mostren els encerts al lloc (hX), tantes 'X' com 
; encerts al lloc hi hagin i després tantes 'O' com 
; encerts fora de lloc (hO) hi hagin.
; Per a mostrar els encerts s'ha de cridar la subrutina gotoxyP2 per a 
; posicionar el cursor i printchP2 per a mostrar els caràcters. 
; Cada cop que es mostra un encert s'ha d'incrimentar 
; la columna (colScreen) de 2 en 2.
; NOTA: (hX + hO ha de ser sempre més petit o igual que DIMMATRIX).
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (hX)   : rdi(di) : Encerts al lloc.
; (hO)   : rsi(si) : Encerts fora de lloc.
; (tries): rdx(rdx): Intents que queden.
; 
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
printHitsP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   push rdi
   push rsi
   push r8
   push r9
   push r10
   push r11

   mov rax, DIMMATRIX
   sub rax, rdx
   imul rax, 2
   add rax, 9
   mov r8, rax
   mov r9, rdi
   mov r10, 22
   mov r11, rsi

   printHitsP2_for_i_upper_0:
      cmp r9, 0
      jle printHitsP2_end_for_i_upper_0
      mov rdi, r8
      mov rsi, r10
      call gotoxyP2
      mov rdi, 'X'
      call printchP2
      mov EBX, r10d
      add EBX, 2
      mov r10d, EBX
      sub r9, 1
      jmp printHitsP2_for_i_upper_0
   printHitsP2_end_for_i_upper_0:


   printHitsP2_for_j_upper_0:
      cmp r11, 0
      jle printHitsP2_end_for_j_upper_0
      mov rdi, r8
      mov rsi, r10
      call gotoxyP2
      mov rdi, 'O'
      call printchP2
      mov EBX, r10d
      add EBX, 2
      mov r10d, EBX
      sub r11, 1
      jmp printHitsP2_for_j_upper_0
   printHitsP2_end_for_j_upper_0:

   pop r11
   pop r10
   pop r9
   pop r8
   pop rsi
   pop rdi
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Comptar els encerts al lloc i fora de lloc de la jugada
; respecte de la combinació secreta.
; Per a fer la comparació cal fer el següent:
; Per cada element de la combinació secreta (fila 0 de la matriu mSecretPlay)
; (s'utilitza de referència perquè no té valors repetits). 
; Comparar-lo amb tots els elements de la jugada (fila 1 de la matriu mSecretPlay).
; Si un element de la combinació secreta (mSecretPlay[0][i]) és igual a un 
; l'element de la mateixa posició de la jugada (mSecretPlay[1][i]) serà un 
; encert al lloc 'X' i s'han d'incrementar els encerts al lloc (hX++),
; sinó, serà un encert fora de lloc 'O' si un element de la combinació
; secreta (mSecretPlay[0][i]) és igual a un element de la jugada 
; (mSecretPlay[1][j]), però ocupen posicions diferents (i!=j), 
; s'han d'incrementar els encer fora de lloc (hO++) i deixar de 
; recórrer la jugada per comptar cada encert un sol cop.
; Si totes les posicions de la combinació secreta i de la jugada
; són iguals (hX=DIMMATRIX) hem guanyat i s'ha de modificar l'estat
; del joc per a indicar-ho (state=3),
; sinó, mirar si s'han exhaurit els intents (tries==1), si s'han
; exhaurit modificar l'estat del joc per a indicar-ho (state=4).
; Mostrar els encerts al lloc i fora de lloc al tauler de joc 
; cridant la subrutina printHitsP2.
; Retornar l'estat actual del joc (state).
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
; (state)      : rsi(esi): Estat del joc.
; (tries)      : rdx(rdx): Intents que queden.
; 
; Paràmetres de sortida: 
; (state)      : rax(eax): Estat del joc.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
checkPlayP2:
   push rbp
   mov  rbp, rsp
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.
   push rdi
   push rsi
   push r8
   push r9
   push r11
   push r12
   push r13
   push r10
   
   mov r8, -1 ; i
   mov r9, 0 ; hx
   mov r10, 0 ; h0
   mov r11, rsi ; state
   mov r12, rdx ; tries

   checkPlayP2_for_i_lower_dimmatrix:
      add r8, 1
      cmp r8, DIMMATRIX
      jge checkPlayP2_end_for_i_lower_dimmatrix
      mov al, byte[rdi + r8]
      cmp al, byte[rdi + r8 + DIMMATRIX]
      jne checkPlayP2_start_for_j_lower_dimmatrix
      add r9, 1
      jmp checkPlayP2_for_i_lower_dimmatrix

      checkPlayP2_start_for_j_lower_dimmatrix:
      mov r13, -1

      checkPlayP2_for_j_lower_dimmatrix:
         add r13, 1
         cmp r13, DIMMATRIX
         jge checkPlayP2_for_i_lower_dimmatrix
         mov al, byte[rdi + r13]
         cmp al, byte[rdi + r13 + DIMMATRIX]
         jne checkPlayP2_for_j_lower_dimmatrix
         add r10, 1
         jmp checkPlayP2_for_i_lower_dimmatrix
      checkPlayP2_end_for_j_lower_dimmatrix:
   checkPlayP2_end_for_i_lower_dimmatrix:


   cmp r9, DIMMATRIX
   je checkPlayP2_if_hx_equals_dimmatrix
   cmp r12, 1
   jne checkPlayP2_if_end
   mov r11, 4
   jmp checkPlayP2_if_end
   checkPlayP2_if_hx_equals_dimmatrix:
   mov r11, 3
   checkPlayP2_if_end:

   mov rdi, r9
   mov rsi, r10
   mov rdx, r12
   call printHitsP2
   mov rax, r11

   pop r10
   pop r13
   pop r12
   pop r11
   pop r9
   pop r8
   pop rsi
   pop rdi
   ;restaurar l'estat dels registres que s'han guardat a la pila.

   mov rsp, rbp
   pop rbp
   ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mostra un missatge a la part inferior dreta del tauler segons el 
; valor de la variable (state).
; (state) -1: S'ha premut ESC per sortir
;          0: Estem entrant la combinació secreta.
;          1: Estem entrant la jugada.
;          2: La combinació secreta té espais o valors repetits.
;          3: S'ha guanyat, jugada = combinació secreta.
;          4: S'han esgotat els intents.
;          
; S'espera que es premi una tecla per continuar.
; Mostrar un missatge a sota al tauler per indicar-ho 
; i al prémer una tecla l'esborra.
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (state): rdi(edi): Estat del joc.
; 
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
printMessageP2:
   push rbp
   mov  rbp, rsp
   ;guardem l'estat dels registres del processador perquè
   ;les funcions de C no mantenen l'estat dels registres.
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   ; Quan cridem la funció printMessageP2_C(int state) des d'assemblador, 
   ; el paràmetre (state) s'ha de passar pel registre rdi(edi).
   call printMessageP2_C
 
   ;restaurar l'estat dels registres que s'han guardat a la pila.
   pop r15
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax

   mov rsp, rbp
   pop rbp
   ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Aquesta subrutina es dóna feta. NO LA PODEU MODIFICAR.
; Subrutina principal del joc
; Llegeix la combinació secreta i verifica que sigui correcte.
; A continuació es llegeix una jugada, compara la jugada amb la
; combinació secreta per a determinar els encerts.
; Repetir el procés mentre no s'encerta la combinació secreta i 
; mentre queden intents. Si es prem la tecla 'ESC' durant la lectura 
; de la combinació secreta o d'una jugada sortir.
; 
; Pseudo codi:
; El jugador disposa de 5 intents (tries=5) per encertar
; la combinació secreta, l'estat inicial del joc és 0 (state=0) i
; el cursor es posa a la columna 0 (col=0).
; Mostrar el tauler de joc cridant la funció printBoardP2_C.
; 
; Mentre (state==0) llegir la combinació secreta o (state==1) llegir
; la jugada:
;   - Mostrar els intents que queden (tries) per a encertar la combinació 
;     secreta, situar el cursor a la fila 21, columna 5 cridant la subrutina
;     gotoxyP2 i mostra el caràcter associat al valor de la variable.
;     (tries) sumant '0' i cridant la subrutina printchP2.
;   - Mostrar un missatge segons estat del joc (state) cridant 
;     la subrutina printMessageP2.
;   - Posicionar el cursor al tauler cridant la subrutina posCurBoardP2.
;   - Llegir els caràcters de la combinació secreta o de la jugada 
;     i actualitzar l'estat del joc cridant la subrutina getSecretPlayP2.
;   - Si estem introduïnt la combinació secreta (state==0) verificar
;     que es correcte cridant la subrutina chackSecretP2.
;     Sino, si estem introduïnt la jugada (state==1) verificar 
;     els encerts de la jugada cridant la subrutin checkPlayP2,
;     decrementar els intents (tries). Inicialitzar la jugada que
;     tenim guardada a la fila 1 de la matriu mSecretPlay amb 
;     espais (' ') per poder introduir una nova jugada.
; 
; Per a acabar, mostrar els intents que queden (tries) per a 
; encertar la combinació secreta, situar el cursor a la fila 21, 
; columna 5 cridant la subrutina gotoxyP2 i mostra el caràcter associat 
; al valor de la variable (tries) sumant '0' i cridant a la subrutina 
; printchP2, mostrar la combinació secreta cridant la subrutina
; printSecretP2 i finalment mostrar un missatge segons l'estat del 
; joc (state) cridant la subrutina printMessageP2.
; S'acaba el joc.
; 
; Variables globals utilitzades:	
; Cap
; 
; Paràmetres d'entrada : 
; (mSecretPlay): rdi(rdi): Adreça de la matriu on guardem la combinació secreta i la jugada.
; 
; Paràmetres de sortida: 
; Cap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
playP2:
   push rbp
   mov  rbp, rsp 
   ;guardar l'estat dels registres que es modifiquen en aquesta 
   ;subrutina i que no s'utilitzen per retornar valors.	
   push rax
   push rbx
   push rcx
   push rdx
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   
   push rdi
   mov  edi, 5
   call printBoardP2_C        ;printBoardP2_C(tries);
   pop  rdi

   mov  rbx, rdi              ;mSecretPlay
   mov  r10d, 0               ;col = 0;    //Posició on està el cursor.
   mov  r11d, 0               ;state=0;    //Estat del joc.
   mov  r12 , 5               ;tries=5;    //Intents que queden.
   
   p2_while:
   cmp r11d, 0                ;while (state == 0 
   je  p2_whileOk
   cmp r11d, 1                ;|| state==1) {
   jne p2_endwhile
     p2_whileOk:
     
     mov edi, 21
     mov esi, 5
     call gotoxyP2            ;gotoxyP2_C(21,5);
     mov rdi, r12
     add dil, '0' 
     call printchP2           ;printchP2_C(tries + '0');
     mov edi, r11d
     call printMessageP2      ;printMessageP2_C(state);
     mov rsi, r12
     mov edx, r10d
     
     mov  edi, r11d
     mov  rsi, r12
     mov  edx , r10d
     call posCurScreenP2       ;posCurScreenP2_C(state, tries, col);
     
     mov rdi, rbx
     mov esi, r11d
     mov rdx, r12    
     call getSecretPlayP2     ;state = getSecretPlayP2_C(mSecretPlay, state, tries);
	 mov r11d, eax
	 
	 p2_if1:
	 cmp r11d, 0              ;if (state==0) {
	 jne p2_else1
	   mov  rdi, rbx
       call checkSecretP2     ;state = checkSecretP2_C(mSecretPlay);
       mov r11d, eax
       jmp p2_endif1          ;}
     p2_else1:                ;} else {
       p2_if2:
       cmp r11d, 1            ;if (state==1) {
	   jne p2_endif2
	     mov rdi, rbx
	     mov esi, r11d
	     mov rdx, r12
         call checkPlayP2     ;state = checkPlayP2_C(mSecretPlay, state, tries);
         mov r11d, eax
         dec r12              ;tries --;
       p2_endif2:             ;}
       mov rcx, 0             ;i=0;
       p2_for:                ;for (
       cmp rcx, DIMMATRIX
       jge p2_endfor          ;i<DIMMATRIX;i++) {
		 mov BYTE[rbx+DIMMATRIX+rcx], ' '  ;mSecretPlay[1][i]=' ';
		 inc rcx              ;i++;
		 jmp p2_for
	   p2_endfor:             ;}
     p2_endif1:               ;}
     jmp p2_while
   p2_endwhile:               ;}
     
   mov  edi, 21
   mov  esi, 5
   call gotoxyP2              ;gotoxyP2_C(21,5);
   mov  rdi, r12
   add  dil, '0'
   call printchP2             ;printchP2_C(tries + '0');
   mov  rdi, rbx
   call printSecretP2         ;printSecretP2_C(mSecretPlay);
   mov  edi, r11d
   call printMessageP2        ;printMessage_C(state);
   
   p2_end:
   ;restaurar l'estat dels registres que s'han guardat a la pila.
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rdx
   pop rcx
   pop rbx
   pop rax

   mov rsp, rbp
   pop rbp
   ret
