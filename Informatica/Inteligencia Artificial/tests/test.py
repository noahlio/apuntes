import matplotlib.pyplot as plt

# Datos
terminos = ['VL', 'L', 'M', 'H', 'VH']
niveles_activacion = [0.875, 0.982, 1, 0.333, 0.333]

# Crear gráfico de barras
plt.figure(figsize=(8, 6))
plt.bar(terminos, niveles_activacion, color=['blue', 'green', 'red', 'orange', 'purple'])

# Títulos y etiquetas
plt.title('Niveles de Activación de los Términos', fontsize=14)
plt.xlabel('Términos', fontsize=12)
plt.ylabel('Nivel de Activación', fontsize=12)
plt.ylim(0, 1.1)  # Limitar el eje y para que se vea mejor

# Mostrar el gráfico
plt.show()
