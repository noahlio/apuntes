import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

x = np.linspace(0, 15, 100)

VL = fuzz.trapmf(x, [0, 0, 2, 3])
L = fuzz.trapmf(x, [0, 3, 4, 7])
M = fuzz.trapmf(x, [5, 7, 9, 10])
H = fuzz.trapmf(x, [9, 10, 12, 15])
VH = fuzz.trapmf(x, [12, 13, 15, 15])

plt.figure(figsize=(12, 6))
plt.plot(x, VL, label="VL", color="blue")
plt.plot(x, L, label="L", color="orange")
plt.plot(x, M, label="M", color="green")
plt.plot(x, H, label="H", color="red")
plt.plot(x, VH, label="VH", color="purple")

plt.text(2.6, 0.5, "3-x", color="blue")

plt.text(1, 0.5, "x/3", color="orange")
plt.text(4.25, 0.5, "(7-x)/3", color="orange")

plt.text(6.1, 0.5, "(x-5)/2", color="green")
plt.text(9.75, 0.5, "10-x", color="green")

plt.text(9, 0.5, "x-9", color="red")
plt.text(13.75, 0.5, "(15-x)/3", color="red")

plt.text(11.5, 0.5, "x-12", color="purple")

plt.title("Funcions de Pertinença VarA")
plt.xlabel("Valors")
plt.ylabel("Graus de pertinença")
plt.legend()
plt.grid()
plt.show()

