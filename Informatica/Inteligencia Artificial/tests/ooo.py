import numpy as np
import matplotlib.pyplot as plt

# Definir el intervalo de x
x = np.linspace(0, 1, 500)

# Función de pertenencia OOO(x)
def OOO(x):
    if x <= 0.25:
        return 10 * x
    elif x <= 0.5:
        return 0.25
    elif x <= 0.75:
        return 10 * x - 5
    else:
        return 0.5

y = np.array([OOO(xi) for xi in x])

plt.figure(figsize=(8, 6))
plt.plot(x, y, label=r"$OOO(x)$", color='b', linewidth=2)
plt.title("Función de Pertenencia $OOO(x)$")
plt.xlabel("x")
plt.ylabel("$OOO(x)$")
plt.grid(True)
plt.axhline(0, color='black',linewidth=0.5)
plt.axvline(0, color='black',linewidth=0.5)
plt.legend()
plt.show()
