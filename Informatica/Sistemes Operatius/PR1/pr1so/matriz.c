#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Función que genera una palabra de 3 letras aleatorias de la 'a' a la 'z'
// Se le añade el caracter nulo '\0' al final
char* generarPalabraAleatoria() {
    int i;
    char* palabra = (char*)malloc(4 * sizeof(char));

    for (i = 0; i < 3; i++) {
        palabra[i] = 'a' + rand() % 26;
    }

    palabra[3] = '\0';
    return palabra;
}

int main(int argc, char *argv[]) {
    // Inicializar para que en cada ejecución se generen valores aleatorios
    srand(time(NULL)); 

    char ***matriz;
    
    int filas = atoi(argv[1]);
    int columnas = atoi(argv[2]);
    int i, j;

    // Asignación dinámica de memoria para la matriz
    matriz = (char***)malloc(filas * sizeof(char**));
    for (i = 0; i < filas; i++) {
        matriz[i] = (char**)malloc(columnas * sizeof(char*));
    }
    
    for (i = 0; i < filas; i++) {
        for (j = 0; j < columnas; j++) {
            matriz[i][j] = generarPalabraAleatoria();
        }
    }
    
    // Dibujar la matriz por pantalla
    for (i = 0; i < filas; i++) {
        for (j = 0; j < columnas; j++) {
            printf("%s ", matriz[i][j]);
        }
        printf("\n");
    }


    for (i = 0; i < filas; i++) {
        for (j = 0; j < columnas; j++) {
            free(matriz[i][j]);
        }
        free(matriz[i]);
    }
    free(matriz);

    return 0;
}
