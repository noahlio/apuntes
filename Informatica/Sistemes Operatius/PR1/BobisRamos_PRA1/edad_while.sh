#!/bin/bash
original="edad_2023.txt"
nou="edad_2024.txt"

while IFS=: read -r nom cognom edat pelicula || [[ -n $nom ]]; do
    ((edat++))
    echo "$nom:$cognom:$edat:$pelicula"
done < "$original" > "$nou"
