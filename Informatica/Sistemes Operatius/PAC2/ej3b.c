#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

sem_t semA;
sem_t semB;

void escriureA(void *p) {
    int i;
    for (i = 0; i < 5; i++) {
        sem_wait(&semA); 
        write(1, "A", 1);
        sleep(random() % 2);
        sem_post(&semB); 
    }
}

void escriureB(void *p) {
    int i;
    for (i = 0; i < 5; i++) {
        sem_wait(&semB);
        write(1, "B", 1);
        sleep(random() % 2);
        sem_post(&semA); 
    }
}

int main() {
    sem_init(&semA, 0, 0);
    sem_init(&semB, 0, 1);

    pthread_t thread1, thread2;

    pthread_create(&thread1, NULL, (void *)escriureA, NULL);
    pthread_create(&thread2, NULL, (void *)escriureB, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    sem_destroy(&semA);
    sem_destroy(&semB);

    return 0;
}

