#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h> 

int pipe_ping[2];
int pipe_pong[2];
char msg[200];

void handler_sigquit(int signum)
{
    sprintf(msg, "[%d] SIGQUIT signal received from 0.\n", getpid());
    write(STDOUT_FILENO, msg, strlen(msg));
    sprintf(msg, "[%d] End Ping process. Bye bye.\n", getpid());
    write(STDOUT_FILENO, msg, strlen(msg));
    close(pipe_ping[1]);
    close(pipe_pong[0]);
    wait(NULL);
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        char error_message[] = "Ús: <delay>\n";
        write(STDERR_FILENO, error_message, strlen(error_message));
        return 1;
    }
    if (pipe(pipe_ping) == -1 || pipe(pipe_pong) == -1)
    {
        perror("Error en la creación de pipes");
        exit(EXIT_FAILURE);
    }

    int delay = atoi(argv[1]);
    char buffer[100];

    struct sigaction sa_quit;
    sa_quit.sa_handler = handler_sigquit;
    sigemptyset(&sa_quit.sa_mask);
    sa_quit.sa_flags = 0;
    sigaction(SIGQUIT, &sa_quit, NULL);

    pid_t pid = fork();
    if (pid == -1) {
        perror("Error en fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0)
    {
        close(pipe_ping[1]);
        close(pipe_pong[0]);
        dup2(pipe_ping[0], 3);
        dup2(pipe_pong[1], 4);
        execl("./Pong_2-3", "Pong_2-3", argv[1], NULL);
        perror("execl");
        exit(EXIT_FAILURE);
    } else {
        close(pipe_ping[0]);
        close(pipe_pong[1]);

        int counter;

        while (1) {
            counter++;
            sleep(delay);

            sprintf(msg, "%d", counter);
            write(pipe_ping[1], msg, strlen(msg) + 1);
            sprintf(buffer, "[%d] Sending ping message %d.\n", getpid(), counter);
            write(1, buffer, strlen(buffer));

            ssize_t bytes_read = read(pipe_pong[0], buffer, sizeof(buffer) - 1);
            if (bytes_read > 0) {
                buffer[bytes_read] = '\0';
                sprintf(msg, "[%d] Pong message %s recived.\n", getpid(), buffer);
                write(1, msg, strlen(msg));
            }
        }

        close(pipe_ping[1]);
        close(pipe_pong[0]);
        exit(EXIT_SUCCESS);
    }

    return 0;
}
