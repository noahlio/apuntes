
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>

pid_t pid_p;
int delay;
char msg[100];
int pong_count = 0;

void handle_sigusr1(int sig) {
    pong_count++;
    int len = snprintf(msg, sizeof(msg), "[%d] Ping %d received (signal SIGUSR1 from %d).\n", getpid(), pong_count, pid_p);
    write(STDOUT_FILENO, msg, len);
    alarm(delay);
}

void handle_sigalarm(int sig) {
    int len = snprintf(msg, sizeof(msg), "[%d] Sending pong %d (signal SIGUSR2 to %d).\n", getpid(), pong_count, pid_p);
    write(STDOUT_FILENO, msg, len);
    kill(pid_p, SIGUSR2);
}

void handle_sigquit(int sig) {
    int len = snprintf(msg, sizeof(msg), "[%d] Quitting.\n", getpid());
    write(STDOUT_FILENO, msg, len);
    exit(0);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        char error_message[] = "Ús: <delay>\n";
        write(STDERR_FILENO, error_message, strlen(error_message));
        return 1;
    }

    delay = atoi(argv[1]);
    pid_p = getppid();

    struct sigaction sa_usr1, sa_alrm, sa_quit;

    sa_usr1.sa_handler = handle_sigusr1;
    sigemptyset(&sa_usr1.sa_mask);
    sa_usr1.sa_flags = 0;

    sa_alrm.sa_handler = handle_sigalarm;
    sigemptyset(&sa_alrm.sa_mask);
    sa_alrm.sa_flags = 0;

    sa_quit.sa_handler = handle_sigquit;
    sigemptyset(&sa_quit.sa_mask);
    sa_quit.sa_flags = 0;

    sigaction(SIGUSR1, &sa_usr1, NULL);
    sigaction(SIGALRM, &sa_alrm, NULL);
    sigaction(SIGQUIT, &sa_quit, NULL);

    while (1) {
        pause();
    }

    return 0;
}

