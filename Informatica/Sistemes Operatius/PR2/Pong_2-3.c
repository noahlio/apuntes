#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        char error_message[] = "Ús: <delay>\n";
        write(STDERR_FILENO, error_message, strlen(error_message));
        return 1;
    }

    int delay = atoi(argv[1]);

    char buffer[100];
    char msg[200];
    int counter;
    while (1) {
        counter++;
        ssize_t bytes_read = read(3, buffer, sizeof(buffer) - 1);
        if (bytes_read > 0) {
            buffer[bytes_read] = '\0';
            sprintf(msg, "[%d] Ping message %s recived.\n", getpid(), buffer);
            write(1, msg, strlen(msg));
        } 
        else if (bytes_read == 0){
            exit(EXIT_SUCCESS);
        }
        
        sleep(delay);
        
        sprintf(buffer, "[%d] Sending pong message %d.\n", getpid(), counter);
        write(1, buffer, strlen(buffer));
        sprintf(msg, "%d", counter);
        write(4, msg, strlen(msg) + 1);

    }

    return 0;
}
