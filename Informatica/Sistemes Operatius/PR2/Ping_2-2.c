
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>

pid_t pid_f;
int delay;
char msg[100];
int ping_count = 0;

void handle_sigusr2(int sig) {
    ping_count++;
    int len = snprintf(msg, sizeof(msg), "[%d] Pong %d received (signal SIGUSR2 from %d).\n", getpid(), ping_count, pid_f);
    write(STDOUT_FILENO, msg, len);
    alarm(delay);
}

void handle_sigalrm(int sig) {
    int len = snprintf(msg, sizeof(msg), "[%d] Sending ping %d (signal SIGUSR1 to %d).\n", getpid(), ping_count, pid_f);
    write(STDOUT_FILENO, msg, len);
    kill(pid_f, SIGUSR1);
}

void handle_sigquit(int sig) {
    int len = snprintf(msg, sizeof(msg), "[%d] Sending quit (signal SIGQUIT to %d).\n", getpid(), pid_f);
    write(STDOUT_FILENO, msg, len);
    kill(pid_f, SIGQUIT);
    exit(0);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        char error_message[] = "Ús: <delay>\n";
        write(STDERR_FILENO, error_message, strlen(error_message));
        return 1;
    }

    delay = atoi(argv[1]);

    struct sigaction sa_usr2, sa_alrm, sa_quit;

    sa_usr2.sa_handler = handle_sigusr2;
    sigemptyset(&sa_usr2.sa_mask);
    sa_usr2.sa_flags = 0;

    sa_alrm.sa_handler = handle_sigalrm;
    sigemptyset(&sa_alrm.sa_mask);
    sa_alrm.sa_flags = 0;

    sa_quit.sa_handler = handle_sigquit;
    sigemptyset(&sa_quit.sa_mask);
    sa_quit.sa_flags = 0;

    sigaction(SIGUSR2, &sa_usr2, NULL);
    sigaction(SIGALRM, &sa_alrm, NULL);
    sigaction(SIGQUIT, &sa_quit, NULL);

    pid_f = fork();

    if (pid_f == 0) { 
        execl("./Pong_2-2", "Pong_2-2", argv[1], NULL);
        char error_message[] = "Error en exec\n";
        write(STDERR_FILENO, error_message, strlen(error_message));
        return 1;
    } else if (pid_f > 0) { 
      alarm(delay);
        while (1) {
            pause();
        }
    } else {
        char error_message[] = "Error en fork\n";
        write(STDERR_FILENO, error_message, strlen(error_message));
        return 1;
    }

    return 0;
}

