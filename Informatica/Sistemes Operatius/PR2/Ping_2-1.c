#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        char error_message[] = "Ús: <missatge>\n";
        write(1, error_message, strlen(error_message));
        return 1;
    }

    while (1) {
        char message[100];
        int length = snprintf(message, sizeof(message), "PPid: %d - Pid: %d --> %s\n", getppid(), getpid(), argv[1]);
        write(1, message, length);
        sleep(2);
        pid_t pid = fork();
        if (pid == -1) {
            char error_message[] = "Error en fork\n";
            write(1, error_message, strlen(error_message));
            return 1;
        } else if (pid == 0) {
            execl("./Pong_2-1", "Pong_2-1", "Pong", NULL);
            char error_message[] = "Error en exec\n";
            write(1, error_message, strlen(error_message));
            return 1;
        }
        wait(NULL);
    }

    return 0;
}
