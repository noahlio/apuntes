#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h>
#include <sys/wait.h> 
#include <sys/types.h> 
#include <sys/stat.h> 
#include <fcntl.h>

int main()
{
  int x;
  char buff[100];
  x= read (0, buff, 100); 
  fork();
  close (1);
  open("./file.txt", O_WRONLY |O_CREAT, 0600);
  write (1, buff, x);
  wait (NULL);

  exit(0);
}
