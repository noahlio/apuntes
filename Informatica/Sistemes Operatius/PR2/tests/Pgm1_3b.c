#include <stdlib.h> 
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main()
{
  int x;
  char buff[100];
  x = read (0, buff, 100); 
  fork();
  write (1, buff, x); 
  wait (NULL);

  exit(0);
}
