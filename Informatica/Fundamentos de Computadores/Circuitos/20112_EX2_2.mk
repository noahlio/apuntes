<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<project source="3.3.5" version="2.2">
  This file is intended to be loaded by KeMap  (https://eimtveril.uoc.edu/pubver).

  <config>
    <a name="contents">MC4wX3hYeFh4XzEzMjAyX3hYeFh4X3NpYm9iaGFvbl9jQ2NfMQ==</a>
  </config>
  <original>
    <a name="contents">C2VvZi8LMiExMTIxCzIhMTIxMQsyITExMTELNzIhcS8LVCFjcC8LZSFkIWMhYiFjbWovCzIhcC8LNSFqLwtlZyFmcXp1Lw==-</a>
  </original>
  <truetable>
    <a name="contents">DGZwZzAMMyIyMjMyDDMiMjMyMgwzIjIyMjIMODMicjAMVSJkcTAMZiJlImQiYyJkbmswDDMicTAMNiJrMAxmaCJncnt2MA==-</a>
  </truetable>
  <editkm>
    <a name="contents">DWdxaDENNCMzMzQzDTQjMzQzMw00IzMzMzMNOTQjczENViNlcjENZyNmI2UjZCNlb2wxDTQjcjENNyNsMQ1naSNoc3x3MQ==-</a>
  </editkm>
  <selkm>
    <a name="contents">DmhyaTIONSQ0MTQ0DjUkNDQxNA46NSR0Mg5XJGZzMg5oJGckZiRlJGZwbTIONSRzMg44JG0yDmhqJGl0fXgy-</a>
  </selkm>
  <expression>
    <a name="contents">D2lzajMPNiU1MjU1DzYlNTUyNQ87NiV1Mw9YJWd0Mw9pJWglZyVmJWdxbjMPNiV0Mw85JW4zD2lrJWp1fnkz-</a>
  </expression>
</project>
