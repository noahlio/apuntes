package xai.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import edu.uoc.dpcs.lsim.logger.LoggerManager.Level;
import lsim.library.api.LSimLogger;
//import edu.uoc.dpcs.lsim.logger.LoggerManager.Level;
//import lsim.library.api.LSimLogger;
import xai.rest.jettyserver.api.Trimmed;

/**
 * @author Noah Bobis
 *
 */

public class RESTclient {

	public char charAt(String address, int port, String string, int posicio) {
		LSimLogger.log(Level.INFO, "getAt");
		LSimLogger.log(Level.INFO, "string: "+string);
		LSimLogger.log(Level.INFO, "position: "+posicio);

	    Client client = ClientBuilder.newClient();
	    String targetUrl = "http://" + address + ":" + port + "/strings/charAt/" + string + "/" + posicio;

	    Response response = client.target(targetUrl).request("text/plain").get();

	    String resultat = response.readEntity(String.class);
	    LSimLogger.log(Level.INFO, "Response: " + resultat);

	    response.close();
	    client.close();

	    if (resultat.startsWith("Error")) {
	        throw new RuntimeException(resultat);
	    }

	    return resultat.charAt(0);
	}
	
	public byte[] getBytes(String address, int port, String string) {
		LSimLogger.log(Level.INFO, "getBytes");
		LSimLogger.log(Level.INFO, "string: "+string);

	    Client client = ClientBuilder.newClient();
	    String targetUrl = "http://" + address + ":" + port + "/strings/getBytes/" + string;

	    Response response = client.target(targetUrl).request("application/json").get();

	    String jsonResult = response.readEntity(String.class);
	    LSimLogger.log(Level.INFO, "Response: " + jsonResult);

	    response.close();
	    client.close();

	    Gson gson = new Gson();
	    return gson.fromJson(jsonResult, byte[].class);
	}

	public Trimmed trim(String address, int port, String string) {
		LSimLogger.log(Level.INFO, "trim");
		LSimLogger.log(Level.INFO, "string: "+string);

	    Client client = ClientBuilder.newClient();
	    String target = "http://" + address + ":" + port + "/strings/trim/" + string;
	    Response response = client.target(target).request().get();

	    String jsonResponse = response.readEntity(String.class);
	    response.close();

	    Gson gson = new Gson();
	    Trimmed resultat = gson.fromJson(jsonResponse, Trimmed.class);

	    return resultat;
	}
}
