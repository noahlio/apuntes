/*

* Copyright (c) Joan-Manuel Marques 2013. All rights reserved.
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This file is part of the practical assignment of Distributed Systems course.
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This code is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

package tcp.client;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import edu.uoc.dpcs.lsim.logger.LoggerManager.Level;
import lsim.library.api.LSimLogger;


/**
 * @author Noah Bobis
 *
 */

public class HTTPclient {

	public HTTPclient() {
	}
			
	public HTTPrequestResponse get(String http_server_address, int http_server_port){
		LSimLogger.log(Level.INFO, "inici HTTPclient.get ");
		LSimLogger.log(Level.INFO, "HTTP server_address: " + http_server_address);
		LSimLogger.log(Level.INFO, "HTTP server_port: " + http_server_port);
		
		String peticio = "";
		String resposta = "";

		try (Socket socket = new Socket(http_server_address, http_server_port);
	             BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

	            peticio = "OPTIONS / HTTP/1.0\r\n" +
	                      "Host: " + http_server_address + "\r\n" +
	                      "Accept: */*\r\n" +
	                      "\r\n";

	            LSimLogger.log(Level.INFO, "Request: " + peticio);

	            out.write(peticio);
	            out.flush();

	            StringBuilder respostaBuilder = new StringBuilder();
	            String linia;
	            while ((linia = in.readLine()) != null) {
	                respostaBuilder.append(linia).append("\r\n");
	            }
	            resposta = respostaBuilder.toString();

	            LSimLogger.log(Level.INFO, "Response: " + resposta);

	        } catch (Exception e) {
	            LSimLogger.log(Level.ERROR, "Error en la comunicació: " + e.getMessage());
	            resposta = "Error: " + e.getMessage();
	        }

	   	LSimLogger.log(Level.INFO, "Request: " + peticio);
		LSimLogger.log(Level.INFO, "Response: " + resposta);
		
		return new HTTPrequestResponse(peticio, resposta);
	}
}
