package xai.rest.utilitats;

/**
 * @author Joan-Manuel Marques
 *
 */
public class Utilitats {
	public static boolean iguals(byte[] bytes_1, byte[] bytes_2) {
		if (bytes_1 == null) return false;
		if (bytes_2 == null) return false;
		if (bytes_1.length != bytes_2.length) return false;
		
		for (int i = 0; i < bytes_1.length ; i++)
			if (bytes_1[i] != bytes_2[i]) return false;
		return true;
	}
}
