package xai.rest.client.main;

import java.io.File;
import java.util.Arrays;

import lsim.library.api.LSimLogger;
import xai.rest.client.RESTclient;

/**
 * @author Joan-Manuel Marques
 *
 */

public class RESTclientMain {
	private static final String IP_SERVIDOR = "localhost";
	private static final int PORT_SERVIDOR = 7070;
	
	public static void main(String[] args) throws Exception {
		LSimLogger.setLoggerAsLocalLogger("Rest_client", "." + File.separator + "logs" + File.separator, true);
		RESTclient rs = new RESTclient();
		
		String string = "  Ephemeris";
		int posicio = 6;
		System.out.println("char: " + rs.charAt(IP_SERVIDOR, PORT_SERVIDOR, string, posicio));
		System.out.println("bytes: " + Arrays.toString(rs.getBytes(IP_SERVIDOR, PORT_SERVIDOR, string)));
		System.out.println("trim: " + rs.trim(IP_SERVIDOR, PORT_SERVIDOR, string));
	}
}
