package xai.rest.jettyserver.api;

import java.io.Serializable;

/**
 * @author Joan-Manuel Marques
 *
 */
public class Trimmed implements Serializable{
	private static final long serialVersionUID = 1L;

	String string;
	int num_spaces_removed;
	
	public Trimmed (String string, int num_spaces_removed){
		this.string = string;
		this.num_spaces_removed = num_spaces_removed;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trimmed other = (Trimmed) obj;
		if (num_spaces_removed != other.num_spaces_removed)
			return false;
		if (string == null) {
			if (other.string != null)
				return false;
		} else if (!string.equals(other.string))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "{string=" + string + ", num_spaces_removed=" + num_spaces_removed+ "}";
	}
}
