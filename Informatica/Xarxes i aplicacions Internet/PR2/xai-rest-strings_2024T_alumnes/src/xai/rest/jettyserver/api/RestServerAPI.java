package xai.rest.jettyserver.api;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.uoc.dpcs.lsim.logger.LoggerManager.Level;
import lsim.library.api.LSimLogger;
import xai.rest.utilitats.Utilitats;

/**
 * @author Noah Bobis
 *
 */

@Path("/strings")
public class RestServerAPI {

	/**
	 * charAt
	 *
	 * @param string
	 * @param position
	 * @return
	 */
	@GET
	@Path("/charAt/{string}/{position}")
	@Produces(MediaType.TEXT_PLAIN)
	public String charAt (@PathParam("string") String string, @PathParam("position") int position) {
		LSimLogger.log(Level.INFO, "charAt");
		LSimLogger.log(Level.INFO, "string: " + string);
		LSimLogger.log(Level.INFO, "position: " + position);

		String resultat = "";

	    try {
	        if (position < 0 || position >= string.length()) {
	            resultat = "Error: Position out of bounds";
	        } else {
	            resultat = String.valueOf(string.charAt(position));
	        }
	    } catch (Exception e) {
	        resultat = "Error: " + e.getMessage();
	    }

		LSimLogger.log(Level.INFO, "response: " + resultat);
		return resultat;
	}

	/**
	 * getBytes
	 *
	 * @param string
	 * @return
	 */
	@GET
	@Path("/getBytes/{string}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBytes(@PathParam("string") String string) {
		LSimLogger.log(Level.INFO, "getBytes");
		LSimLogger.log(Level.INFO, "string: "+string);

	    byte[] resultat = string.getBytes();
	    Gson gson = new Gson();

	    String jsonResponse = gson.toJson(resultat);
	    LSimLogger.log(Level.INFO, "response: " + jsonResponse);

	    return jsonResponse;
	}

	/**
	 * trim
	 *
	 * @param string
	 * @return a json object containing the parameters and the result
	 */
	@GET
	@Path("/trim/{string}")
	@Produces(MediaType.APPLICATION_JSON)
	public String trim(@PathParam("string") String string) {
		LSimLogger.log(Level.INFO, "trim");
		LSimLogger.log(Level.INFO, "string: "+string);

	    String trimmedString = string.trim();
	    int numSpacesRemoved = string.length() - trimmedString.length();

	    Trimmed resultat = new Trimmed(trimmedString, numSpacesRemoved);
	    Gson gson = new Gson();

	    String jsonResponse = gson.toJson(resultat);
	    LSimLogger.log(Level.INFO, "response: " + jsonResponse);

	    return jsonResponse;
	}
}
