def permuta(nombres, i, n):
    if i == n: 
        return llista_to_number(nombres, n)
    total = 0
    for k in range(i, n):
        intercanvi(nombres, i, k)
        total += permuta(nombres, i+1, n)
        intercanvi(nombres, i, k)
    return total

def intercanvi(nombres, i, k):
    temp = nombres[i]
    nombres[i] = nombres[k]
    nombres[k] = temp

def llista_to_number(nombres, n):
    num = 0
    for y in range(n):
        num += nombres[y] * (10 ** (n-y-1))
    return num

n = 4
nombres = [1, 2, 3, 4]
print(permuta(nombres, 0, n))
