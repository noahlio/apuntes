---
title: 'Mineria de dades: PAC1'
author: "Autor: Noah Bobis"
date: "Octubre 2024"
output:
  html_document:
    highlight: default
    number_sections: yes
    theme: cosmo
    toc: yes
    toc_depth: 2
    includes:
      in_header: 05.584-PAC-header.html
  pdf_document:
    highlight: zenburn
    toc: yes
  word_document: default
---

*****
# Exercici 1
*****


A partir d'un joc de dades que et resulti interessant, proposa un projecte complet de mineria de dades. L'organització de la resposta ha de coincidir en les fases típiques del cicle de vida d'un projecte de mineria de dades. **No cal fer les tasques de la fase**. 

Esperem, per tant, que es respongui de manera argumentada a les següents preguntes (Metodologia *CRISP-DM*):

1. **Comprensió del negoci** - Què necessita el negoci?
2. **Comprensió de les dades** - Quines dades tenim/necessitem? Estan netes?
3. **Preparació de les dades** - Com organitzem les dades per al modelatge?
4. **Modelatge** - Quines tècniques de modelatge hem d'aplicar?
5. **Avaluació** - Quin model compleix millor amb els objectius del negoci?
6. **Implementació** - Com accedeixen els interessats als resultats?

Per a cada fase indica quin és el objectiu de la fase i el producte que s'obtindrà. Utilitza exemples de quines i com podrien ser les tasques. Si hi ha alguna característica que fa diferent el cicle de vida d'un projecte de mineria respecte a d'altres projectes indica-ho.

Extensió mínima: 400 paraules.

*****
## Comprensió del negoci
*****

<br>

Aquest projecte de mineria de dades té com a objectiu l'anàlisi d'una botiga online de roba.

Mitjançant la mineria de dades es pretén identificar:

- Patrons de compra
- Preferències dels clients 
- Factors que influencien les decisions d'adquisició

Amb l'objectiu de millorar:

- Experiència de compra dels clients
- Vendes
- Taxa de devolucions
- Estratègies de màrqueting
- Recomanacions de productes
- Aprovisionament de mercaderia

<br>

*****
## Comprensió de les dades
*****

<br>

Els registres de compres en línia de la botiga online es componen de:

- Històric de compres
- Històric de devolucions
- Informació demogràfica dels clients
- Dades de navegació de la botiga
- Registre de productes (aprovisionament i venda)

<br>

Les dades d'històric (compres i devolucions) i els registres de productes són les dades més estables, els històrics tenen dades d'usuari contrastades i el registre de productes és actualitzat en temps real.

En canvi, les dades de navegació i demogràfiques poden no ser tan significatives pel que fa als clients sense compres realitzades, ja són dades no contrastades. És per això que es farà una segmentació de dades, per tal de poder analitzar totes les dades disponibles amb la perspectiva d'anàlisi requerida.

<br>

*****
## Preparació de les dades
*****

<br>

En el procés de preparació de les dades s'unificaren les dades de clients duplicats, deixant totes les seves dades relacionades (històrics, navegació, dades, etc.) a un únic usuari.

S'eliminaran les dades incorrectes dels usuaris sense compres per evitar malentesos demogràfics (direccions, codis postals i telèfons inexistents o incorrectes).

Es generaran variables derivades (freqüència de compra, devolució i cerca) per tenir una comprensió de comportament més exacte dels clients i poder segmentar en grups més fàcilment.

<br>

*****
## Modelatge
*****

<br>

Les tècniques més adequades aplicables són:

- **Regressió logística:** Mesura la probabilitat de devolució d'un producte basant-se en factors (categoria del producte, històric de compres, comportament de navegació...)
- **Clustering k-means:** Segmentació dels clients segons el comportament de compra (clients recurrents, nous, amb alt risc de devolució...)
- **Sistemes de recomanació (filtratge col·laboratiu):** Suggeriencia de productes personalitzats als clients en funció de les seves preferències i l'activitat de navegació.
- **Anàlisi de sèrie temporal:** Previsió de la futura demanda, amb l'objectiu d'optimitzar l'inventari.

En aquesta fase, també es definirà el pla de prova per assegurar que els models siguin robustos. Això inclou la divisió de les dades en conjunts d'entrenament i de prova, utilitzant l'estratègia de validació creuada per garantir que el model generalitzi bé amb noves dades.

<br>

*****
## Avaluació
*****

<br>

### Avaluació dels resultats

Primerament, es comprovarà si els models s'adeqüen als criteris establerts en la fase de comprensió del negoci. Per exemple, si el model de predicció de devolucions és capaç d'identificar quins clients tenen més probabilitats de tornar els productes, o si els sistemes de recomanació estan generant recomanacions rellevants.

S'utilitzaràn mètriques (accuracy, el recall, precisió i AUC-ROC) per avaluar el rendiment dels models. Si el model de recomanacions augmenta la taxa de conversió i els clients que han comprat productes recomanats presenten una menor taxa de devolució, es consideraràn resultats exitósos.

### Revisió del procés

Un cop avaluats els models, es revisarà el procés global de la mineria de dades. En aquesta fase es comprovarà si s'han seguit correctament totes les etapes de CRISP-DM. Es revisarà si poden haber ajustaments en la preparació de les dades o el modelatge.

Per exemple, si el model presenta un baix rendiment en certs segments de clients, es pot decidir tornar a la fase de preparació de dades per crear noves variables o ajustar l'enfocament.

### Determinació dels següents passos

Finalment, es decidirà si el projecte està llest per passar a la fase d'implementació o si cal continuar refinant els models. Si el model compleix amb els objectius del negoci, es pot procedir a la implementació. 

Si es detecten àrees de millora, es pot iterar en les fases de preparació o modelatge per afinar els resultats. A més, es podria explorar l'inici de nous projectes de mineria de dades per abordar altres àrees de negoci que no s'han tractat en aquest projecte inicial.

<br>

*****
## Implementació
*****

<br>

### Planificació del desplegament

Per implementar el model es duran a terme les següents accions:

- Integració del sistema de recomanació dins de la botiga en línia.
- Creació de dashboards per a que màrqueting i vendes visualitzin patrons de compra i prediccions en temps real.
- Integració a nivell intern del sistema de previsió de futura demanda.

### Planificació del monitoratge i manteniment

Un cop en producció, periodicament s'ha d'assegurar el correcte funcionament, amb un pla de monitoratge que permeti detectar anomalies, canvis en la precisió dels models o l'aparició de noves dades que requereixen reentrenar el model.

### Producció de l'informe final

Al informe final es resumeix els resultats obtinguts, les tècniques de modelatge utilitzades i l'afectació en el negoci. Aquest informe inclou visualitzacions, interpretacions dels models i suggeriments sobre com utilitzar les prediccions apropiadament.

### Revisió del projecte

Finalment, es realitza una revisió del projecte avaluant el funcionament, quins problemes s'han trobat i com es poden millorar futurs projectes, ajudant a millorar els processos interns de l'empresa en termes de gestió de projectes de mineria de dades.

<br>

*****
# Exercici 2
*****
A partir del joc de dades utilitzat a l'exemple de la PAC, realitza les tasques prèvies a la generació d’un model de mineria de dades explicades en els mòduls  "El procés de mineria de dades" i "Preprocessat de les dades i gestió de característiques". 

Es demana utilitzar de referència l'exemple però canviar l'enfocament i analitzar les dades en funció de les diferents dimensions que presenten les dades. Així, no es pot treballar amb la combinació de les variables analitzades a l'exemple: "FATALS","DRUNK_DR","VE_TOTAL","VE_FORMS","PVH_INVL","PEDS","PERSONS","PERMVIT","PERNOTMVIT". Es pot analitzar qualsevol altre combinació que pot incloure (o no) algunes d'aquestes variables amb altres de noves.

Opcionalment i valorable es poden afegir a l'estudi de dades d'altres anys per a realitzar comparacions temporals (https://www.nhtsa.gov/file-downloads?p=nhtsa/downloads/FARS/) o afegir altres fets a estudiar relacionats, per exemple el consum de drogues en els accidents (https://static.nhtsa.gov/nhtsa/downloads/FARS/2020/National/FARS2020NationalCSV.zip)

## Anàlisi exploratòria

A continuació s'agafaran les dades de accidents.csv i drugs.csv.

```{r echo=TRUE, message=FALSE, warning=FALSE}

if (!require('dplyr')) install.packages('dplyr'); library('dplyr')
if (!require('lubridate')) install.packages('lubridate'); library('lubridate')
if (!require('knitr')) install.packages('knitr'); library('knitr')

accidents <- read.csv("accidents.csv")
drugs <- read.csv("drugs.csv")

drugs_summary <- drugs %>%
  group_by(ST_CASE) %>%
  mutate(
    MOST_FREQUENT_DRUG = {
      filtered_drugresname <- DRUGRESNAME[DRUGRES > 1 & DRUGRES < 999]
      filtered_drugresname <- filtered_drugresname[!filtered_drugresname %in% c(
        "Not Reported", 
        "Other Drug", 
        "Test For Drug, Results Unknown", 
        "Tested For Drugs, Drugs Found, Type unknown/Positive"
      )]
      freq_table <- table(filtered_drugresname)
      if (length(freq_table) > 0) {
        max_freq <- max(freq_table)
        if (sum(freq_table == max_freq) > 1) {
          NA
        } else {
          names(freq_table[freq_table == max_freq])
        }
      } else {
        NA
      }
    }
  ) %>%
  summarise(
    TOTAL_CARS = n_distinct(VEH_NO),
    TOTAL_PEOPLE = n_distinct(VEH_NO, PER_NO),
    TOTAL_TEST_NOT_GIVEN = sum(drugs$DRUGRES[drugs$ST_CASE == ST_CASE] == 0),
    TOTAL_TEST_DRUGS_NEGATIVE = sum(drugs$DRUGRES[drugs$ST_CASE == ST_CASE] == 1),
    TOTAL_TEST_DRUGS_POSITIVE = n_distinct(paste(VEH_NO[DRUGRES > 1 & DRUGRES < 999], PER_NO[DRUGRES > 1 & DRUGRES < 999])),
    TOTAL_TEST_UNKNOWN_RESULTS = sum(drugs$DRUGRES[drugs$ST_CASE == ST_CASE] == 999),
    MOST_FREQUENT_DRUG = first(MOST_FREQUENT_DRUG),
    .groups = 'drop'
  )

data <- full_join(accidents, drugs_summary, by = "ST_CASE")

kable(head(data), caption = "Head of Merged Data")
```

Veiem que tenim 78 variables i 35935 registres

Descripció de variables:

- ST_CASE identificador d’accident

FETS A ESTUDIAR

- TOTAL_CARS total de cotxes involucrats
- TOTAL_PEOPLE total de persones involucrades
- TOTAL_TEST_NOT_GIVEN total de tests no fets
- TOTAL_TEST_DRUGS_NEGATIVE total de tests negatius
- TOTAL_TEST_DRUGS_POSITIVE total de tests positius
- TOTAL_TEST_UNKNOWN_RESULTS total de tests amb resultat desconegut
- MOST_FREQUENT_DRUG droga més freqüent

DIMENSIÓ VICTIMES

- FATALS morts
- DRUNK_DR conductors beguts
- VE_TOTAL nombre de vehicles implicats en total
- VE_FORMS nombre de vehicles en moviment implicats
- PVH_INVL nombre de vehicles estacionats implicats
- PEDS nombre de vianants implicats
- PERSONS nombre ocupants de vehicle implicats
- PERMVIT nombre conductors i ocupants implicats
- PERNOTMVIT nombre vianants, ciclistes, a cavall… qualsevol cosa menys vehicle motoritzat

DIMENSIÓ GEOGRÀFICA

- STATE codificació d’estat
- STATENAME nom d’estat
- COUNTY identificador de contat
- COUNTYNAME comtat
- CITY identificador de ciutat
- CITYNAME ciutat
- NHS 1 ha passat a autopista del NHS 0 no
- NHSNAME TBD
- ROUTE identificador de ruta
- ROUTENAME ruta
- TWAY_ID via de transit (1982)
- TWAY_ID2 via de transit (2004)
- RUR_URB identificador de segment rural o urbà
- RUR_URBNAME segment rural o urbà
- FUNC_SYS classificació funcional segment
- FUNC_SYSNAME TBD
- RD_OWNER identificador propietari del segment
- RD_OWNERNAME propietari del segment
- MILEPT milla int
- MILEPTNAME milla chr
- LATITUDE latitud int
- LATITUDENAME latitud chr
- LONGITUD longitud int
- LONGITUDNAME longitud chr
- SP_JUR codi jurisdicció
- SP_JURNAME jurisdicció


DIMENSIÓ TEMPORAL

- DAY dia
- DAYNAME dia repetit
- MONTH mes
- MONTHNAME nom de mes
- YEAR any
- DAY_WEEK dia de la setmana
- DAY_WEEKNAME nom de dia de la setmana
- HOUR hora
- HOURNAME franja hora
- MINUTE minut int
- MINUTENAME minut chr

DIMENSIÓ CONDICIONS ACCIDENT

- HARM_EV codi primer esdeveniment de l’accident que produeixi danys o lesions
- HARM_EVNAME primer esdeveniment de l’accident que produeixi danys o lesions
- MAN_COLL codi de posició dels vehicles
- MAN_COLLNAME posició dels vehicles
- RELJCT1 codi si hi ha àrea d’intercanvi
- RELJCT1NAME si hi ha àrea d’intercanvi
- RELJCT2 codi proximitat encreuament
- RELJCT2NAME proximitat encreuament
- TYP_INT codi tipus d’intersecció
- TYP_INTNAME tipus d’intersecció
- WRK_ZONE codi tipologia d’obres
- WRK_ZONENAME tipologia d’obres
- REL_ROAD codi ubicació vehicle a la via
- REL_ROADNAME ubicació vehicle a la via
- LGT_COND codi condició lumínica
- LGT_CONDNAME condició lumínica

DIMENSIÓ METEOROLOGIA

- WEATHER codi temps
- WEATHERNAME : temps

ALTRES

- SCH_BUS codi si vehicle escolar implicat
- SCH_BUSNAME vehicle escolar implicat
- RAIL codi si dins o a prop pas ferroviari
- RAILNAME si dins o a prop pas ferroviari

DIMENSIÓ SERVEI EMERGENCIES

- NOT_HOUR hora notificació a emergències int
- NOT_HOURNAME hora notificació a emergències franja
- NOT_MIN minut notificació a emergències int
- NOT_MINNAME minut notificació a emergències chr
- ARR_HOUR hora arribada emergències int
- ARR_HOURNAME hora arribada emergències franja
- ARR_MIN minut arribada emergències int
- ARR_MINNAME minut arribada emergències franja
- HOSP_HR hora arribada hospital int
- HOSP_HRNAME hora arribada hospital franja
- HOSP_MN minut arribada hospital int
- HOSP_MNNAME : minut arribada hospital franja

## Preprocessament i gestió de característiques

El següent pas serà la neteja de dades, mirant si hi ha valors buits o nulls.

**Registres nuls:**

```{r echo=TRUE, message=FALSE, warning=FALSE}
colSums(is.na(data))
```

**Registres buits:**

```{r echo=TRUE, message=FALSE, warning=FALSE}
colSums(data=="")
```

Veiem que hi ha valors nuls (23245) en el camp MOST_FREQUENT_DRUG, però això té un significat, simbolitza que no hi ha cap substància predominant.
També trobem espais en blanc (27102) el camp TWAY_ID2. Valorem no fer cap acció d'eliminar registres, ja que aquest camp no l'utilitzarem.

Anem a crear histogrames i descriure els valors per veure les dades en general d'aquests atributs per fer una primera aproximació al contingut de les dades:

```{r echo=TRUE, message=FALSE, warning=FALSE}
if(!require('ggplot2')) install.packages('ggplot2'); library('ggplot2')
if(!require('Rmisc')) install.packages('Rmisc'); library('Rmisc')
if(!require('xfun')) install.packages('xfun'); library('xfun')

summary(data[c("TOTAL_CARS", "TOTAL_PEOPLE","TOTAL_TEST_DRUGS_POSITIVE")])
```

```{r echo=TRUE, message=FALSE, warning=FALSE}
histList<- list()

n = c("TOTAL_CARS", "TOTAL_PEOPLE","TOTAL_TEST_DRUGS_POSITIVE")
dataAux= data %>% select(all_of(n))
for(i in 1:ncol(dataAux)){
  col <- names(dataAux)[i]
  ggp <- ggplot(dataAux, aes_string(x = col)) +
    geom_histogram(bins = 30, fill = "cornflowerblue", color = "black",ggtittle = "Comptador d'ocurrències per variable") 
      histList[[i]] <- ggp
}
 multiplot(plotlist = histList, cols = 1)
```

**Observacions:**

Nombre de cotxes: Tots els accidents recollits en aquestes dades reporten un cotxe com a mínim. Sent l'accident més greu amb quinze vehicles i veiem que la distribució tendeix a dos cotxes per accident.

Nombre de persones: Tots els accidents recollits en aquestes dades reporten una persona com a mínim. Sent l'accident més greu amb 61 persones i veiem que la distribució tendeix a estar entre dues o tres persones per accident.

Persones sota efectes involucrats a l'accident: Analitzarem amb més detall aquesta dada més endavant per derivar una nova dada: Accidents on les drogues estan presents o no. D'entrada la mitjana és de 0.68% d'accidents on intervenen persones sota efectes de les drogues. La franja va des de cap involucrat fins a vuit com a màxim.

<br>

```{r echo=TRUE, message=FALSE, warning=FALSE}
summary(data[c("TOTAL_TEST_NOT_GIVEN","TOTAL_TEST_DRUGS_NEGATIVE", "TOTAL_TEST_DRUGS_POSITIVE", "TOTAL_TEST_UNKNOWN_RESULTS")])
```

```{r echo=TRUE, message=FALSE, warning=FALSE}
histList<- list()
n = c("TOTAL_TEST_NOT_GIVEN","TOTAL_TEST_DRUGS_NEGATIVE", "TOTAL_TEST_DRUGS_POSITIVE", "TOTAL_TEST_UNKNOWN_RESULTS")
dataAux = data %>% select(all_of(n))
for(i in 1:ncol(dataAux)){
  col <- names(dataAux)[i]
  ggp <- ggplot(dataAux, aes_string(x = col)) +
    geom_histogram(bins = 30, fill = "cornflowerblue", color = "black") 
      histList[[i]] <- ggp
}
 multiplot(plotlist = histList, cols = 1)
```

**Observacions pel que fa als resultats dels tests:**

Nombre de tests no realitzats: El nombre de tests no realitzats indica que hi ha una variabilitat significativa en la recollida de mostres. La mitjana de tests no realitzats és d'1.36, amb un mínim de 0 i un màxim de 60.

Nombre de tests negatius: Els tests negatius, amb una mitjana de 0.37 i un màxim de 10, indiquen que hi ha una part dels accidents en què els tests de substàncies han resultat negatius. La majoria dels accidents no presenten tests negatius, el que suggereix que en pocs casos es fa la prova i no es detecta la presència de drogues en les persones involucrades.

Nombre de tests positius: El nombre de tests positius, amb una mitjana de 0.68 i un màxim de 8, revela que una proporció d'accidents involucra conductors que han donat positiu en les proves de drogues. La mediana és d'1, la qual cosa indica que la meitat dels accidents amb tests positius han presentat almenys una persona que ha donat positiu.

Nombre de tests no determinants: El nombre de tests no determinants, amb una mitjana de 0.01 i un màxim de 3, mostra que, en pocs casos, els resultats dels tests no són concloents. La majoria dels accidents no impliquen tests amb resultats desconeguts, cosa que pot indicar que els protocols de prova són generalment eficaços.

<br>

```{r echo=TRUE, message=FALSE, warning=FALSE}
most_frequent_counts <- table(data$MOST_FREQUENT_DRUG)
most_frequent_summary <- sort(most_frequent_counts, decreasing = TRUE)
most_frequent_df <- as.data.frame(most_frequent_summary)
colnames(most_frequent_df) <- c("Drug", "Count")
top_10_drugs <- most_frequent_df %>%
  arrange(desc(Count)) %>%
  head(10)

ggp <- ggplot(top_10_drugs, aes(x = reorder(Drug, -Count), y = Count)) +
  geom_bar(stat = "identity", fill = "cornflowerblue", color = "black") +
  labs(title = "Top 10 Drogues més freqüents per accident",
       x = "Droga més freqüent",
       y = "Nombre d'accidents") +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))

print(ggp)
```

**Observacions pel que fa a la droga més freqüent:**

Tipus de droga més freqüent: Segons el gràfic representat hi ha una gran predominança entre les drogues provinents del cànnabis, indicant un possible problema d'addicció en la població o una alteració més greu en les persones que altres drogues.

<br>

**Analisí de la presencia de drogues als accidents:**

```{r echo=TRUE, message=FALSE, warning=FALSE}
data$drogues <- ifelse(data$TOTAL_TEST_DRUGS_POSITIVE %in% c(0), 0, 1)
counts <- table(data$drogues)
barplot(prop.table(counts),col=c("green","red"), main="Accidents amb persones drogades", legend.text=c("No Drogues / No verificat","Sí Drogues"),xlab ="Presencia Drogues", ylab = "Percentatge",ylim=c(0,0.8) )
```

Com es pot veure, prop del 55% dels accidents, algun dels implicats està sota l'efecte de les drogues. No pas vol dir que totes les vegades siguin el causant directe, però pot ser una de les causes implicades.

<br>

**Analisí de la absència de drogues als accidents:**

```{r echo=TRUE, message=FALSE, warning=FALSE}
data$no_drogues <- ifelse(data$TOTAL_TEST_DRUGS_NEGATIVE %in% c(0), 0, 1)
counts <- table(data$no_drogues)
barplot(prop.table(counts),col=c("red","green"), main="Accidents amb persones no drogades", legend.text=c("Si Drogues / No verificat", "No Drogues"),xlab ="Absència Drogues", ylab = "Percentatge",ylim=c(0,0.8) )
```

Com es pot observar, en pocs dels casos on hi ha hagut accidents les persones implicades no donaven positiu als tests de drogues, sent al voltant del 30%.

<br>

**Relació entre index (tests positius / persones) i total de persones:**

```{r echo=TRUE, message=FALSE, warning=FALSE}
data_summary <- data %>%
  mutate(INDEX = (TOTAL_TEST_DRUGS_POSITIVE / TOTAL_PEOPLE) * 100)

ggplot(data_summary, aes(x = TOTAL_PEOPLE, y = INDEX)) +
  geom_point(aes(color = INDEX), size = 3, alpha = 0.6) + 
  geom_smooth(method = "lm", color = "red", se = FALSE, linetype = "dashed") +
  scale_color_gradient(low = "lightblue", high = "blue") +
  scale_y_continuous(limits = c(0, 100)) +
  labs(
    title = "Relació entre l'índex (tests positius / persones) i total de persones",
    x = "Total de persones involucrades",
    y = "Índex de tests positius (%)",
    color = "Índex de positius (%)"
  ) +
  theme_minimal() +
  theme(
    plot.title = element_text(hjust = 0.5, face = "bold", size = 14),
    axis.title = element_text(size = 12),
    legend.position = "right"
  )
```

Finalment, mitjançant l'índex que relaciona el nombre de positius dins de la població es pot veure que hi ha una relació inversa entre el nombre de positius i el total de persones involucrades. La majoria dels casos tot i haver-hi puntualitats d'índexs elevats, no són majoritaris, i a mesura que hi ha més persones involucrades l'índex baixa. Aquest fet pot estar informant de la perillositat de l'ús de drogues, ja que sembla que no és necessari un índex de positius alt per ocasionar un accident.