from qiskit.circuit.library import QuantumVolume
from qiskit.primitives import Sampler
from qiskit.visualization import plot_distribution

qc = QuantumVolume(4)
qc.measure_all()
results = Sampler().run(qc).result()
plot_distribution(results.quasi_dists[0], sort='value_desc')
