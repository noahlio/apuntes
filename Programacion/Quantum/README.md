# Proyecto de Aprendizaje Qiskit

<br>

Este proyecto está diseñado para aprender y explorar Qiskit, un framework de computación cuántica para Python.

Qiskit es un conjunto de herramientas de código abierto para la programación cuántica en Python. 
Permite la creación, simulación y ejecución de algoritmos cuánticos en sistemas cuánticos reales y simulados.

<br>

## Configuración del Entorno

<br>

Para asegurar la replicabilidad del entorno de desarrollo, sigue estos pasos:

<br>

### Crear el entorno Conda:

Utiliza el archivo `environment.yml` proporcionado para crear el entorno Conda:

```
conda env create -f environment.yml
```

Al final del environment esta la ruta a anaconda3 y el nombre del entorno conda, cambiarlo segun sea necesario.

<br>

### Activar el entorno Conda:

```
conda activate nombre_del_entorno
```

<br>

## Iniciar jupyter notebook

Una vez activado el entorno Conda, puedes iniciar Jupyter Notebook para empezar a trabajar con Qiskit:

```
jupyter notebook
```

Esto abrirá Jupyter Notebook en tu navegador web predeterminado.


