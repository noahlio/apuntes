= Flutter
Author Name <noahbobis0512@gmail.com>; Noah Bobis 
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

:imagesdir: ./Imagenes/

== Iniciar proyecto

{sp}+

=== Recursos

{sp}+

- Flutter: sdk

----
# instalar flutter
sudo snap install flutter --classic
# ver donde esta sdk (si no esta lo instala)
flutter sdk-path
----

- Android studio: Flutter pluggin

image::pluginFlutter.png[]

- Android studio: Emulador

image::emulatorFlutter.png[]

{sp}+

=== Creacion de proyecto

{sp}+

Con el pluggin de Flutter en android studio aparece la opción de crear nuevo proyecto Flutter.

image::initFlutterProject.png[]

Seleccionar proyecto Flutter y poner el sdk

image::createFlutterProject.png[]

Configurar ruta de creación, nombre organización, lenguajes, etc.

image::configFlutterProject.png[]

== Estructura

{sp}+

{sp}+

== Widgets

{sp}+

Son elementos de interfaz grafica para ver o interactuar.

{sp}+

=== Tipos de widget

{sp}+

*Stateless Widget:* Widgets sin estado que solo se dibujan una vez y no se pueden cambiar sus valores. Es importante utilizarlos en todos los componentes que no necesiten cambiar su valor.

*Stateful Widget:* Widgets con estado que solo se dibujan una vez y no se puedencambiar sus valores. Para crear un componente statefulWidget se necesita un Objeto `State`, este tendra las variables que pueden modificarse y devolveran la parte mutable del widget.

{sp}+

== Patrones de arquitectura

{sp}+

{sp}+

