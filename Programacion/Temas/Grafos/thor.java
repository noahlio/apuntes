import java.util.*;
public class thor {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		for(int a=input.nextInt();a>0;a--) {
			ArrayList<nodo> nodos=new ArrayList<nodo>();
			int b=input.nextInt();
			input.nextLine();
			int pos=0;
			nodo nodo1 = null, nodo2 = null;
			for(int c=0;c<b-1;c++) {
				String[] nombres=input.nextLine().split(" HIJO DE ");
				nodo n1=new nodo(nombres[0]);
				nodo n2=new nodo(nombres[1]);
				n1.addNodo(n2);
				n2.addNodo(n1);
				boolean in1 = false, in2=false;
				for(int p=0;p<nodos.size();p++) { 
					if(nodos.get(p).equals(n1)) {
						nodos.get(p).addNodo(n2);
						in1=true;
					}
					if(nodos.get(p).equals(n2)) {
						nodos.get(p).addNodo(n1);
						in2=true;
					}
				}
				if(!in1)nodos.add(n1);
				if(!in2)nodos.add(n2);
			}
			String familiar=input.nextLine();
			for(nodo n:nodos) {
				if(n.nombre.equals("THOR")) nodo1=n;
				if(n.nombre.equals(familiar)) nodo2=n;
			}
			System.out.println(familia(nodos,nodo1, nodo2, 0)?"SI":"NO");
		}
	}
	public static boolean familia(ArrayList<nodo> nodos, nodo n1, nodo n2, int c) {
		if(c>=nodos.size()) return false;
		if (n1.equals(n2)) return true;
		for(nodo n:n1.familiares) {
			if(familia(nodos,n,n2,c+1)) return true;
		}
		return false;
	}
}
class nodo{
	String nombre;
	ArrayList<nodo> familiares=new ArrayList<nodo>();
	public void addNodo(nodo n) {
		familiares.add(n);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		nodo other = (nodo) obj;
		return Objects.equals(nombre, other.nombre);
	}
	public nodo(String nombre) {
		this.nombre=nombre;
	}	
}
