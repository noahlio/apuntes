# -*- coding: utf-8 -*-
# Copyright (c) 2023 Noah Bobis

import pandas as pd

#axis=0: Indica que la operación se realizará a lo largo de las filas.
#axis=1: Indica que la operación se realizará a lo largo de las columnas.


def main():
    dataframe = getDataframe()
    #dataframe = getDataframeFromCSV('studentsDataset.csv')
    #dataframe = getDataframeRenameColumns(dataframe)
    #dataframe = getDataframeMarksAsString(dataframe)
    showDataframe(dataframe)
    #showDataframeLength(dataframe)
    #showDataframeNTopRows(Dataframe, 3)
    #showDataframeNLastRows(dataframe, 3)
    #showDataframeShape(dataframe)
    #showDataframeInformation(dataframe)
    #showDataframeNullValues(dataframe)
    #showDataframeNullValuesSummary(dataframe,1)
    #showDataframeOverallStatistics(dataframe)
    #showDataframeColumnUniqueValues(dataframe['Gender'])
    #showDataframeColumnNumberUniqueValues(dataframe['Gender'])
    #showDataframeColumnCountUniqueValues(dataframe['Gender'])
    #showDataframeStudentsBetweenRangMarks(dataframe, 90, 100)
    #showDataframeMarksStatistics(dataframe)
    #showDataframeExtraPoint(dataframe)
    #showDataframe(dataframeExtraPoint)
    #showDataframeRoundedMarks(dataframe)
    #showDataframeNamesLength(dataframe)
    #showDataframeRefactorGender(dataframe)
    #showDataframeSort(dataframe, 'Marks', False)
    #showDataframeSort(dataframe, ['Marks', 'Gender'], False)
    #showFilteredDataframeColumns(dataframe, dataframe['Gender'] == 'Female', ['Name', 'Marks'])
    #showDataframeStudents(dataframe, ['Aadhya', 'Mittal'])
    #showDataframeStudentsWithXLetter(dataframe, 'e')
    
    #dataframe2 = getDataframe()
    #dropDataframeColumn(dataframe2, 'Gender')
    #dropDataframeColumn(dataframe2, ['Gender', 'Marks'])
    #showDataframeColumns(dataframe2)
    #showDataframeRows(dataframe2)

def getDataframe():
    dataset ={
        'Name': pd.Series(['Priyang','Aadhya','Krisha','Vedant','Parshv', 'Mittal','Archana']),
        'Marks':pd.Series([98,89,99,87,90,83,99]),
        'Gender': pd.Series(['Male','Female','Female','Male','Male', 'Female','Female'])
    }
    dataframe=pd.DataFrame(dataset)
    return dataframe

def getDataframeFromCSV(name):
    return pd.read_csv(name)

def getDataframeRenameColumns(dataframe):
    return dataframe.rename(columns={'Name': 'student_name', 'Marks': 'student_marks', 'Gender': 'student_gender'})

def getDataframeMarksAsString(dataframe):
    dataframe['Marks'] = dataframe['Marks'].astype(str)
    return dataframe

def showDataframeColumns(dataframe):
    print(dataframe.columns)

def showDataframeRows(dataframe):
    print(dataframe.index)

def showDataframe(dataframe):
    print(dataframe)

def showDataframeLength(dataframe):
    print(len(dataframe))

def showDataframeNTopRows(dataframe, n):
    print(dataframe.head(n))

def showDataframeNLastRows(dataframe, n):
    print(dataframe.tail(n))

def showDataframeShape(dataframe):
    shape = dataframe.shape
    print('Rows: {}, Columns: {}'.format(shape[0], shape[1]))

def showDataframeInformation(dataframe):
    print(dataframe.info())

def showDataframeNullValues(dataframe):
    print(dataframe.isnull())
    
def showDataframeNullValuesSummary(dataframe, isSummaryByRow):
    print(dataframe.isnull().sum(axis=isSummaryByRow))

def showDataframeOverallStatistics(dataframe):
    #print(dataframe['Gender'].describe())
    print(dataframe.describe(include='all'))

def showDataframeColumnUniqueValues(column):
    print(column.unique())

def showDataframeColumnNumberUniqueValues(column):
    print(column.nunique())

def showDataframeColumnCountUniqueValues(column):
    print(column.value_counts())

def showDataframeStudentsBetweenRangMarks(dataframe, minValue, maxValue):
    #filtered_dataframe = dataframe[(dataframe['Marks'] >= minValue) & (dataframe['Marks'] <= maxValue)]
    #print((dataframe['Marks'] >= minValue) & (dataframe['Marks'] <= maxValue))
    #print(filtered_dataframe)
    dataframe_filter = dataframe['Marks'].between(minValue, maxValue)
    print(sum(dataframe_filter))
    print(dataframe[dataframe_filter])

def showDataframeMarksStatistics(dataframe):
    print('Average: {} \nMax: {} \nMin: {}'.format(dataframe['Marks'].mean(), dataframe['Marks'].min(), dataframe['Marks'].max()))

def showDataframeExtraPoint(dataframe):
    dataframeExtraPoint = dataframe.copy()
    dataframeExtraPoint['Marks'] = dataframeExtraPoint['Marks'].apply(lambda x:x+1)
    print(dataframeExtraPoint)

def showDataframeRoundedMarks(dataframe):
    dataframeRoundedMarks = dataframe.copy()
    dataframeRoundedMarks['Rounded Marks'] = dataframe['Marks'].apply(roundMark)
    print(dataframeRoundedMarks)

def showDataframeRefactorGender(dataframe):
    dataframeNewGender = dataframe.copy()
    dataframeNewGender['Gender'] = dataframe['Gender'].map({'Male': 'M', 'Female': 'F'})
    print(dataframeNewGender)

def roundMark(x):
    if x%10 >= 5:
        return x + (10 - x%10)
    return x - x%10

def showDataframeNamesLength(dataframe):
    print(dataframe['Name'].apply(len))

def showDataframeSort(dataframe, sort_column, asc):
    print(dataframe.sort_values(by=sort_column, ascending = asc))

def dropDataframeColumn(dataframe, column_name):
    # inplace change the object
    dataframe.drop(column_name, axis=1, inplace = True)

def showFilteredDataframeColumns(dataframe, filter, columns):
    print(dataframe[filter][columns])

def showDataframeStudents(dataframe, students):
    print(dataframe[dataframe['Name'].isin(students)])

def showDataframeStudentsWithXLetter(dataframe, letter):
    print(dataframe[dataframe['Name'].str.contains(letter)])

main()
