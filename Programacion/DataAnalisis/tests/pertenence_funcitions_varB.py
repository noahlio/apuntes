import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

x = np.linspace(0, 10, 100)

L = fuzz.trapmf(x, [0, 0, 1, 4])
M = fuzz.trapmf(x, [1, 5, 5, 7])
H = fuzz.trapmf(x, [6, 9, 10, 10])

plt.figure(figsize=(12, 6))
plt.plot(x, L, label="L", color="orange")
plt.plot(x, M, label="M", color="green")
plt.plot(x, H, label="H", color="red")

plt.text(1.75, 0.5, "(4-x)/3", color="orange")

plt.text(3.5, 0.5, "(x-1)/4", color="green")
plt.text(6.1, 0.5, "(7-x)/2", color="green")

plt.text(7.75, 0.5, "x/3 - 2", color="red")

plt.title("Funcions de Pertinença VarB")
plt.xlabel("Valors")
plt.ylabel("Graus de pertinença")
plt.legend()
plt.grid()
plt.show()

