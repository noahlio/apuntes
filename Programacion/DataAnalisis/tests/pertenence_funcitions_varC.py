import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

x = np.linspace(-5, 10, 100)

VL = fuzz.trapmf(x, [-5, -5, -4, -2])
L = fuzz.trapmf(x, [-4, -2, 0, 4])
M = fuzz.trapmf(x, [0, 2, 2, 4])
H = fuzz.trapmf(x, [2, 4, 10, 10])

plt.figure(figsize=(12, 6))
plt.plot(x, VL, label="VL", color="blue")
plt.plot(x, L, label="L", color="orange")
plt.plot(x, M, label="M", color="green")
plt.plot(x, H, label="H", color="red")

plt.text(-4.75, 0.7, "-1 - 0.5x", color="blue")

plt.text(-2.75, 0.5, "0.5x + 1", color="orange")
plt.text(-1, 0.8, "1 - 0.25x", color="orange")

plt.text(1.1, 0.5, "0.5x", color="green")
plt.text(2.25, 0.9, "2 - 0.5x", color="green")

plt.text(4, 0.8, "0.5x - 1", color="red")

plt.title("Funcions de Pertinença VarC")
plt.xlabel("Valors")
plt.ylabel("Graus de pertinença")
plt.legend()
plt.grid()
plt.show()

