import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

x = np.linspace(0, 10, 100)

L = fuzz.trapmf(x, [0, 0, 3, 6])
M = fuzz.trapmf(x, [4, 5, 5, 6])
H = fuzz.trapmf(x, [3, 7, 10, 10])

plt.figure(figsize=(12, 6))

plt.plot(x, L, label="L", color="orange")
plt.plot(x, M, label="M", color="green")
plt.plot(x, H, label="H", color="red")

plt.fill_between(x, 0, np.minimum(L, 0.25), color='gray', alpha=1)
plt.fill_between(x, 0, np.minimum(M, 0.5), color='gray', alpha=1)

plt.title("Out")
plt.legend()
plt.grid()

plt.show()

