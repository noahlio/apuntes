class Sexo
  MALE = 'M'
  FEMALE = 'F'
  OTHER = 'X'

  def self.values
    [MALE, FEMALE, OTHER]
  end

  def self.to_s(value)
    case value
    when MALE then 'Masculino'
    when FEMALE then 'Femenino'
    when OTHER then 'No Especificado'
    else 'Desconocido'
    end
  end

  def self.valid?(value)
    values.include?(value)
  end
end

class Persona
  def initialize(nombre, edad, sexo)
    @nombre = nombre
    @edad = edad
    self.sexo = sexo
  end

  attr_reader :nombre

  attr_reader :edad

  def sexo
    @sexo
  end

  def sexo=(valor)
    if Sexo.valid?(valor)
      @sexo = valor
    else
      raise ArgumentError, "Sexo inválido. Debe ser 'M', 'F', o 'X'."
    end
  end

  def to_s
    "Nombre: #{@nombre}, Edad: #{@edad}, Sexo: #{Sexo.to_s(@sexo)}"
  end
end

persona = Persona.new("Ana", 25, Sexo::FEMALE)

puts persona

begin
  persona.sexo = 'A'
rescue ArgumentError => e
  puts e.message
end
