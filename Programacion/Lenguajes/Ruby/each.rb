# Iterar sobre un arreglo
[1, 2, 3].each do |num|
  print "#{num} "
end

puts

# Iterar sobre un hash
{a: 1, b: 2, c: 3}.each do |key, value|
  print "#{key}: #{value} "
end

puts

# Iterar con índice
["a", "b", "c"].each_with_index do |item, index|
  print "#{index}: #{item} "
end

puts
