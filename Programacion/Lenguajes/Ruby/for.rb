# Imprimir números del 1 al 5 separados por espacios
for i in 1..5
  print "#{i} "
end
puts  # Añade un salto de línea al final

# Imprimir elementos del arreglo separados por espacios
arr = ["a", "b", "c"]
for item in arr
  print "#{item} "
end
puts  # Añade un salto de línea al final

