= Entornos de desarrollo
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:imagesdir: ./Imagenes/
:toclevels: 3
:numbered:
:experimental:

<<<

== Entornos de desarrollo para Java

{sp}+

* Eclipse
* JDK
* NetBeans
* BlueJ
* JBuilder
* Jcreator
* JDeveloper

{sp}+

== Eclipse

{sp}+

=== Warnings

{sp}+

image::warning.png[]
Alerta cuando algo podría causar un fallo o es innecesario.

{sp}+

=== Alerts

{sp}+

image::alert.png[]
Errores de sintaxis que impiden compilar.

{sp}+

== Teoría

{sp}+

*https://www.java.com/es/[JRE]* (Java Runtime Enviorment): maquina virtual de java

*Compilar:* Pasar código fuente a código bytecodes, ejecutable desde cualquier JRE sin importar el SO. En eclipse los archivos compilados se guardan en la carpeta bin (.class).

*Versión eclipse*: 2021-06

*Versión JRE:* 1.8

*Características:* orientado a objetos, buen tratamiento de redes, seguro, multiplataforma, interpretado, de alto rendimiento y multihilo.

{sp}+

=== Tipos de aplicaciones

{sp}+

* *Aplicaciones de consola:* Aplicaciones ejecutadas en consola

* *Aplicaciones de propósito general:* Aplicaciones con GUI.