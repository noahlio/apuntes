= Serialización
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:imagesdir: ./Imagenes/
:toclevels: 3
:numbered:
:experimental:

<<<

El proceso de serialización consiste en poder pasar un objeto a bytes y después restaurarlo.

La serialización es útil para almacenar o transmitir información.

*Interfaz: Serializable Clases: ObjectOutputStream, ObjectInputStream*

*Ejemplo de escriptura y lectura con serialización:*

*import* java.util.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

Empleado[] personal=**new** Empleado[3];

//+++lista+++ +++de+++ 3 +++empleados+++

personal[0]=**new** Empleado("Marcos",20000,2001,20,3);

personal[1]=**new** Empleado("Ana",25000,2008,10,1);

personal[2]=**new** Empleado("Luis",18000,2012,9,15);

*try* \{

//+++abrir+++ +++fichero+++ +++de+++ +++escritura+++ +++en+++ bytes

ObjectOutputStream out=**new** ObjectOutputStream(*new* FileOutputStream("D:\\Basura\\personal.txt"));

out.writeObject(personal);//+++escribir+++ +++la+++ +++lista+++ +++de+++ +++empleados+++

out.close();//+++cerrar+++ el +++fichero+++

//+++abrir+++ +++fichero+++ +++de+++ +++lectura+++ +++en+++ bytes

ObjectInputStream in=**new** ObjectInputStream(*new* FileInputStream("D:\\Basura\\personal.txt"));

Empleado[] personalRecuperado=(Empleado[])in.readObject(); //leer +++los+++ +++empleados+++ +++del+++ +++fichero+++

in.close();//+++cerrar+++ +++fichero+++

*for*(Empleado e:personalRecuperado) System.*_out_*.println(e);//+++printear+++ el +++metodo+++ toString +++de+++ +++cada+++ +++empleado+++

}

*catch*(Exception e) \{//+++llamar+++ a +++la+++ +++excepcion+++ Exception

System.*_out_*.println("Fichero no encontrado");

}

}

}

*class* +++Empleado+++ *implements* Serializable\{//+++habilitar+++ +++serializacion+++ +++en+++ +++objetos+++ +++tipo+++ +++empleado+++

String nombre;

*double* sueldo;

Date fechaContrato;

*public* Empleado(String n, *double* s, *int* agno, *int* mes, *int* dia) \{

nombre = n;

sueldo = s;

fechaContrato = *new* GregorianCalendar(agno, mes - 1, dia).getTime();

}

*public* String toString() \{ //+++al+++ +++printear+++ el +++objeto+++ +++se+++ +++muestra+++ +++esta+++ +++frase+++

*return* "Nombre = " + nombre + " ,sueldo = " + sueldo + " , fecha de contrato: " + fechaContrato;

}

}

== SerialVersionUID

Código que se genera automáticamente según las clases y métodos del programa, al enviar datos serializados a otra versión del programa fallara por no tener el mismo SerialVersionUID.

*A partir del fichero generado en el ejemplo de serialización ejecuta el siguiente código:*

*import* java.util.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

Empleado[] personal=**new** Empleado[3];

//+++lista+++ +++de+++ 3 +++empleados+++

personal[0]=**new** Empleado("Marcos",20000,2001,20,3);

personal[1]=**new** Empleado("Ana",25000,2008,10,1);

personal[2]=**new** Empleado("Luis",18000,2012,9,15);

*try* \{

//+++abrir+++ +++fichero+++ +++de+++ +++escritura+++ +++en+++ bytes

//ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("D:\\+++Basura+++\\personal.txt"));

//out.writeObject(personal);//+++escribir+++ +++la+++ +++lista+++ +++de+++ +++empleados+++

//out.close();//+++cerrar+++ el +++fichero+++

//+++abrir+++ +++fichero+++ +++de+++ +++lectura+++ +++en+++ bytes

ObjectInputStream in=**new** ObjectInputStream(*new* FileInputStream("D:\\Basura\\personal.txt"));

Empleado[] personalRecuperado=(Empleado[])in.readObject(); //leer +++los+++ +++empleados+++ +++del+++ +++fichero+++

in.close();//+++cerrar+++ +++fichero+++

*for*(Empleado e:personalRecuperado) System.*_out_*.println(e);//+++printear+++ el +++metodo+++ toString +++de+++ +++cada+++ +++empleado+++

}

*catch*(Exception e) \{//+++llamar+++ a +++la+++ +++excepcion+++ Exception

System.*_out_*.println("Fichero no encontrado");

}

}

}

*class* +++Empleado+++ *implements* Serializable\{//+++habilitar+++ +++serializacion+++ +++en+++ +++objetos+++ +++tipo+++ +++empleado+++

String nombre;

*double* sueldos;

Date fechaContrato;

*public* Empleado(String n, *double* s, *int* agno, *int* mes, *int* dia) \{

nombre = n;

sueldos = s;

fechaContrato = *new* GregorianCalendar(agno, mes - 1, dia).getTime();

}

*public* String toString() \{ //+++al+++ +++printear+++ el +++objeto+++ +++se+++ +++muestra+++ +++esta+++ +++frase+++

*return* "Nombre = " + nombre + " ,sueldo = " + sueldos + " , fecha de contrato: " + fechaContrato;

}

}

*Al haber cambiado la variable sueldo a sueldos el serialVersionUID del programa no es el mismo y el contenido no se puede recuperar. Para arreglar este error hay que declarar un serialVersionUID propio, y así aunque se cambien el nombre de las variables los objetos son recuperables:*

*class* Empleado *implements* Serializable\{//+++habilitar+++ +++serializacion+++ +++en+++ +++objetos+++ +++tipo+++ +++empleado+++

*private* *static* *final* *long* *_serialVersionUID_* = 1L; //+++definir+++ serialVersionUID

*El serialVersionUID también puede ser generado por java y guardarlo, lo generara a partir del código.*

image:extracted-media/media/image31.png[image,width=274,height=88]