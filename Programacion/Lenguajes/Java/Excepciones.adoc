= Excepciones
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:imagesdir: ./Imagenes/
:toclevels: 3
:numbered:
:experimental:

<<<

image:extracted-media/media/image21.png[image,width=226,height=217]

*Jerarquía de errores:*

Cuando hay un error se genera un objeto tipo error y se lanza la excepción correspondiente al fallo.

Los fallos principales son compilación (sintaxis) y los throwable (error o exception).

Los *_error_* suelen ser errores de hardware (memoria, procesador, etc)

Los *_exception_* son los mas manejables, los IOException son excepciones controladas por el programador y abarcan errores concretos (normalmente no es culpa del programador).

Los RuntimeException son errores no comprobados (normalmente culpa del programador).

== Error comprobado

*Ejemplo de IOException (error de imagen no encontrada) :*

Image +++imagen+++;

*try* \{

imagen=ImageIO._read_(*new* File("src//graficos//bola.gif"));

}

*catch* (IOException e) \{

+++System+++.*_out_*.println("imagen no encontrada");

}

*Para utilizar el metodo read de ImageIO es OBLIGATORIO usar un try catch de IOException, ya que el método esta escrito con un throws IOException (si falla crea un objeto tipo IOException):*

image:extracted-media/media/image22.png[image,width=342,height=18]

== Error no comprobado

*Ejemplo de RuntimeError (error de elemento nulo):*

*if*(imagen==**null**) \{

System.*_out_*.println("imagen no encontrada");

}

La excepción que devuelve un elemento nulo (NULLPOINTEREXCEPTION) no se puede solucionar con un try catch con un error concreto, pero si un try catch general:

*try* \{

add(imagen);

}

*catch*(Exception e) \{

System.*_out_*.println("Imagen no encontrada");

}

== Throws

Si quieres que una función tenga un try/catch sobre un error concreto se utiliza throws para recoger ese error en concreto.

*Ejemplo de try catch dentro de la función (no muy común):*

*public* *static* *int* pedirDatos() *throws* InputMismatchException\{

*try* \{

*int* a=__input__.nextInt();

*return* a;

}

*catch*(InputMismatchException e) \{ //+++se+++ +++puede+++ +++utilizar+++ +++la+++ +++clase+++ exception (+++padre+++) +++que+++ +++es+++ +++mas+++ +++generalista+++

+++System+++.*_out_*.println("Edad erronea");

}

*return* 0;

}

*Ejemplo de try catch al utilizar la función (común):*

*public* *static* *void* main(String[] args) \{

System.*_out_*.println("edad: ");

*int* +++a+++;

*try* \{

a=__pedirDatos__();

System.*_out_*.println("OK");

}

*catch*(InputMismatchException e) \{

System.*_out_*.println("dato incorrecto");

}

}

*public* *static* *int* pedirDatos() *throws* InputMismatchException\{

*int* a=__input__.nextInt();

*return* a;

}

*Estos ejemplos utilizan el error InputMismatchException que es un tipo de error que hereda de RuntimeException, por lo tanto, aunque se declare el throws en la función sigue sin ser obligatorio el uso de try catch (excepción no comprobada)*

*Si se declara un error que herede de IOException al utilizar throws será obligatorio crear el try catch.*

*Si se declara un error pero ocurre otro que no herede del declarado saltara la excepción y el programa no se ejecutara.*

* +
*

== Throw

Throw se utiliza para lanzar una excepción personalizada, la excepción puede ser una de las ya existentes de java o una creada (Creación de excepciones), se escoge según la lógica de la excepción.

*Ejemplo de lanzamiento de excepción si la longitud de un String no es la esperada (excepción ilógica):*

String a=__+++input+++__.nextLine();

*if*(a.length()<=3) \{

EOFException e=**new** EOFException ();

*throw* e;

}

*Abreviación:*

String a=__input__.nextLine();

*if*(a.length()<3) \{

*throw* *new* EOFException ();

}

De esta forma el programa para debido a la excepción, si se utiliza Throws para controlar la excepción en una función el programa continuara.

== Finally

Finally se utiliza para ejecutar algo tanto si el try catch falla como si no, siempre se ejecuta.

*Ejemplo:*

*int* +++a+++;

*try* \{

a=__input__.nextInt();

}

*catch*(Exception e) \{

System.*_out_*.println("valor no valido");

}

*finally* \{

_input_.close();

}

Se suele utilizar para cerrar cosas si dejas de utilizarlas (escáneres, conexiones con bbdd, ficheros, etc)

== Creación de excepciones

La creación de excepciones sirve para cuando quieres lanzar una excepción que no existe en java.

Al crear los constructores es recomendable crear 2, uno sin parámetros para ejecutar solo el error y otro con un string con una explicación del error.

*Ejemplo de lanzamiento de excepción si la longitud de un String no es la esperada (comprobada):*

*import* java.util.*;

*public* *class* form \{

*static* Scanner __input__=**new** Scanner(System.*_in_*);

*public* *static* *void* main(String[] args)\{

String +++a+++;

*try* \{

a=__recogerNombre__();

} *catch* (longitudErronea e) \{

System.*_out_*.println("Error");

e.printStackTrace(); //+++imprimir+++ error +++de+++ +++excepcion+++

}

}

*public* *static* String recogerNombre() *throws* longitudErronea\{

String a=__input__.nextLine();

*if*(a.length()<3) \{

*throw* *new* longitudErronea("Nombre corto");

}

*return* a;

}

}

*class* +++longitudErronea+++ *extends* Exception\{

*public* longitudErronea() \{} //+++constuctor+++ sin +++parametros+++

*public* longitudErronea(String error) \{

*super*(error); //+++llama+++ +++al+++ constructor +++padre+++ +++con+++ el +++mensaje+++ +++de+++ error +++como+++ +++parametro+++

}

}

*Ejemplo de lanzamiento de excepción si la longitud de un String no es la esperada (no comprobada):*

*import* java.util.*;

public class form \{

*static* Scanner __input__=**new** Scanner(System.*_in_*);

*public* *static* *void* main(String[] args)\{

String +++a+++;

a=__recogerNombre__();

}

*public* *static* String recogerNombre()\{

String a=__input__.nextLine();

*if*(a.length()<3) \{

*throw* *new* longitudErronea("Nombre corto");

}

*return* a;

}

}

*class* +++longitudErronea+++ *extends* RuntimeException\{

*public* longitudErronea() \{} //+++constuctor+++ sin +++parametros+++

*public* longitudErronea(String error) \{

*super*(error); //+++llama+++ +++al+++ constructor +++padre+++ +++con+++ el +++mensaje+++ +++de+++ error +++como+++ +++parametro+++

}

}

El programa se detendrá mostrando el error pasado por parámetro en super()

== Control de varias excepciones

Para controlar diferentes tipos de excepciones se puede utilizar la clase padre de las excepciones (Exception) o puedes ir añadiendo catch con las excepciones concretas.

*Ejemplo clase Exception (generalista):*

*import* java.util.*;

*public* *class* form \{

*static* Scanner __input__=**new** Scanner(System.*_in_*);

*public* *static* *void* main(String[] args)\{

*try* \{

_division_();

}

*catch*(Exception e) \{

System.*_out_*.println("Error");

}

}

*public* *static* *void* division()\{

*int* a=__input__.nextInt();

*int* b=__input__.nextInt();

System.*_out_*.println(a/b);

}

}

*Ejemplo clases concretas (individualista):*

*import* java.util.*;

*public* *class* form \{

*static* Scanner __input__=**new** Scanner(System.*_in_*);

*public* *static* *void* main(String[] args)\{

*try* \{

_division_();

}

*catch*(ArithmeticException e) \{

System.*_out_*.println("Error division /0");

System.*_out_*.println(e.getMessage()); //+++imprimir+++ +++mensaje+++ +++de+++ +++la+++ +++clase+++

System.*_out_*.println(e.getClass());//+++imprimir+++ +++clase+++

}

*catch*(InputMismatchException e) \{

System.*_out_*.println("Error valor no valido");

}

}

*public* *static* *void* division()\{

*int* a=__input__.nextInt();

*int* b=__input__.nextInt();

System.*_out_*.println(a/b);

}

}