= Streams
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:imagesdir: ./Imagenes/
:toclevels: 3
:numbered:
:experimental:

<<<

Los streams (flujos de datos) en java se utilizan para guardar información en ficheros, al contrario de los programas hechos hasta ahora, donde los datos se guardaban en la ram.

== Stream character

Para enviar y recibir datos en binario se usan las clases inputStream y outputStream.

Para leer y escribir se utilizan las clases Reader y Writer.

*Ejemplo de impresión de contenido de un fichero:*

*import* java.util.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

*try* \{

//+++abrir+++ +++fichero+++

FileReader in=**new** FileReader("D:\\Codigos\\Apuntes\\Java\\Curso Java\\Ficheros\\txts\\hola.txt");

*int* a=in.read(); //leer +++la+++ +++letra+++ 0 +++del+++ +++fichero+++ (+++en+++ +++int+++)

*while*(a!=-1) \{

System.*_out_*.print((*char*)a);//+++imprimir+++ +++la+++ +++letra+++ +++pasada+++ a +++caracter+++

a=in.read();//leer +++la+++ +++siguiente+++ +++letra+++

}

in.close(); //+++cerrar+++ +++fichero+++

}

*catch*(IOException i) \{

System.*_out_*.println(i.getMessage());//+++recoger+++ +++excepcion+++ IOException

}

}

}

Se recoge la excepción IOException porque es la padre de eclipse-javadoc:%E2%98%82=Ficheros/C:%5C/Users%5C/noahb%5C/.p2%5C/pool%5C/plugins%5C/org.eclipse.justj.openjdk.hotspot.jre.full.win32.x86_64_16.0.2.v20210721-1149%5C/jre%5C/lib%5C/jrt-fs.jar%60java.base=/module=/true=/=/javadoc_location=/https:%5C/%5C/docs.oracle.com%5C/en%5C/java%5C/javase%5C/16%5C/docs%5C/api%5C/=/%3Cjava.io(FileReader.class%E2%98%83FileReader~FileReader~Ljava.lang.String;%E2%98%82java.io.FileNotFoundException[FileNotFoundException] (excepción comprobada de FileReader) y es la excepción del método .read().

Se podrían hacer 2 catch, pero se simplifica utilizando el padre.

*Ejemplo de escritura en un fichero:*

*import* java.util.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

String a="Adios, ta luego";

*try* \{

//abrir fichero *SI NO EXISTE LO CREA, SI EXISTE LO SOBREESCRIBE*

FileWriter out=**new** FileWriter("D:\\Codigos\\Apuntes\\Java\\Curso Java\\Ficheros\\txts\\adios.txt");

*for*(*int* x=0;x<a.length();x++) \{//recorrer string

out.write(a.charAt(x));//escribir letra a letra

}

out.close();//cerrar fichero *IMPORTANTE PARA GUARDAR CAMBIOS*

}

*catch*(IOException i) \{

System.out.println(i.getMessage());//recoger excepcion IOException

}

}

}

*En caso de no querer sobrescribir un fichero existente, y quieres solo añadir contenido se añade un true después de escribir la ruta:*

FileWriter out=**new** FileWriter("D:\\Codigos\\Apuntes\\Java\\Curso Java\\Ficheros\\txts\\adios.txt",*true*);

== Streams byte

Se utilizan para enviar y recibir información de manera más eficaz que Stream character.

*Clases utilizadas: FileInputStream y FileOutputStream.*

*Ejemplo de recogida de bytes de una imagen e inserción en otra (copiar y pegar):*

*import* java.util.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

_writeImage_(_readImage_());

}

*public* *static* ArrayList<Integer> readImage() \{

ArrayList<Integer> bytes=**new** ArrayList<Integer>();

*try* \{

//+++abrir+++ +++foto+++

FileInputStream foto=**new** FileInputStream("D:\\Basura\\java.png");

*int* a=foto.read();//leer primer byte

*while*(a!=-1) \{//+++bucle+++ +++hasta+++ +++que+++ no +++haya+++ +++mas+++ bytes

bytes.add(a);//+++añadir+++ byte a +++un+++ arrayList

a=foto.read();//leer +++siguiente+++ byte

}

foto.close();//+++cerrar+++ +++foto+++

}

*catch*(IOException i) \{

System.*_out_*.println(i.getMessage());//+++recoger+++ +++excepcion+++ IOException

}

System.*_out_*.println("Cantidad de bytes: "+bytes.size());//+++printear+++ +++cantidad+++ +++de+++ bytes

*return* bytes;//+++devolver+++ +++la+++ arrayList +++con+++ +++los+++ bytes

}

*public* *static* *void* writeImage(ArrayList<Integer> bytes) \{

*try* \{

//+++crear+++ +++nueva+++ +++imagen+++

FileOutputStream newFoto=**new** FileOutputStream("D:\\\\Basura\\\\javaCopia.png");

*for*(*int* x=0;x<bytes.size();x++) \{

newFoto.write(bytes.get(x));//+++insertar+++ +++los+++ bytes +++de+++ +++la+++ +++foto+++ original

}

newFoto.close();//+++cerrar+++ +++foto+++

}

*catch*(IOException e)\{

System.*_out_*.println(e.getMessage());//+++recoger+++ +++excepcion+++ IOException

}

}

}

== Buffers

Un buffer es una memoria intermedia entre el fichero y la aplicación, gracias a esto no es necesario estar consultando el fichero carácter a carácter. Se utiliza para lectura y escritura.

*Ejemplo de la clase BufferedReader:*

*import* +++java.util+++.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

*try* \{

//+++abrir+++ +++fichero+++

FileReader in=**new** FileReader("D:\\Codigos\\Apuntes\\Java\\Curso Java\\Ficheros\\txts\\adios.txt");

BufferedReader bf=**new** BufferedReader(in); //+++almacenar+++ +++fichero+++ +++en+++ +++un+++ buffer

String linea=bf.readLine();//leer +++linea+++

*while*(linea!=**null**) \{//+++comprobar+++ +++si+++ +++la+++ +++linea+++ +++es+++ +++nula+++

System.*_out_*.println(linea);//+++imprimir+++ +++linea+++

linea=bf.readLine();//+++recoger+++ +++la+++ +++siguiente+++ +++linea+++

}

in.close();//+++cerrar+++ +++fichero+++

bf.close();//+++cerrar+++ buffer

}

*catch*(IOException i) \{

System.*_out_*.println(i.getMessage());//+++recoger+++ +++excepcion+++ IOException

}

}

}

*Ejemplo de la clase FileWriter:*

*import* java.util.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

*try* \{

//+++abrir+++ +++fichero+++

FileWriter out=**new** FileWriter("D:\\Codigos\\Apuntes\\Java\\Curso Java\\Ficheros\\txts\\adios.txt");

BufferedWriter bf=**new** BufferedWriter(out); //+++almacenar+++ +++fichero+++ +++en+++ +++un+++ buffer

*for*(*int* x=0;x<10;x++) \{

bf.write("Linea: "+(x+1)+"\n");//+++escribir+++ +++lineas+++

}

out.close();//+++cerrar+++ fichero

bf.close();//+++cerrar+++ buffer

}

*catch*(IOException i) \{

System.*_out_*.println(i.getMessage());//+++recoger+++ +++excepcion+++ IOException

}

}

}