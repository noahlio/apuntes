= Archivos y directorios
Author Name Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-06-28
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:imagesdir: ./Imagenes/
:toclevels: 3
:numbered:
:experimental:

<<<

== Manipulación de archivos y directorios

Para manipular ficheros hay que utilizar la clase File, de parámetro de constructor hay que llamar por lo menos a una ruta de fichero.

*Ejemplo visualización de ficheros y directorios:*

*import* +++java.util+++.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

//+++archivo+++ +++con+++ +++ruta+++ +++relativa+++, el +++fichero+++ +++se+++ +++guardara+++ +++en+++ +++la+++ +++carpeta+++ +++de+++ +++proyecto+++

File archivo=**new** File("ejemplo");

System.*_out_*.println(archivo.getAbsolutePath()); //+++imprimir+++ +++ruta+++ +++absoluta+++

//+++comprobar+++ +++si+++ +++existe+++ el +++fichero+++/+++directorio+++

System.*_out_*.println(archivo.exists());

System.*_out_*.println(*new* File("src").exists());

String s=File.*_separator_*; //+++separador+++ *VALIDO PARA DIFERENTES S.O.*

//+++llamar+++ a +++arbol+++ +++con+++ +++un+++ +++directorio+++

_arbol_("D:"+s+"Codigos"+s+"Apuntes"+s+"C++"+File.**_separator_**+"Arduino"+s+"Codigos");

}

*public* *static* *void* arbol(String path) \{

//+++objeto+++ file

File ruta=**new** File(path);

//+++printear+++ +++todos+++ +++los+++ +++archivos+++ +++que+++ hay +++en+++ +++la+++ +++carpeta+++ +++ruta+++

*for*(String s:ruta.list()) \{

System.*_out_*.println(s+" ");

File nf=**new** File(ruta.getAbsoluteFile(),s);

*if*(nf.isDirectory()) \{ //+++comprobar+++ +++si+++ +++es+++ +++un+++ +++directorio+++

_arbol_(nf.getAbsolutePath(),"--");//+++llamar+++ a +++arbol+++ +++con+++ +++ruta+++ +++del+++ +++directorio+++

}

}

}

*public* *static* *void* arbol(String path, String guiones) \{

//+++objeto+++ file

File ruta=**new** File(path);

//+++printear+++ +++todos+++ +++los+++ +++archivos+++ +++que+++ hay +++en+++ +++la+++ +++carpeta+++ +++ruta+++

*for*(String s:ruta.list()) \{

System.*_out_*.println(guiones+s+" ");

File nf=**new** File(ruta.getAbsoluteFile(),s);//+++objeto+++ file +++iniciado+++ +++con+++ path y +++directorio+++ +++hijo+++

*if*(nf.isDirectory()) \{//+++comprobar+++ +++si+++ +++es+++ +++un+++ +++directorio+++

_arbol_(nf.getAbsolutePath(),guiones+"--");//+++llamar+++ a +++arbol+++ +++con+++ +++ruta+++ +++del+++ +++directorio+++

}

}

}

}

*Ejemplo de creación y eliminación de ficheros y directorios:*

*import* +++java.util+++.*;

*import* java.io.*;

*public* *class* accesoFichero \{

*public* *static* *void* main(String[] args)\{

String s=File.*_separator_*; //+++separador+++ *VALIDO PARA DIFERENTES S.O.*

File f=**new** File(("D:"+s+"Basura"+s+"MasBasura")); //+++crear+++ +++objeto+++ file

f.mkdir(); //+++crear+++ f +++como+++ +++directorio+++ *EN CASO DE EXISTIR NO LO REESCRIBE*

File a=**new** File(("D:"+s+"Basura"+s+"basura.txt")); //+++crear+++ +++objeto+++ file

File a2=**new** File(("D:"+s+"Basura"+s+"basura2.txt")); //+++crear+++ +++objeto+++ file

*try* \{

a.createNewFile();//+++crear+++ a +++como+++ +++archivo+++ *EN CASO DE EXISTIR NO LO REESCRIBE*

a2.createNewFile();//+++crear+++ a +++como+++ +++archivo+++ *EN CASO DE EXISTIR NO LO REESCRIBE*

} *catch* (IOException e) \{

e.printStackTrace();

}

a.delete();//+++eliminar+++ +++fichero+++

}

}