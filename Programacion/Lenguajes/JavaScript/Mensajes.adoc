= Mensajes
Author Name <noahbobis0512@gmail.com>; Noah Ramos Bobis <noahbobis0512@gmail.com>
v1.0, 2022-03-31
:encoding: utf-8
:lang: ca
:toc: 
:toc-title: Índice
:toclevels: 3
:numbered:
:experimental:
:imagesdir: Imagenes/Mensajes/
:check: &#10004;
:checked: pass:normal[{startsb}&#10004;{endsb}]
:cross: &#10060;
:crossed: pass:normal[{startsb}&#10060;{endsb}]
:infinite: &#9854;

<<<

{sp}+

Los mensajes generalmente dan información al desarrollador però hay mensajes emergentes para el cliente.

{sp}+

== Niveles

Los niveles de mensaje son la prioridad por la que se informan por consola, se pueden filtrar para encontrar antes el contenido que buscas.

{sp}+

[#niveles]
:table-caption!:
.Niveles
|===
|Tipo           |Definición

|Verbose        |Mensajes informativos para el desarrollador
|Info           |Mensajes de información
|Warnings       |Mensajes de alerta
|Errors         |Mensajes que informan de un error
|Default        |Activa los tipos Info, Warnings y Errors
|===

{sp}+



== Mensajes de consola

Los mensajes de consola sirven para informar al desarrollador de errores, información o ver variables.

:error: image:error.png[error, 459, 19]
:enlaceError: link:https://developer.mozilla.org/es/docs/Web/API/Console/error[Error]
:errorCondicional: image:error.png[error condicional, 459, 19]
:enlaceErrorCondicional: link:https://developer.mozilla.org/es/docs/Web/API/console/assert[Error condicional (assert)]
:count: image:count.png[contador de cosas, 470, 85]
:enlaceCount: link:https://developer.mozilla.org/es/docs/Web/API/Console/count[Contador (count)]
:debug: image:debug.png[Mensajes para el desarrollador]
:enlaceDebug: link:https://developer.mozilla.org/en-US/docs/Web/API/console/debug[Depuracion (debug)]
:nino: image:nino.png[mensaje descriptivo de un objeto]
:enlaceDir: link:https://developer.mozilla.org/es/docs/Web/API/Console/dir[Descripcion del objeto (dir)]
:ninoxml: image:ninoxml.png[mensaje descriptivo de un objeto en xml]
:enlaceDirxml: link:https://developer.mozilla.org/es/docs/Web/API/Console/dirxml[Descripción en xml (dirxml)]
:group: image:group.png[agrupar mensajes consola]
:enlaceGroup: link:https://developer.mozilla.org/es/docs/Web/API/Console/group[Agrupacion (group)]
:info: image:info.png[mensajes informativos]
:enlaceInfo: link:https://developer.mozilla.org/es/docs/Web/API/Console/info[Informativos (info)]
:log: image:log.png[mensajes de registro]
:enlaceLog: link:https://developer.mozilla.org/es/docs/Web/API/Console/log[Registro (log)]
:profile: image:profile.png[mensajes de creación de perfil]
:enlaceProfile: link:https://developer.mozilla.org/es/docs/Web/API/Console/profile[Perfil (profile)]
:table: image:table.png[lista de elementos en formato tabla]
:enlaceTable: link:https://developer.mozilla.org/es/docs/Web/API/Console/table[Table]
:time: image:time.png[temporizador]
:enlaceTime: link:https://developer.mozilla.org/es/docs/Web/API/Console/time[Time]
:trace: image:trace.png[camino hacia el mensaje lanzado]
:enlaceTrace: link:https://developer.mozilla.org/es/docs/Web/API/Console/trace[Trazar camino (trace)]
:warn: image:warn.png[Mensaje de advertencia]
:enlaceWarn: link:https://developer.mozilla.org/es/docs/Web/API/Console/warn[Mensaje de advertencia (warn)]

:linkErrors: <<niveles, Errors>>
:linkInfo: <<niveles, Info>>
:linkVerbose: <<niveles, Verbose>>
:linkWarnings: <<niveles, Warnings>>

{sp}+

[#mensajesConsola, cols=4*]
:table-caption!:
.Mensajes de consola
|===
|Nombre                            |Entrada                                                      |Salida                |Tipo

|{enlaceError}                     a|include::Codigos/Mensajes/codeError.adoc[]                  |{error}               |*{linkErrors}*
|{enlaceErrorCondicional}          a|include::Codigos/Mensajes/codeErrorCondicional.adoc[]       |{errorCondicional}    |*{linkErrors}*
|{enlaceCount}                     a|include::Codigos/Mensajes/codeCount.adoc[]                  |{count}               |*{linkInfo}*
|{enlaceDebug}                     a|include::Codigos/Mensajes/codeDebug.adoc[]                  |{debug}               |*{linkVerbose}*
|{enlaceDir}                       a|include::Codigos/Mensajes/codeDir.adoc[]                    |{nino}                |*{linkInfo}*
|{enlaceDirxml}                    a|include::Codigos/Mensajes/codeDirxml.adoc[]                 |{ninoxml}             |*{linkInfo}*
|{enlaceGroup}                     a|include::Codigos/Mensajes/codeGroup.adoc[]                  |{group}               |*{linkInfo}*
|{enlaceInfo}                      a|include::Codigos/Mensajes/codeInfo.adoc[]                   |{info}                |*{linkInfo}*
|{enlaceLog}                       a|include::Codigos/Mensajes/codeLog.adoc[]                    |{log}                 |*{linkInfo}*
|{enlaceProfile}                   a|include::Codigos/Mensajes/codeProfile.adoc[]                |{profile}             |*{linkInfo}*
|{enlaceTable}                     a|include::Codigos/Mensajes/codeTable.adoc[]                  |{table}               |*{linkInfo}*
|{enlaceTime}                      a|include::Codigos/Mensajes/codeTime.adoc[]                   |{time}                |*{linkInfo}*
|{enlaceTrace}                     a|include::Codigos/Mensajes/codeTrace.adoc[]                  |{trace}               |*{linkInfo}*
|{enlaceWarn}                      a|include::Codigos/Mensajes/codeWarn.adoc[]                   |{warn}                |*{linkWarnings}*

|===

{sp}+

== Mensajes de ventana

Los cuadros de mensaje se utilizan para lanzar ventanas con las cuales puede interactuar el usuario.

{sp}+

:alert: image:alerta.png[alert]
:enlaceAlert: https://developer.mozilla.org/es/docs/Web/API/Window/alert[Alert]
:confirm: image:confirm.png[confirm]
:enlaceConfirm: https://developer.mozilla.org/es/docs/Web/API/Window/confirm[Confirm]
:prompt: image:prompt.png[prompt]
:enlacePrompt: https://developer.mozilla.org/es/docs/Web/API/Window/prompt[Prompt]

[#mensajesVentana, cols=3*]
:table-caption!:
.Mensajes de ventana
|===
|Nombre                            |Entrada                                                      |Salida 

|{enlaceAlert}                     a|include::Codigos/Mensajes/codeAlert.adoc[]                  |{alert}
|{enlaceConfirm}                   a|include::Codigos/Mensajes/codeConfirm.adoc[]                |{confirm}
|{enlacePrompt}                    a|include::Codigos/Mensajes/codePrompt.adoc[]                 |{prompt}

|===

{sp}+
