= Vagrant
Author Name <noahbobis0512@gmail.com>; Noah Bobis 
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

https://www.vagrantup.com/downloads[Vagrant] es una herramienta para la creación y configuración de entornos de desarrollo virtualizados.

Es compatible con VirtualBox, VMware, Amazon EC2, LXC, DigitalOcean...

{sp}+

https://www.vagrantup.com/downloads[Enlace de descarga]

{sp}+

link:./UbuntuVM[*EJEMPLO DE PROYECTO VAGRANT*]

{sp}+

== Boxes

{sp}+

Vagrant utiliza imagenes de SO ya creadas.

Se descargan en *https://app.vagrantup.com/boxes/search[Vagrant cloud]*, se pueden descargar <<Instalar boxes, manualmente>> o utilizando su nombre en el <<Archivo de configuracion>>.

{sp}+

=== Instalar boxes

{sp}+

Con el comando `vagrant box add nombre-box` puedes instalar la box nombrada. 

Ejemplo: `vagrant box add ubuntu/trusty64`.

{sp}+

=== Ver boxes instaladas

{sp}+

Con el comando `vagrant box list` se pueden ver las boxes instaladas en el ordenador.

{sp}+

=== Otras paginas con boxes

{sp}+

En https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/[VM Microsoft] se pueden descargar boxes asignando Vagrant como *VM platform*.

En http://www.vagrantbox.es/[vagrant.es] hay muchas mas boxes desarrolladas para entornos de virtualizacion especificos.

{sp}+

== Comandos basicos

{sp}+

=== Iniciar VagrantFile

{sp}+

Con el comando `vagrant init` se crea el fichero <<VagrantFile>> en el que se escribe toda la configuracion de la MV.

{sp}+

=== Crear e iniciar MV

{sp}+

Con el comando `vagrant up` (estando en la carpeta donde esta el <<VagrantFile>>) se inicia la maquina virtual y en caso de no existir se creara.

{sp}+

=== Conectarse a la MV

{sp}+

Con el comando `vagrant ssh` (estando en la carpeta donde esta el <<VagrantFile>>) se hace la conexion ssh con la maquina. 

Cerrar conexion con el comando `exit`.

{sp}+

=== Reiniciar MV

{sp}+

Con el comando `vagrant reload` (estando en la carpeta donde esta el <<VagrantFile>>) reinicia la MV.

{sp}+

=== Apagar MV

{sp}+

Con el comando `vagrant halt` (estando en la carpeta donde esta el <<VagrantFile>>) apaga la MV.

{sp}+

=== Borrar MV

{sp}+

Con el comando `vagrant destroy` (estando en la carpeta donde esta el <<VagrantFile>>) borra la MV.

{sp}+

== VagrantFile

{sp}+

Ejemplo de archivo con los parametros basicos a configurar en la MV.

[source,ruby]
----
Vagrant.configure("2") do |config| #comienzo de la configuracion
  config.vm.box = "ubuntu/trusty64" #nombre de la box
  config.vm.network "private_network", type: "dhcp" #tipo de red

  # Redirección de puertos
  config.vm.network "forwarded_port", guest: 8080, host: 8080 # web server
  config.vm.network "forwarded_port", guest: 5432, host: 5432 # Postgres

  config.vm.provider "virtualbox" do |v| #configuracion del proveidor
    v.memory = 4096 #RAM en mb
    v.cpus = 2 #Nucleos de cpu
  end

  config.ssh.forward_agent = true #activar conexiones ssh
  config.vm.synced_folder "data", "/data" #Carpeta compartida
  config.vm.provision "shell", path: "ejemplo.sh" #script que se ejecuta al crear la MV

  # Cargar clave ssh
  ssh_key_path = "~/.ssh/"
  config.vm.provision "shell", inline: "mkdir -p /home/vagrant/.ssh"
  config.vm.provision "file", source: "#{ ssh_key_path + 'id_rsa' }", destination: "/home/vagrant/.ssh/id_rsa"
  config.vm.provision "file", source: "#{ ssh_key_path + 'id_rsa.pub' }", destination: "/home/vagrant/.ssh/id_rsa.pub"

end #Fin de la configuracion
----

{sp}+

=== Scripts de aprovision

{sp}+

En el fichero de configuración se puede asignar un script que se ejecutara al crear la MV. Si se quiere volver a ejecutar se utilizara el comando `vagrant provision`

Se pueden añadir haciendo referencia a un *fichero* o *inline*:

[source,ruby]
----
config.vm.provision "shell", path: "ejemplo.sh"
----

[source,ruby]
----
config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get install -y apache2
SHELL
----

*AVISO: los comandos no pueden ser interactivos, se tienen que ejecutar automaticamente.*

{sp}+

=== Archivos ansible

{sp}+

Para añadir un archivo link:++/Programacion/Herramientas/Ansible I/ansible.adoc++[ansible] hay que hacer referencia al .yml con las reglas ansible.

[source,ruby]
----
config.vm.provision "ansible" do |ansible|
    ansible.playbook = "reglas.yml" #Define archivo con reglas
    ansible.limit = "all" #Quita el tiempo limite de conexion
end
----

Si se quiere ejecutar siempre las reglas ansible se utiliza el parametro run.

[source,ruby]
----
config.vm.provision "ansible", run: "always" do |ansible|
    ansible.playbook = "reglas.yml" #Define archivo con reglas
    ansible.limit = "all" #Quita el tiempo limite de conexion
end
----

{sp}+