= Git
Author Name <noahbobis0512@gmail.com>; Noah Bobis <noahbobis0512@gmail.com>
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

== Instalacion

{sp}+

- *Windows*

----
https://gitforwindows.org/
----

{sp}+

- *Linux*

----
apt-get install git
----

{sp}+

- *Mac*

----
git
----

{sp}+

== Terminos especificos

{sp}+

=== Stage 

{sp}+

Zona en la que especifican las cosas que pasaran al proyecto git al hacer commit.

{sp}+

=== HEAD

{sp}+

Hace referencia al commit actual, si nos queremos referir a un numero x de commits anteriores se utiliza `HEAD~x`.

{sp}+

== Comandos habituales

{sp}+

Los comandos se ejecutan desde terminal en linux o mac. En windows es necesario abrir la aplicacion `git bash`.

{sp}+

=== Init

{sp}+

Con el comando `git init` se crea una carpeta oculta llamada .git, donde estara la informacion del proyecto.

{sp}+

=== Commit

{sp}+

Con el comando `git commit` todo lo que se encuentre en <<Stage,stage>> se enviara al directorio de trabajo.

Al ejecutar `git commit` se abre un editor de texto que te permite poner un mensaje para describir los cambios realizados.

Añadiendo el parametro amend `git commit --amend` todo lo que se encuentre en <<Stage,stage>> se enviara al *ultimo comit realizado*, sin crear otro nuevo y te permite cambiar el mensaje del commit anterior.

{sp}+

=== Status

{sp}+

Con el comando `git status` se ve información del proyecto (rama, commit, documentos sin subir).

{sp}+

=== Log

{sp}+

Con el comando `git log` se ve el historial de commits que se han hecho.

Si se utiliza `git log --oneline` se mostrara el codigo del <<Commit,commit>> y su mensaje.

Con el parametro *graph* se muestra un grafo que relacciona los commits con sus antecesores y descendencia, `git log --graph`.

Con el parametro *decorate* se marca el <<Commit,commit>> en el que estas actualmente, `git log --decorate`.

Con el parametro *all* se listan todas las <<Ramas>>, `git log --all`.

{sp}+

=== Diff

{sp}+

Con el comando `git diff` se ven los cambios que hemos hecho respecto al los archivos que hay en el <<Stage,stage>>.

Para ver diferencias entre commits se señalan <<Log, los codigos de comit>>, `git diff 9772ecd 39468c3`.

{sp}+

=== Add

{sp}+

Con el comando `git add nombre` se añaden documentos/carpetas a la zona de <<Stage,stage>>, zona desde donde se subiran los datos al hacer commit.

Para añadir todo al <<Stage,stage>> se señala al directorio principal `git add .`.

{sp}+

=== Reset

{sp}+

El comando `git add` es reversible con `git reset` o especificando los documentos/carpetas que no se quieren subir `git reset nombre`.

Si se añade un <<Log, codigo de comit>> o se hace referencia con <<HEAD>> se revertiran todos los commits hasta el señalado, `git reset 9772ecd` = `git reset HEAD~1`.

Si se utiliza el parametro --hard, se revertira el commit y los archivos locales al comit señalado, `git reset --hard 9772ecd`. 

El parametro --soft solo revertira el commit y dejara los cambios en <<Stage,stage>>, `git reset --soft 9772ecd`.

{sp}+

=== Revert

{sp}+

El comando `git revert` sirve para revertir un commit especificado, por ejemplo si quiero revertir el commit actual se utiliza `git revert HEAD`. Eso creara un commit que sera la reversion del commit actual.

Si se quieren revertir varios commits sin crear un commit, dejando los cambios en <<Stage,stage>>, por cada ver que se revierte se utiliza el parametro --no-commit, `git revert --no-commit HEAD`, `git revert --no-commit HEAD~1`.

Finalmente para hacer el <<Commit, commit>> de todos los reverts se utiliza el parametro --continue, `git revert --continue`.

{sp}+

=== Tag

{sp}+

El comando `git tag nombre-tag idcomit` sirve para asignar una etiqueta a cada commit, se suele utilizar para apuntar la versión. Si no se especifica el commit se ejecuta en el actual.

Un commit al tener tag se le puede hacer referencia con ese tag, por ejemplo: `git checkout nombre-tag`.

Para listar todos los tags se ejecuta el comando `git tag`. Se pueden filtrar con el parametro -l, `git tag -l "v0.1.*"`, en este ejemplo lista todas las versiones 0.1.

El parametro -d elimina el tag, `git tag -d nombre-tag`.

El parametro -a hace que puedas añadir toda la información que quieras referente al tag, un mensaje al igual que en <<Commit,commit>>, `git tag -a nombre-tag`.

{sp}+

=== Show

{sp}+

Con el comando `git show nombre` se puede ver toda la información referente a un <<Tag, tag>>, <<Commit, commit>>, <<Rama, rama>> o <<Stash, stash>>.

{sp}+

== Ramas

{sp}+

Las ramas se utilizan para poder modificar una parte del proyecto de forma paralela a la principal, una vez acabada la modificacion la rama se une al proyecto.

{sp}+

=== Branch

{sp}+

Con el comando `git branch` se ven las ramas que tiene el proyecto.

Permite crear una nueva rama indicando su nombre, `git branch nombre`.

Puedes cambiar el nombre de una rama con el parametro -m, `git branch -m nombre-original nuevo-nombre`, 

Tambien borrar una rama con el parametro -d (-D fuerza el borrado), `git branch -d nombre`.

{sp}+

=== Checkout

{sp}+

Con el comando `git checkout nombre` puedes cambiarte a la rama indicada.

Si se indica el parametro -b estas creando y cambiando de rama a la vez, `git checkout -b nombre`.

Con el parametro -- nombre, se revierte los cambios que se han hecho en un archivo desde el ultimo guardado <<Stage,stage>> o `commit`, `git checkout -- nombre`.

Si se acaba de subir a <<Stage,stage>> habra que hacer un `git reset nombre` y despues utilizar el `git checkout -- nombre`.

{sp}+

==== Switch

{sp}+

El comando switch tiene utilidades muy parecidas al <<Checkout, checkout>> con algun parametro mas facil de usar.

La creación de ramas es igual `git checkout nombre = git switch nombre` y `git checkout -b nombre = git switch -c nombre`.

Para crear una rama sin relación se utiliza el parametro orphan, `git switch --orphan nombre`.

Tambien se puede cambiar de rama descartando todo lo del <<Stash, stash>> con el parametro discard-changes, `git switch --discard-changes nombre`.

{sp}+

=== Merge

{sp}+

Con el comando `git merge nombre` puedes fusionar la rama en la que te encuentras con la indicada, quedando en la que te encuentras como una mezcla de ambas.

Si al fusionar dos ramas encuentra un conflicto, hay cambios en las mismas lineas, git dejara ambas versiones en el mismo fichero señalizadas con `<<<<<<< >>>>>>>`.

En caso de querer abortar el merge que ha ocasionado el conflicto se utiliza `git merge --abort`.

{sp}+

=== Stash

{sp}+

Si tienes que cambiar de rama y tienes cosas en <<Stage,stage>> que no puedes commitear ni borrar se dejan en stash (commitea sin hacer visibles los cambios).

Con el comando `git stash` guardas lo que hay en <<Stage,stage>> a stash.

Con el comando `git stash list` listas los stash que hay en el commit actual.

Con el comando `git stash apply` devuelves los datos de stash a <<Stage,stage>>.

Con el comando `git stash drop` eliminas el stash.

Tambien puedes hacer que el stash se guarde como una rama nueva con el comando `git stash branch nombre-rama`.

{sp}+

== Config

{sp}+

=== Alias

{sp}+

Cuando los comandos son muy largos, ejemplo: `git log --oneline --decorate --all --graph`, se crean alias quedando el anterior como lodag

{sp}+

==== Crear alias

{sp}+

Con el comando `git config --global alias.nombre-alias-creado 'comando'` se crea un alias que se puede ejecutar con `git nombre-alias-creado`.

{sp}+

==== Eliminar alias

{sp}+

Para eliminar un alias se utiliza el comando `git config --global --unset alias.nombre-alias`.

{sp}+

==== Ver alias

{sp}+

Para ver los alias que has creado en git se utiliza el comando `git config --global --get-regexp alias`.

{sp}+

=== Rama defecto

{sp}+

La rama que se crea por defecto suele nombrarse master, pero en algunas aplicaciones como eclipse se denomina main u otros.

Para configurar el nombre que quieres darle a la rama principal por defecto se utiliza el comando `git config --global init.defaultBranch nombre-rama`.

{sp}+

=== Guardar credenciales

{sp}+

Con el comando `git config --global credential.helper store` se configura que la proxima vez que utilicemos las credenciales git estas se almacenen en el documento ~/.git-credentials, de manera que no las vuelva a pedir.

{sp}+

== Repositorios remotos

{sp}+

GIT esta formado en forma de nodos, no hay un servidor central, pero normalmente se utiliza un nodo central (github, gitlab...), como nodo al que todo el mundo se comunica para no tener muchas conexiones para compartir codigo.

Las ramas remotas son aplicables a todos los comandos de git, mencionandolas como `nombre-remoto/nombre-rama` por ejemplo `git log origin/main`.

{sp}+

=== Remote

{sp}+

Con el comando `git remote add origin url-proyecto-git` puedes agregar el repositorio externo.

Con el comando `git remote` se listan los repositorios que tienes.

Con el parametro -v se muestra mas información, `git remote -v`.

Con el comando `git remote remove nombre-repositorio` puedes borrar un repositorio.

{sp}+

=== Push

{sp}+

Con el comando `git push nombre-repositorio nombre-rama` puedes subir tu rama actual a la rama señalada.

{sp}+

=== Pull

{sp}+

Con el comando `git pull nombre-remoto nombre-rama` actualizas el contenido de tu rama con el contenido que hay en el repositorio remoto.

El parametro rebase sirve para cuando ambas ramas han hecho modificaciones, rebase tratara de aplicar todos los commits de manera cronologica, `git pull --rebase nombre-repositorio nombre-rama`. 

ALERTA *rebase modifica el historial* haciendo que los commits de ambas ramas se fusionen y salgan uno despues de otro.

Si da el siguiente error `rehusando fusionar historias no relacionadas` es porque los repositorios no tienen nada en comun. Lo mas normal es que el repositorio remoto sea nuevo y tenga un documento `README.ME` puesto por defecto. Hay que borrar el documento y volver a probar o si se quiere guardar el documento ejecutar el comando `git pull origin main --allow-unrelated-histories`.

==== Fetch merge

{sp}+

El comando `git pull nombre-remoto nombre-rama` realmente lo que hace es ejecutar el comando `git fetch nombre-remoto nombre-rama` para ver si hay actualizaciones en la rama que no tengas en la tuya y despues ejecuta `git merge nombre-remoto/nombre-rama` para que quede la fusion de ambas.

{sp}+

=== Clone

{sp}+

Con el comando `git clone link-repositorio` clonas todo el repositorio a tu carpeta actual y el repositorio remoto se asigna como origin.

{sp}+

=== Rebase

{sp}+

El comando `git rebase -i` hace que puedas interactuar con el historial de commits, editando mensajes, cambiando el orden de los commits, etc.

Para hacer cambios hay que señalar que commits quieres modificar, ejemplo del actual al actual-4: `git rebase -i HEAD~4`. Se abrira un documento en el que cambiando la primera palabra `pick` por las descritas en el documento se pueden hacer modificaciones en el historial. *Para fusionar commits se utiliza squash*.

Este comando solo se tiene que ejecutar en proyectos locales o individuales, ya que al cambiar el historial cambian los hashes y si hay otra persona trabajando con el mismo repositorio se acabara borrando informacion.

{sp}+