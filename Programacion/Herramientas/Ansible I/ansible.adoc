= Ansible
Author Name <noahbobis0512@gmail.com>; Noah Bobis 
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

Ansible es una plataforma para configurar y administrar ordenadores. En lugar de programar un script que añada los programas, actualizaciones, etc, en un fichero yml se declaran las cosas que el servidor debe tener y ansible se encarga de instalarlo.

{sp}+

== Instalacion

{sp}+

Ansible es compatible en sistemas UNIX, para instalarlo en Windows 10 hay que tener un https://www.ionos.es/digitalguide/servidores/know-how/windows-subsystem-for-linux-wsl/[subsistema linux].

https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html[Pasos de instalacion Ansible]

*AVISO*: Si no existe la carpeta `/etc/ansible/` hay que añadir el repositorio de ansible, actualizar e instalar ansible y ansible-core

[source,shell]
----
sudo apt-add-repository ppa:ansible/ansible
sudo apt update
sudo apt install ansible
sudo apt-get install ansible-core
----

{sp}+

== Inventory

{sp}+

Inventory es una lista de las maquinas a las que aprovisiona ansible.

El documento en el que se declara la lista es `etc/ansible/hosts`.

Los grupos se definen entre '[]' y debajo del nombre del grupo el nombre/ip de las maquinas:

[source,shell]
----
vagrant1 ansible_host=192.168.56.100

[vagrantVM]
vagrant1

[vagrantVM:vars]
ansible_ssh_host=127.0.0.1
ansible_ssh_port=2200
ansible_ssh_user=vagrant
ansible_ssh_pass=vagrant

[webservers]
alpha.example.org
beta.example.org
192.168.1.100
192.168.1.[110:120] #ips de la 110 a la 120
----

{sp}+

=== Archivos inventory

{sp}+

Si en una consulta puntual con otro inventory se puede utilizar señalando el fichero con el parametro -i


[source,shell]
----
ansible vagrant1 -m ping -i inventory
----

{sp}+

== Comandos ad-hoc

{sp}+

Estos comandos se ejecutan en terminal para hacer consultas rapidas a hosts que existan en <<Inventory>> o <<Archivos inventory>>

Parametros

- -m: https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html[Modulo] que se utiliza.
- -a: Opciones del comando.
- -i: Archivo de inventory
- -b: Ejecutar como sudo

[source,shell]
----
ansible vagrant1 -m ping -i inventory
ansible vagrant1 -m ping
ansible vagrant1 -a 'echo hola' -i inventory
ansible vagrant1 -m apt -a 'name=vim state=present' -b
----

{sp}+

=== Errores

{sp}+

En caso de error añadir todos los datos de la maquina en el <<Inventory>> y eliminar su clave ssh de ~/.ssh/known_hosts.

----
ansible_user=vagrant
ansible_ssh_host=127.0.0.1
ansible_ssh_port=2200
ansible_ssh_user=vagrant
ansible_ssh_pass=vagrant
ansible_ssh_private_key_file=.vagrant/machines/vagrant1/virtualbox/private_key
----

{sp}+

Si ahora el error es que pide añadir el fingerprint a known_hosts se puede desactivar añadiendo el siguiente codigo en el <<Archivo de configuracion>>

----
[defaults]
host_key_checking = false
----

{sp}+

== Playbook

{sp}+

Un playbook es un archivo yml en el que se escribe la configuracion que se quiere para los hosts.

----
include::reglas.yml[]
----

Para ejecutar un playbook se utiliza el comando `ansible-playbook fichero.yml`

{sp}+

=== Name

{sp}+

Nombre que se le asigna al bloque de configuración que se define debajo.

{sp}+

=== Hosts

{sp}+

Parametro donde definir un grupo o todos en los que va a afectar las siguientes <<Tasks>>.

{sp}+

=== Become

{sp}+

Parametro para definir que todo lo que hay en ese bloque se ejecute con sudo.

{sp}+

=== Tasks

{sp}+

Parametro donde se definen la lista de tareas que realizara el bloque.

{sp}+

=== Modulos

{sp}+

Los parametros como apt, shell, etc son https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html[modulos], estos ejecutan un comando concreto.

{sp}+

=== State

{sp}+

Estado en el que se quiere dejar la accion definida.
Se utiliza tanto para https://docs.ansible.com/ansible/2.9/modules/service_module.html#service-module[servicios] (started, stopped, restarted, reloaded) como para https://docs.ansible.com/ansible/2.9/modules/apt_module.html#apt-module[paquetes] (absent, build-dep, latest, present, fixed), entre otros.

Consultar https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html[modulos].

{sp}+

== Archivo de configuracion

{sp}+

El archivo de configuracion `ansible.cfg` tiene que estar ubicado en el directorio de trabajo, en home o en `/etc/ansible/ansible.cfg`.

En el puedes https://docs.ansible.com/ansible/2.4/intro_configuration.html[configurar] el usuario remoto, puerto, etc. 

{sp}+

== Handlers

{sp}+

Los handlers son bloques del <<Playbook>> que se ejecutan cuando una tarea se ejecuta correctamente.

----
- name: deploy
  hosts: vagrantVM
  become: true
  tasks:

    - name: Instalar apache
      apt:
        name: apache2
        state: present
        update_cache: true
      notify:
        - "Reiniciar apache"

  handlers:
    - name: Reiniciar apache
      service: name=apache2 state=restarted
----

En este ejemplo al instalarse el apache al primera vez se reiniciara el servicio, si se vuelve a ejecutar el playbook `ansible-playbook -i inventory playbook.yml` al estar ya el servicio instalado no se ejecutara el handler.

{sp}+