local map = vim.api.nvim_set_keymap
local default_opts = {noremap = true, silent = true}
local cmd = vim.cmd

-- Moverse entre ventanas
map('n', '<Left>', ':<C-U>TmuxNavigateLeft<CR>', {silent = true})
map('n', '<Down>', ':<C-U>TmuxNavigateDown<CR>', {silent = true})
map('n', '<Up>', ':<C-U>TmuxNavigateUp<CR>', {silent = true})
map('n', '<Right>', ':<C-U>TmuxNavigateRight<CR>', {silent = true})

-- Redimensionar ventanas
map('n', '<A-Left>', ':wincmd H<CR>', {silent = true})
map('n', '<A-Right>', ':wincmd L<CR>', {silent = true})
map('n', '<A-Up>', ':wincmd K<CR>', {silent = true})
map('n', '<A-Down>', ':wincmd J<CR>', {silent = true})

-- Moverse al buffer anterior/siguiente
map('n', '<A-,>', ':BufferPrevious<CR>', {silent = true})
map('n', '<A-.>', ':BufferNext<CR>', {silent = true})

-- Reordenar al buffer anterior/siguiente
map('n', '<A-<> ', ':BufferMovePrevious<CR>', {silent = true})
map('n', '<A->>', ':BufferMoveNext<CR>', {silent = true})

-- Ir a buffer en posición...
map('n', '1', ':BufferGoto 1<CR>', {silent = true})
map('n', '2', ':BufferGoto 2<CR>', {silent = true})
map('n', '3', ':BufferGoto 3<CR>', {silent = true})
map('n', '4', ':BufferGoto 4<CR>', {silent = true})
map('n', '5', ':BufferGoto 5<CR>', {silent = true})
map('n', '6', ':BufferGoto 6<CR>', {silent = true})
map('n', '7', ':BufferGoto 7<CR>', {silent = true})
map('n', '8', ':BufferGoto 8<CR>', {silent = true})
map('n', '9', ':BufferGoto 9<CR>', {silent = true})
map('n', '0', ':BufferLast<CR>', {silent = true})

-- Pin/unpin buffer
map('n', '<A-p>', ':BufferPin<CR>', {silent = true})

-- Close buffer
map('n', '<A-x>', ':BufferClose<CR>', {silent = true})

-- Restore buffer
map('n', '<A-z>', ':BufferRestore<CR>', {silent = true})

-- Magic buffer-picking mode
map('n', '<A-p>', '<Cmd>BufferPick<CR>', {silent = true})
map('n', '<A-p>', '<Cmd>BufferPickDelete<CR>', {silent = true})

-- Sort automatically by...
map('n', '<Space>bb', '<Cmd>BufferOrderByBufferNumber<CR>', {silent = true})
map('n', '<Space>bd', '<Cmd>BufferOrderByDirectory<CR>', {silent = true})
map('n', '<Space>bl', '<Cmd>BufferOrderByLanguage<CR>', {silent = true})
map('n', '<Space>bw', '<Cmd>BufferOrderByWindowNumber<CR>', {silent = true})

-- Ajustar buffers
map('n', '<C-right>', ':vertical resize +5<CR>', {silent = true})
map('n', '<C-left>', ':vertical resize -5<CR>', {silent = true})
map('n', '<C-down>', ':resize +5<CR>', {silent = true})
map('n', '<C-up>', ':resize -5<CR>', {silent = true})
map('n', '<leader>c', ':split<CR>:ter<CR>:resize 15<CR>', {silent = true})
map('i', '<leader>c', ':split<CR>:ter<CR>:resize 15<CR>', {silent = true})

-- Crear buffers
map('n', '<leader>t', ':tabe<CR>', {silent = true}) -- Crear nuevo buffer en una pestaña
map('n', '<leader>vs', ':vsp<CR>', {silent = true}) -- Split vertical
map('n', '<leader>sp', ':sp<CR>', {silent = true}) -- Split horizontal

-- Listar buffers
map('n', '<S-Tab>', ':BufferLineCyclePrev<CR>', {silent = true})
map('n', '<Tab>', ':BufferLineCycleNext<CR>', {silent = true})
