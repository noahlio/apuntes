local map = vim.api.nvim_set_keymap
local default_opts = {noremap = true}
local cmd = vim.cmd

map("n", "<leader>rr", ":lua require ('rest-nvim').run()<CR>", default_opts)    -- Abrir rest 

cmd([[command! -nargs=1 PlayoffApiReal execute ':%s/http:\/\/' .. <q-args> .. '.local/https:\/\/' .. <q-args> .. '.playoffinformatica.com/g']])   -- Api de local a real

cmd([[command! -nargs=1 PlayoffApiLocal execute ':%s/https:\/\/' .. <q-args> .. '.playoffinformatica.com/http:\/\/' .. <q-args> .. '.local/g']])    -- Api de real a local
