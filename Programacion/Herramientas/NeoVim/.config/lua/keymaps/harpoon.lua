local map = vim.api.nvim_set_keymap
local default_opts = {noremap = true, silent = true}
local cmd = vim.cmd

map("n", "<leader>a", ':lua require("harpoon.mark").add_file()<CR>', default_opts)    -- Abrir rest 
map("n", "<leader>d", ':lua require("harpoon.mark").rm_file()<CR>', default_opts)    -- Abrir rest 
map("n", "<leader>m", ':lua require("harpoon.ui").toggle_quick_menu()<CR>', default_opts)    -- Abrir rest 
