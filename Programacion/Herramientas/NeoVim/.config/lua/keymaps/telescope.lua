local map = vim.api.nvim_set_keymap
local default_opts = {noremap = true, silent = true}
local cmd = vim.cmd

map("n", "<leader>n", "<cmd>Telescope find_files<CR>", default_opts) -- Buscar archivos
map("n", "<leader>F", "<cmd>Telescope live_grep<CR>", default_opts) -- Buscar en archivos
