local map = vim.api.nvim_set_keymap


map('n', '<leader>w', ':w<CR>', {noremap = true})                               -- Guardar
map('n', '<leader>q', ':q<CR>', {noremap = true})                               -- Salir

map('n', '<leader>C', ':e $MYVIMRC<CR>', {noremap = true})                      -- Abrir archivo de configuración de Vim
map('n', '<F5>', ':source ~/.config/nvim/init.lua<CR>', {noremap = true})       -- Recargar archivo de configuración
map('v', '<F5>', ':source ~/.config/nvim/init.lua<CR>', {noremap = true})       -- Recargar archivo de configuración

map('n', '<leader>h', ':noh<CR>', {noremap = true})                             -- Desactivar resaltado de búsqueda
map('n', '<leader>x', 'G$vgg_dd', {noremap = true})                             -- Borrar todo el contenido
