local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
	packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

return require('packer').startup(function(use)

  -- Instalador de paquetes
  use 'wbthomason/packer.nvim'

  -- Peticiones http
  -- use {"NTBBloodbath/rest.nvim", requires = "nvim-lua/plenary.nvim"}

  -- Interfaz
  use 'Shatur/neovim-ayu'

  -- Navegador de buffers
  use 'christoomey/vim-tmux-navigator'

  -- Explorador de archivos
  use 'nvim-telescope/telescope.nvim'
  
  -- Arbol de archivos
  vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])
  use {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v2.x",
    requires = { 
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
      "MunifTanjim/nui.nvim",
    }
  }

  -- Administrador bbdd
  use { "tpope/vim-dadbod", requires = "kristijanhusak/vim-dadbod-ui"}

  use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
  use('theprimeagen/harpoon')
  use('mbbill/undotree')
  use('tpope/vim-fugitive')

  use {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v1.x',
    requires = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},
      {'williamboman/mason.nvim'},
      {'williamboman/mason-lspconfig.nvim'},

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},
      {'hrsh7th/cmp-buffer'},
      {'hrsh7th/cmp-path'},
      {'saadparwaiz1/cmp_luasnip'},
      {'hrsh7th/cmp-nvim-lsp'},
      {'hrsh7th/cmp-nvim-lua'},

      -- Snippets
      {'L3MON4D3/LuaSnip'},
      {'rafamadriz/friendly-snippets'},
    }
  }

  -- Snippets
  use({"L3MON4D3/LuaSnip", tag = "v<CurrentMajor>.*",run = "make install_jsregexp"})

  -- Barra de navegacion
  use 'romgrk/barbar.nvim'
  
  -- Iconos de la barra de navegacion
  use 'nvim-tree/nvim-web-devicons'

  -- Administrador git
  use { 'NeogitOrg/neogit', requires = 'nvim-lua/plenary.nvim' }

  -- Administrador docker
  use {'dgrbrady/nvim-docker', requires = {'nvim-lua/plenary.nvim', 'MunifTanjim/nui.nvim'}}

end)
