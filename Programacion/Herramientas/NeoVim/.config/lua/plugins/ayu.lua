local cmd = vim.cmd
local opt = vim.opt

opt.termguicolors = true

require('ayu').setup({
	mirage = true,
	dark = true,
	overrides = {},
})

cmd [[colorscheme ayu]]
