local exec = vim.api.nvim_exec

exec ([[
	augroup mardownSpell
		autocmd!
		autocmd FileType mardown setlocal spell spelllang=es
		autocmd BufRead,BufNewFile *.md setlocal spell spelllang=es
	augroup END
]], false)

exec ([[
	augroup templates
		au!
		let g:template_name = 'Noah Bobis'
		autocmd BufNewFile *.* silent! execute '0r $HOME/.config/nvim/templates/'. expand("<afile>:e").'.tpl'
		autocmd BufNewFile * %s/{{YEAR}}/\=strftime('%Y')/ge
		autocmd BufNewFile * %s/{{NAME}}/\=template_name/ge
		autocmd BufNewFile * %s/{{EVAL\s*\([^}]*\)}}/\=eval(submatch(1))/ge
	augroup END
]], false)
