local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local opt = vim.opt

-- Configuración global
g.mapleader = ','

-- Configuración de opciones
opt.mouse = 'a'                                                 -- Interacción con el ratón
opt.sw = 2                                                      -- Tabulaciones con 2 espacios
opt.number = true                                               -- Numeración de línea
opt.expandtab = true                                            -- Tabulaciones en espacios
opt.smartindent = true                                          -- Indentación automática al hacer Enter
opt.numberwidth = 1                                             -- Números pegados al borde de la terminal
opt.incsearch = true                                            -- Mejoras en las búsquedas
opt.ignorecase = true                                           -- Ignora mayúsculas al buscar
opt.clipboard = 'unnamedplus'                                   -- Copiar en el portapapeles
opt.encoding = 'utf-8'                                          -- Codificación
opt.cursorline = true                                           -- Resalta la línea actual
opt.termguicolors = true                                        -- Activa colores en la sintaxis
opt.colorcolumn = '140'                                         -- Color límite de longitud
cmd('highlight ColorColumn ctermbg=0 guibg=lightgrey')          -- Configura el color de la columna
opt.splitbelow = true                                           -- Al hacer split, poner la pantalla abajo
opt.completeopt = 'menuone,noselect'                            -- Activar autocompletado
opt.wildmode = 'longest,list'                                   -- Modo de expansión de comandos

