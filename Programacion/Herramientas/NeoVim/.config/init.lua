require('settings/generalSettings')	-- ajustes generales de neovim
require('settings/templates')		-- ajustes de plantillas

require('keymaps/genericKeymaps')	-- atajos de teclado generales de neovim
require('keymaps/movement')		-- atajos de teclado para moverse
require('keymaps/buffers')		-- atajos de teclado para buffers
require('keymaps/telescope')		-- atajos de teclado de telescope
-- require('keymaps/rest') 		-- atajos de teclado de rest
require('keymaps/neo-tree') 		-- atajos de teclado de neotree
require('keymaps/harpoon') 		-- atajos de teclado de harpoon

require('plugins/packer') 		-- instalador plugins
require('plugins/ayu') 			-- theme
require('plugins/neo-tree')		-- arbol archivos
-- require('plugins/rest')	                -- peticiones api 
