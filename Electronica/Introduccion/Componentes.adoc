= Componentes
Author Name <noahbobis0512@gmail.com>; Noah Bobis 
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

:imagesdir: ./Imagenes/
:contenidos: ./Contenido/
:simbolos: ./Contenido/Simbolos/

== Bobina

{sp}+

image::{contenidos}bobina.png[Bobina, 150,float="right"]

Componente que almacena energia en forma de campo magnetico. Favorece el flujo de corriente y controla las fluctuaciones, evitando cambios de intensidad.

{sp}+

Es muy comun en dispositivos con un voltaje mínimamente elevado.

Simbolo: image:{simbolos}bobina.png[Bobina, 50]

{sp}+
{sp}+
{sp}+

== Boton

{sp}+

image::{contenidos}boton.png[Boton, 150,float="right"]

Circuito electrico que se abre y cierra al pulsarlo.

{sp}+

Es muy comun en dispositivos con un voltaje mínimamente elevado.

{sp}+
{sp}+
{sp}+

== Condensador

{sp}+

image::{contenidos}condensador.png[Condensador, 200,float="right"]

Un condensador almacena pequeñas cantidades de energia. La cantidad de energia que almacena se mide en faradios (F).

Si la fuente de energia tiene mayor voltaje que el aguante del condensador explotará.

{sp}+
{sp}+

=== Polaridad

Hay condensadores polarizados y no polarizados, polarizados es que un conector indica el positivo y otro el negativo.

{sp}+
{sp}+
{sp}+

== Diodo

{sp}+

image::{contenidos}diodoInterno.png[Partes de un diodo, 400,float="right"]

Componente que permite el paso de corriente en un solo sentido y con un amperaje especifico. Es útil para proteger y regular el circuito.

Tiene un consumo de voltaje en el circuito.

Tienen una numeración que si buscas corresponde a sus caracteristicas y funciones.

{sp}+

image::{contenidos}diodo.png[Diodo, 200,float="right"]

La parte del diodo con una franja de diferente color es por donde no pasa la corriente.

{sp}+
{sp}+

=== Puente de diodos

{sp}+

image::{contenidos}puenteDiodos.png[Puente de diodos, 200,float="right"]

Conjunto de cuatro diodos que convierten ondas alternas en continuas.

{sp}+

Todos los cargadores tienen un puente de diodos, ya que la corriente que llega a los dispositivos son continuas.

{sp}+
{sp}+
{sp}+

== Integrados

{sp}+

image::{contenidos}integrado.png[Componente integrado, 200,float="right"]

Componentes en un chip desarrollados para una tarea concreta, (sonido, sensores...). Tienen una numeración que indica su función.

{sp}+

No se puede reprogramar un integrado, al contrario que un microcontrolador.

{sp}+
{sp}+
{sp}+

== Led

{sp}+

image::{contenidos}ledInterno.png[Led por dentro, 300,float="right"]

Componente para proyectar luz, pueden ser de un color o RGB. Los RGB tienen 3 luces internas regulables, lo que hace que puedas mostrar un color concreto.

{sp}+

Tienen un catodo y anodo (conector positivo y negativo).

{sp}+

image::{contenidos}tablaLeds.png[Tabla leds, 160,float="right"]

Cada led consume un voltaje y soporta unos amperios determinados.

{sp}+
{sp}+
{sp}+

=== Display

{sp}+

image::{contenidos}display.png[Display, 100,float="right"]

Componente formado por <<Led,leds>> para simbolizar numeros segun su estado.

{sp}+
{sp}+
{sp}+

=== Matriz de leds

{sp}+

image::{contenidos}matrizLeds.png[Matriz de leds, 150,float="right"]

Componente formado por una matriz <<Led,leds>> para simbolizar letras.

Puede mostrar todo tipo de dibujo que se pueda realizar en sus dimensiones.

{sp}+
{sp}+
{sp}+

== Oscilador de cristal

{sp}+

image::{contenidos}osciladorDeCristal.png[Oscilador de cristal, 150,float="right"]

Componente que se encarga de dar una frequencia constante.

{sp}+

Ejemplos de uso:

- Frequencia relojes

- Estabilizador frequencias de receptores y transmisores de radio

{sp}+
{sp}+
{sp}+

== Relé

{sp}+

image::{contenidos}rele.png[Relé, 120,float="right"]

Componente que regula el paso de la corriente.

{sp}+

Se utiliza para activar circuitos que consumen mucha energia pero tienen poca potencia.

{sp}+

image::{contenidos}releInterno.png[Relé interno, 200,float="right"]

Un relé esta compuesta de una bobina conectada a un metal, cuando la bobina tiene corriente se comporta como un electroiman, acercando el metal y haciendo que haga contacto. Se comporta como un interruptor.

Se necesita un diodo para que la energia que almacena la bobina no dañe el circuito y se almacene en un diodo y vuelva al relé

{sp}+
{sp}+
{sp}+

== Resistencia

{sp}+

image::{contenidos}resistencia.png[Resistencia, 200,float="right"]

Una resistencia es un componente que se resiste al paso de la corriente, tiene unos Ω especificos señalizados con sus bandas de colores.

{sp}+
{sp}+

=== Potenciometro

{sp}+

image::{contenidos}potenciometroInterno.png[Potenciometro interno, 300,float="right"]

Un potenciometro es una resistencia regulable, normalmente de unos 250KΩ. El cursor regula la cantidad de material resistente que se utiliza.

{sp}+
{sp}+
{sp}+

== Terminal

{sp}+

image::{contenidos}terminales.png[Terminales, 200,float="right"]

Componentes para fijar un cable a una conexión.

La parte a fijar del cable tiene que estar sin ninguna protección de plástico.

{sp}+
{sp}+
{sp}+

== To220

{sp}+

image::{contenidos}to220.png[To220, 100,float="right"]

Tipo de encapsulado, puede ser un <<Transistor, transistor>>, <<Regulador de tensión, regulador de tensión>>...

{sp}+

Su función se indica en la numeración que tenga.

{sp}+
{sp}+
{sp}+

== Transformador

{sp}+

image::{contenidos}transformador.png[Transformador, 200,float="right"]

Componente compuesto de 2 <<Bobina, bobinas>>, aumenta o disminuiye la tension de un circuito con corriente alterna, manteniendo la potencia.

{sp}+
{sp}+

=== Transformador de audio

{sp}+

image::{contenidos}transformadorAudio.png[Transformador, 200,float="right"]

Aumentan la intensidad de la corriente recibida.

Se utiliza en sonido para subir el volumen, suelen ser integrados.

{sp}+
{sp}+
{sp}+

== Transistor

{sp}+

image::{contenidos}transistor.png[Transistor, 200,float="right"]

Componente semiconductor que permite el paso de la corriente segun como le llegue a el la corriente.

{sp}+