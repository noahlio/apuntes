= Circuitos
Author Name <noahbobis0512@gmail.com>; Noah Bobis 
v1.0, 2022-03-14
:encoding: utf-8
:lang: es
:toc: 
:toc-title: Índice
:toclevels: 2
:numbered:
:experimental:

<<<

{sp}+

:imagesdir: ./Imagenes/
:contenidos: ./Contenido/
:circuitos: ./Contenido/Circuitos/

{sp}+

== Tipos de circuitos

{sp}+

=== Astable

{sp}+

Un circuito astable es aquel que no tiene un estado estable, siempre varia entre dos estados inestables un tiempo determinado.

{sp}+

== Circuitos

{sp}+

=== LM555

{sp}+

image::{circuitos}LM555.png[Circuito LM555, 400, float="right"]

El circuito LM555 es un circuito <<Astable, astable>> que utiliza un circuito integrado 555, el cual se utiliza para generar temporizadores, pulsos y oscilaciones.

El circuito 555 pasa la señal del capacitor de analogica a continua, con el potenciometro se puede regular la velocidad a la que se carga y descarga el capacitor.

https://www.youtube.com/watch?v=y8_wxpw-Yj4&list=PLNipMBg3MF-by_2uhpnmGbGTBPMfqo7HV&index=13&ab_channel=Editronikx[Ejemplo de montaje]

{sp}+
