int verde=10;
int azul=11;
int rojo=9;
void setup() {
 pinMode(verde,HIGH);
 pinMode(azul,HIGH);
 pinMode(rojo,HIGH);
}

void loop() {
  //naranja
  analogWrite(verde,30);
  analogWrite(azul,0);
  analogWrite(rojo,255);
  delay(1000);
  //lila
  analogWrite(verde,0);
  analogWrite(azul,255);
  analogWrite(rojo,197);
  delay(1000);
  //amarillo
  analogWrite(verde,150);
  analogWrite(azul,0);
  analogWrite(rojo,255);
  delay(1000);
}
