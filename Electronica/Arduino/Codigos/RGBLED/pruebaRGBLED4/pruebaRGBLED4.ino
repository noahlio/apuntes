int verde=10;
int azul=11;
int rojo=9;
void setup() {
 pinMode(verde,HIGH);
 pinMode(azul,HIGH);
 pinMode(rojo,HIGH);
}

void loop() {
  //naranja
  color(255,30,0);
  //lila
  color(197,0,255);
  //amarillo
  color(255,150,0);
}
void color(int R,int G,int B){
  //se invierten los colores ya que al tener un anodo comun los leds se alimentan por "-".
  analogWrite(verde,255-G);
  analogWrite(azul,255-B);
  analogWrite(rojo,255-R);
  delay(1000);
}
