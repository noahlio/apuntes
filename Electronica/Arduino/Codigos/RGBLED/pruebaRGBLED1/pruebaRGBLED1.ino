int verde=10;
int azul=11;
int rojo=9;
void setup() {
 pinMode(verde,HIGH);
 pinMode(azul,HIGH);
 pinMode(rojo,HIGH);
}

void loop() {
  analogWrite(verde,255);
  analogWrite(azul,0);
  analogWrite(rojo,0);
  delay(1000);
  analogWrite(verde,0);
  analogWrite(azul,255);
  analogWrite(rojo,0);
  delay(1000);
  analogWrite(verde,0);
  analogWrite(azul,0);
  analogWrite(rojo,255);
  delay(1000);
}
