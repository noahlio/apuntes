#include <LiquidCrystal.h>

LiquidCrystal lcd(7,6,5,4,3,2); //pines a los que esta conectado el lcd

void setup() {
  lcd.begin(16,2); //tamaño del lcd (16 columnas, 2 filas)
}

void loop() {
  lcd.setCursor(10,0);//escribir a partir de la columna 10 de la fila 0
  lcd.print("Alarma");//escribir alarma
  lcd.setCursor(10,1);//escribir a partir de la columna 10 de la fila 1
  lcd.print(millis()/1000);//escribir los segundos que lleva el prpgrama encendido
  //efecto parpadeo
  lcd.display();//mostrar el mensaje
  delay(200);
  lcd.noDisplay();//ocultar el mensaje
  delay(200);
  lcd.scrollDisplayLeft();// mover texto hacia la izquierda
}
