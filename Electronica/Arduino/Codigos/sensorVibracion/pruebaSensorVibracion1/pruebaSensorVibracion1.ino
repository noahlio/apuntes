void setup() {
  pinMode(2,INPUT);
  pinMode(3,OUTPUT);
}
void loop() {
  int estado=digitalRead(2); //leer estado del sensor
  if(estado==LOW){//cuando se detecte una vibracion
    digitalWrite(3,HIGH);//encender led
    delay(500);//dejar encendido 500ms para notar la vibracion
  }
  digitalWrite(3,LOW); //apagar led
}
