void setup() {
  pinMode(2,INPUT);
  pinMode(3,OUTPUT);
}
void loop() {
  int estado=digitalRead(2); //leer estado del sensor
  digitalWrite(3,estado); //encender led si el trozo de mercurio esta cerrando el circuito
  delay(100);//espera de 100 ms
}
