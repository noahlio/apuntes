int led_izquierda=3;
int led_derecha=5;
int led_abajo=6;
int led_arriba=9;
int pulsador=10;
int led_sw=11;
void setup() {
  pinMode(led_izquierda,OUTPUT);
  pinMode(led_derecha,OUTPUT);
  pinMode(led_abajo,OUTPUT);
  pinMode(led_arriba,OUTPUT);
  pinMode(led_sw,OUTPUT);
  pinMode(pulsador,INPUT);
  
}
void loop() {
  int x=analogRead(A0);
  int y=analogRead(A1);
  int sw=digitalRead(pulsador);
  if(x>650 && x<830){
    analogWrite(led_derecha, map(x,650,830,255,0));
  }
  else analogWrite(led_derecha, 0);
  
  if(x>870 && x<1024){
    analogWrite(led_izquierda, map(x,870,1023,0,255));
  }
  else analogWrite(led_izquierda, 0);
  
  if(y>650 && y<830){
    analogWrite(led_arriba, map(y,650,830,255,0));
  }
  else analogWrite(led_arriba, 0);
  
  if(y>870 && y<1024){
    analogWrite(led_abajo, map(y,870,1023,0,255));
  }
  else analogWrite(led_abajo, 0);

  digitalWrite(led_sw, !sw);
}
