int led=3; //numero de pin del led
int pot=0; //numero de pin (analogico) del potenciomentro
void setup() {
 pinMode(led,OUTPUT); //pin de salida (led)
}

void loop() {
  int b=analogRead(pot)/4; //variable b con el valor que el potenciometro envie /4
  // se divide entre 4 devido a que la potencia analogica se mide de 0-255,
  // pero el potenciometro envia señal de 0-1023
  analogWrite(led,b); //se cambia la intensidad del led a la de la variable b
}
