int led=3; //numero de pin del led
void setup() {
 pinMode(led,OUTPUT); //pin de salida (led)
}

void loop() {
  for(int b=0;b<256;b++){ //bucle de 0-256 sumando cada ciclo 1 a "b"
    analogWrite(led,b); //cambiando la intensidad del led a "b"
    delay(5);//espera de 5 milisegundos para que se note el cambio
  }
  //lo mismo a la inversa
  for(int b=255;b>=0;b--){
    analogWrite(led,b);
    delay(5);
  }
}
