void setup()
{
  pinMode(4, OUTPUT);//pin de salida (led)
  pinMode(3, INPUT); //pin de entrada (boton)
}

void loop() {
  if(digitalRead(3)==HIGH){
    digitalWrite(4,HIGH);//si el boton esta pulsado se enciende el led
  }
  else{
    digitalWrite(4,LOW);//si no se apaga
  }
}
