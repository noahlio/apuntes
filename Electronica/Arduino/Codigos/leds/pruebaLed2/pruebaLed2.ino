int led=4; //se define a la variable led el 4 para no tener que recordar su pin
int boton=3; //se define a la variable boton el 3 para no tener que recordar su pin
int estado=LOW; //se define el estado inicial del led (apagado)
void setup() {
 pinMode(led,OUTPUT);//pin de salida (led)
 pinMode(boton,INPUT); //pin de entrada (boton)
}

void loop() {
  while(digitalRead(boton)==LOW){  //se detiene el codigo en un bucle hasta que se pulse el boton  
  }
  estado=digitalRead(led); //se lee el estado actual del led
  digitalWrite(led,!estado); //se escribe el estado contrario al que estaba el led
  while(digitalRead(boton)==HIGH){ //se detiene el codigo en un bucle hasta que se suelte el boton
  }
}
