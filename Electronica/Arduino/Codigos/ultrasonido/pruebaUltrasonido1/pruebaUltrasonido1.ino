int trig=10; //pin de trigger
int eco=9;//pin de eco
int led=3;//pin de led
void setup() {
  pinMode(trig,OUTPUT);//pin de salida
  pinMode(eco,INPUT); //pin de entrada
  pinMode(led,OUTPUT); //pin de salida
  Serial.begin(9600); //activar que el arduino pueda enviar informacion al ordenador (recibir prints al terminal), 9600 es la cantidad de bits que envia.
}

void loop() {
  digitalWrite(trig,HIGH); //se enciende el transmisor
  delay(1); //se espera un milisegundo de envio
  digitalWrite(trig,LOW); //se apaga el transmisor
  int duracion=pulseIn(eco,HIGH); //se calcula lo que ha tardado la señal en ir, rebotar y volver
  int distancia=duracion/58.2; //se divide el valor anterior entre 58.2, segun las instrucciones del fabricante
  Serial.println(distancia); //se imprime por terminal la distancia a la que esta el objeto
  delay(20);//tiempo entre dato y dato
  if(distancia<30){ //si la distancia al objeto es menor a 30 se encendera durante un tiempo igual a distancia*10 y se apagara. Con este efecto el led parpadea a cierta velocidad segun la distancia a la que este el objeto
    digitalWrite(led,HIGH); 
    delay(distancia*10);
    digitalWrite(led,LOW);
  }
}
