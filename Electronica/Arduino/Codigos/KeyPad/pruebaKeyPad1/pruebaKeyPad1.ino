#include <Key.h>
#include <Keypad.h> //importar las librerias de keypad

char keys[4][4]={
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
}; //matriz de las letras del keypad

char clave[7]; //crear la variable donde se escribira la clave por inputs
char claveMaster[7]="123456"; //la clave real
byte i=0; //el indice donde se escribe

byte pinesFilas[4] = {2,3,4,5}; //pines de las filas
byte pinesColumnas[4] = {6,7,8,9}; //pines de las columnas


Keypad teclado=Keypad(makeKeymap(keys),pinesFilas,pinesColumnas,4,4); //crear el teclado con la matriz de letras, los pines de filas y columnas y las dimensiones del teclado
void setup() {
  Serial.begin(9600);
}

void loop() {
  char tecla=teclado.getKey(); //leer input de teclado
  if(tecla){//si se ha introducido una tecla...
    Serial.print(tecla);
    clave[i]=tecla;
    i++;
  }
  if(i==6){
    i=0;
    Serial.println();
    Serial.println(!strcmp(clave,claveMaster)?"Correcto":"Incorrecto"); //strcmp compara dos strings, si son iguales devuelve 0 y si no 1. Por eso se utiliza ! antes
  }
}
