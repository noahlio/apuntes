#include <Servo.h> //incluir libreria para servomotores

Servo s1; //crear objeto servomotor

int servo=2; //pin servomotor
int pulsoMin=350; //minimo rotacion servomotor
int pulsoMax=2350; //maximo rotacion servomotor
//los extremos de rotacion dependen de cada servomotor, para configurarlo correctamente hay que ir probando hasta dar con la numeracion correcta
int potenciometro=0; //pin potenciometro

void setup() {
  s1.attach(servo,pulsoMin,pulsoMax); // asignar a s1 el pin del servomotor, la rotacion minima y maxima
}

void loop() {
  int valPotenciometro=analogRead(potenciometro); //leer valor del potenciometro
  int angulo = map(valPotenciometro,0,1023,0,180); // convertir el rango devuelto por valPotenciometro. valPotenciometro devuelve un numero entre 0-1023, la funcion map cambia el rango a 0-180
  s1.write(angulo);//se cambia el angulo de s1
  delay(20);//pausa de 20 milisegundos por si el servomotor lleva retraso por el peso que mueve
}
